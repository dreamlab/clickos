#ifndef NIPQUAD_HH
#define NIPQUAD_HH

#ifndef NIPQUAD
#define NIPQUAD(addr) \
 ((unsigned char*)&addr)[3], \
 ((unsigned char*)&addr)[2], \
 ((unsigned char*)&addr)[1], \
 ((unsigned char*)&addr)[0]
#endif

#endif

