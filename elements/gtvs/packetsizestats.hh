#ifndef CLICK_PACKETSIZESTATS_HH
#define CLICK_PACKETSIZESTATS_HH

#include <click/element.hh>
#include <click/ipflowid.hh>
#include <click/notifier.hh>
#include <clicknet/tcp.h>

#include <utility>

#include "flowcache.hh"
#include "dataexport.hh"

CLICK_DECLS

/*
=c

PacketSizeStats([I<KEYWORDS>])

=s tnt

collects the series of packet sizes of TCP/UDP flows

=d

PacketSizeStats collects the series of packet sizez of TCP/UDP flows for the
first SERIES_LENGTH packets of each flow.
PacketSizeStats uses the direction indication to set the sign of each packet
size. The positive sign is used to represent the size for packets having as
source address the originator of the TCP connection (or UDP flow).
Viceversa, the negative sign is used to represent the size when the current
packet has been sent by the destination of the TCP connection.

Keywords are:

=over 8

=item SERIES_LENGTH

The length for the packet size series. Default is 10.

=item SINGLE_COLUMN_OUTPUT

Whether to output the packet size series as a single, colon separated values
string. Default is false.

=item SKIP_THS

For TCP traffic, whether the inter-packet arrival time series should start after
the triple handshake is observed. Default is true.

=item FLOWCACHE

Element. A FlowCache element that provides state tracking of TCP/UDP flows.
Default is the upstream FlowCache element.

=a

FlowCache
*/

class PacketSizeStats: public Element, public FlowCache::FlowStateHolder
{
public:

    PacketSizeStats();
    ~PacketSizeStats();

    const char* class_name() const { return "PacketSizeStats"; }
    const char* port_count() const { return PORTS_1_1; }
    const char* processing() const { return AGNOSTIC;  }

    int configure(Vector<String>& conf, ErrorHandler* errh);

    Packet* simple_action(Packet*);

    void write_header(DataExport& exporter) const;
    void write_flow_state(DataExport& exporter, const BaseFlowState*) const;

    int series_length() { return series_len; }
    
    class FlowState;
private:

    int series_len;
    bool single_col;
    bool skip_ths;

    FlowCache *flow_cache;
};

// use int16_t assuming that we're processing ethernet packets which implies
// that packet sizes are no bigger than 1536
class PacketSizeStats::FlowState : public BaseFlowState
{
public:
    FlowState(int series_len);
    ~FlowState();

    void push_payload_size(Packet* p, int series_len);

    int length() const { return count; }
    
    std::pair<uint16_t,int> pkt_size(int which) const
    {
        assert(which < count);
        int32_t sz = size_series[which];
        return (sz >= 0) ? std::pair<uint16_t,int>(sz, 0) : std::pair<uint16_t,int>(-sz - 1, 1);
    }
    
private:
    int16_t* size_series;
    int count;

    friend class PacketSizeStats;
};


CLICK_ENDDECLS

#endif

