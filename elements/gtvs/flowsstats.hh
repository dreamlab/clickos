#ifndef CLICK_FLOWSSTATS_HH
#define CLICK_FLOWSSTATS_HH

#include <click/element.hh>
#include <clicknet/tcp.h>

#include "flowcache.hh"
#include "dataexport.hh"

CLICK_DECLS

class FlowsStats: public Element, FlowCache::FlowStateHolder
{
    FlowCache* flow_cache;

    class FlowState;
    class TCPOptParser;
public:

    FlowsStats();
    ~FlowsStats();

    const char* class_name() const { return "FlowsStats"; }
    const char* port_count() const { return PORTS_1_1; }
    const char* processing() const { return AGNOSTIC;  }

    int configure(Vector<String>& conf, ErrorHandler* errh);

    Packet* simple_action(Packet*);

    void write_header(DataExport& exporter) const;
    void write_flow_state(DataExport& exporter, const BaseFlowState*) const;
};


CLICK_ENDDECLS

#endif

