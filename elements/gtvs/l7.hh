#ifndef CLICK_L7_HH
#define CLICK_L7_HH

#include <click/element.hh>
#include <click/ipflowid.hh>
#include <clicknet/tcp.h>

#include <pcre.h>
//~ #include <pcreposix.h>

#include "appmarks.hh"
#include "flowcache.hh"

#include "l7patterninfo.hh"
#include "l7pattern.hh"
#include "patternutils.hh"


#define CLICK_DEBUG_L7


CLICK_DECLS

/*
=c

L7([I<KEYWORDS>])

=s tnt

classifies TCP/UDP flows using the l7-filter engine

=d

L7 classifies TCP/UDP flows using the l7-filter engine.
Please see http://l7-filter.sf.net for l7-filter documentation.

Keywords are:

=over 8

=item PROTOCOLS

A space separate string containing the list of protocol pattern names to be used by L7.

=item MAX_PKTS

The maximum number of packets which are used to search for the configured
patterns. Default 10.

=item MAX_BYTES

The maximum number of payload bytes which are used to search for the
configured patterns. Default 2048.

=item PATTERNS_INFO

Element. A L7PatternInfo element that provides l7-filter pattern definitions.

=item USE_APPMARKS

Boolean. Whether the output is an APPMARK value or the l7-filter's pattern
name.

=item FLOWCACHE

Element. A FlowCache element that provides state tracking of TCP/UDP flows.
Default is the upstream FlowCache element.

=a

FlowCache,AppMarks,L7PatternInfo
*/

class L7: public Element, FlowCache::FlowStateHolder
{
public:
	L7(): buffer_count_(10), byte_count_(2048), max_pkt_match_(0) {}

	const char* class_name() const { return "L7"; }
	const char* port_count() const { return PORTS_1_1; }
	const char* processing() const { return AGNOSTIC;  }

	int configure(Vector<String>& conf, ErrorHandler* errh);
	int initialize(ErrorHandler* errh);
	
	void add_handlers();

	Packet* simple_action(Packet* p);

	void write_header(DataExport& exporter) const;
	void write_flow_state(DataExport& exporter, const BaseFlowState* fs_) const;

	class FlowState;

private:
	bool add_pattern(const String& name, const String& pattern);
	static String read_max_pkt_match(Element*, void*);
	AppMark classify(char*, u_int);
private:
	bool initialized_;
	Vector<String> patternNames_;
	Vector<L7Pattern> patterns_;
	uint32_t buffer_count_;
	uint32_t byte_count_;
	FlowCache* flow_cache_;
	L7PatternInfo* pinfo_;
	bool use_appmarks_;
	// for read handler!
	uint32_t max_pkt_match_;

	friend class FlowState;
};

class L7::FlowState: public BaseFlowState, public AppMark
{
public:
	FlowState();
	~FlowState();

	void handle_packet(Packet*, L7*);
	void append_to_buffer(const char* app_data, int appdatalen, uint32_t& max_bytes, char* buffer, uint32_t& lengthsofar);
private:
	uint32_t packet_counter[2];
	uint32_t lengthsofar[2];
	char* buffer[2];
};

CLICK_ENDDECLS
#endif

