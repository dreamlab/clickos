/*
 * interarrivalstats.{cc,hh} -- collect the series of inter-packet arrival
 *                               times of TCP/UDP flows
 * Marco Canini
 * Sergio Mangialardi
 *
 * Copyright (c) 2007 by University of Genova - DIST - TNT laboratory
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: interarrivalstats.cc 2216 2009-05-06 18:05:24Z marco $
 */

#include <click/config.h>
#include <click/error.hh>
#include <click/packet_anno.hh>
#include <click/confparse.hh>
#include <click/string.hh>
#include <click/straccum.hh>

#include "interarrivalstats.hh"

CLICK_DECLS

template <class T> T min(T a, T b)
{
    return a < b ? a : b;
}

InterArrivalStats::InterArrivalStats()
{
}

InterArrivalStats::~InterArrivalStats()
{
}

int InterArrivalStats::configure(Vector<String>& conf, ErrorHandler* errh)
{
    Element* af_element = 0;
    series_len = 10;
    single_col = false;
    skip_ths = true;

    if (cp_va_kparse(conf, this, errh,
            "FLOWCACHE", cpkP, cpElement, &af_element,
            "SERIES_LENGTH", 0, cpUnsigned, &series_len,
            "SINGLE_COLUMN_OUTPUT", 0, cpBool, &single_col,
            "SKIP_THS", 0, cpBool, &skip_ths,
            cpEnd) < 0)
        return -1;

    if (af_element && !(flow_cache = (FlowCache*)(af_element->cast("FlowCache"))))
        return errh->error("FLOWCACHE must be a FlowCache element");

    if (!flow_cache)
        flow_cache = FlowCache::upstream_instance(this);

    if (!flow_cache)
        return errh->error("Initialization failure!");

    if (series_len == 0)
        return errh->error("SERIES_LENGTH must be greater then zero");

    flow_cache->register_flow_state_holder(this, name());

    return 0;
}

Packet* InterArrivalStats::simple_action(Packet* p)
{
    FlowState* fs = flow_cache->lookup_state<FlowState>(this, p);

    if (!fs)
    {
        fs = new FlowState(series_len);
        flow_cache->state(this, p) = fs;

        if (!fs)
        {
            click_chatter("out of memory!");
            p->kill();

            return 0;
        }
        
        fs->initialize(p);
    }
    else
    {
        FlowCache::Flow f = flow_cache->lookup_flow(p);
        if (skip_ths == false || f.is_udp() || f.is_ths_completed())
            fs->handle_packet(p, series_len);
        else
            fs->initialize(p);
    }

    return p;
}

void InterArrivalStats::write_header(DataExport& exporter) const
{
    if (single_col == false) {
        for (int i = 0; i < series_len; ++i) {
            StringAccum name;
            name << "InterArrivalTime" << i;
            exporter += column<double>(name.c_str(), true);
        }
    } else {
        StringAccum name;
        name << "InterArrivalTimes[" << series_len << "]";
    
        exporter += column<String>(name.c_str(), false);
    }
}

static void print_ia(StringAccum& value, const Timestamp &ts)
{
    if (ts.sec() >= 0)
    {
        value << ">" << ts.unparse();
    }
    else
    {
        Timestamp d(-ts);
        value << "<" << d.unparse();
    }
}

void InterArrivalStats::write_flow_state(DataExport& exporter, const BaseFlowState* fs_) const
{
    const FlowState* fs = static_cast<const FlowState*>(fs_);
    int l = min(series_len, fs->count - 1);
    if (single_col == false) {
        int i = 0;
        while (i < l) {
            /* // this would remove the sign
            if (fs->size_series[i].sec() >= 0)
                exporter << fs->ia_series[i];
            else {
                Timestamp d(-fs->ia_series[i]);
                exporter << d;
            }
            */
            exporter << fs->ia_series[i];
            ++i;
        }
        while (i < series_len) {
            exporter << DataExport::NULL_VALUE;
            ++i;
        }
    } else {
        StringAccum value;
    
        if (l > 0)
        {
            for (int i = 0; i < l - 1; i++)
            {
                print_ia(value, fs->ia_series[i]);
                value << ":";
            }
            print_ia(value, fs->ia_series[l - 1]);
        }
        else
        {
            value << ":";
        }
    
        exporter << value.c_str();
    }
}

InterArrivalStats::FlowState::FlowState(int series_len): count(0)
{
    ia_series = new Timestamp[series_len];
}

InterArrivalStats::FlowState::~FlowState()
{
    delete[] ia_series;
}

void InterArrivalStats::FlowState::initialize(Packet* p)
{
    ia_series[0] = p->timestamp_anno();
    count = 1;
}

void InterArrivalStats::FlowState::handle_packet(Packet* p, int series_len)
{
    if (count == series_len + 1)
        return; // nothing to do

    Timestamp d = p->timestamp_anno() - ia_series[count - 1];
    if (PAINT_ANNO(p) == 1)
    {
        //        d.set_sec(-d.sec());
        ia_series[count - 1] = -d;
    }
    else
    {
        ia_series[count - 1] = d;
    }

    if (count < series_len)
        ia_series[count] = p->timestamp_anno();
    count++;
}

ELEMENT_REQUIRES(FlowCache)
EXPORT_ELEMENT(InterArrivalStats)

CLICK_ENDDECLS

