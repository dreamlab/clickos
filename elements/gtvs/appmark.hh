#ifndef CLICK_APPMARK_HH
#define CLICK_APPMARK_HH

#include <click/element.hh>
#include <click/hashcode.hh>

#include "appmark_anno.hh"

CLICK_DECLS

/*
 * AppMark encodes an application mark used to identify to which application
 * and class a flow belongs to.
 * We shall have a coding scheme to represent a top level category, a secondary
 * group and the specific application/protocol.
 * For example, a category is P2P which may contain peer-to-peer file sharing
 * and streaming application. Edonkey and Bittorrent are just the application
 * name, but they have to be mapped to P2P/file sharing class.
 * We also want this scheme to be compact so we chose to use a 8 + 8 + 16 bit
 * encoding for category, 2nd group and application, respectively.
 * The zero has a special meaning in that it represents unavailable
 * information.
 * 
 * We then use a global map to store the correspondence between class, group
 * and application names to some numeric code.
 * This is a convenient way to avoid incoherent representations of the same
 * application in different classifiers.
 * Hence, an AppMark can be looked up just by the name of its components.
 * 
 * A classifier must inherit its FlowState from AppMark and must use
 * SET_APPMARK_ANNO to set the AppMark annotation in each packet of 
 * identified flows.
 */

class AppMarks;

class AppMark
{
public:
    AppMark(): mark_(0) {}
    AppMark(uint32_t m): mark_(m) {}
    
    AppMark& operator=(const AppMark& m) { mark_ = m.mark_; return *this; }
    
    inline uint8_t get_class() const { return (uint8_t) ((mark_ >> 24) & 0xFF); }
    inline uint8_t get_group() const { return (uint8_t) ((mark_ >> 16) & 0xFF); }
    inline uint16_t get_proto() const { return (uint16_t) (mark_ & 0xFFFF); }
    
    inline uint32_t get_mark() const { return mark_; }
    
    inline void set_mark(uint32_t m) { mark_ = m; }
    inline void set_mark(const AppMark& m) { mark_ = m.mark_; }

    inline bool matches(const AppMark &m) const
    {
        if (get_class() != m.get_class())
            return false;
        if (get_group() != 0 && get_group() != m.get_group())
            return false;
        if (get_proto() != 0 && get_proto() != m.get_proto())
            return false;
        return true;
    }
    
    inline hashcode_t hashcode() const
    {
        return mark_;
    }

    inline bool is_valid() const { return mark_ != 0; }
        
private:
    AppMark(uint32_t c, uint32_t g): mark_((c << 24) | (g << 16)) {}
    
    void set_class(uint32_t v) { mark_ = (0xFF000000 & (v << 24)) | (0x00FFFFFF & mark_); }
    void set_group(uint32_t v) { mark_ = (0x00FF0000 & (v << 16)) | (0xFF00FFFF & mark_); }
    void set_proto(uint32_t v) { mark_ = (0xFFFF & v) | (0xFFFF0000 & mark_); }
    
    friend class AppMarks;
    
    uint32_t mark_;
};

inline bool operator==(const AppMark& m1, const AppMark& m2)
{
    return m1.get_mark() == m2.get_mark();
}

inline bool operator!=(const AppMark& m1, const AppMark& m2)
{
    return m1.get_mark() != m2.get_mark();
}


CLICK_ENDDECLS

#endif

