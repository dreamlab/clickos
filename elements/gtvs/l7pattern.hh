#ifndef CLICK_L7PATTERN_HH
#define CLICK_L7PATTERN_HH

/*
 * l7pattern.{cc,hh} -- L7 pattern.
 *
 * Initial version:
 *    Sergio Mangialardi
 *    Marco Canini
 * 
 * PCRE support and modularization:
 *    Vito Piserchia
 * 
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 * Copyright (c) 2013-14 by Dreamlab Technologies AG
 * 
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * * Neither the name of Dreamlab Technologies AG nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */


#include <pcre.h>
#include "l7patterninfo.hh"
#include "patternutils.hh"
#include "l7.hh"
#include "appmarks.hh"

CLICK_DECLS

class L7Pattern
{
public:
	L7Pattern() {}
	L7Pattern(const String& name, const String& pattern);
	bool matches(char* buffer, u_int length);
	String get_name() const { return name_; };
	String get_pattern() const { return pattern_; };
	AppMark get_mark() const { return mark_; };
	uint get_minsize() const { return pminsize_; };

private:
	String name_;
	AppMark mark_;
	String pattern_;
	uint pminsize_;      // pcre_fullinfo PCRE_INFO_MINLENGTH 
	int poptions_;       // pcre_fullinfo PCRE_INFO_OPTIONS
	int cflags_;         // for regcomp
	int eflags_;         // for regexec
	pcre *preg_;         // compiled regular expression
	pcre_extra *preg_e_; // extra compiled stuff

	bool parse_pattern_string(const String& pattern_str, ErrorHandler* errh);
	String pre_process(const String& pattern);
	bool parse_flags(const String& line);
public:
	bool initialized;
};

L7Pattern::L7Pattern(const String& name, const String& pattern_str):
    pminsize_(0), poptions_(0), cflags_(PCRE_EXTENDED | PCRE_CASELESS | PCRE_NO_AUTO_CAPTURE), eflags_(0)
{
	cp_string(name, &name_);
	
	ErrorHandler* errh = ErrorHandler::default_handler();
	const char *errbuf;
	int erroff;
	initialized = false;
	
	
    if (!parse_pattern_string(pattern_str.c_str(), errh))
    {
        StringAccum sa;
        sa << "error parsing " << name_ << " -- /" << pattern_ << "/";
        errh->fatal(sa.c_str());
    }

    if (!(preg_ = pcre_compile(pre_process(pattern_).c_str(), cflags_, &errbuf, &erroff, NULL)))
    {
        StringAccum sa;
        sa << "error compiling " << name_ << " -- /" << pattern_ << "/\n";
        sa << "error offset: " << erroff << "\n";
        sa << "error message: " << errbuf;
        errh->fatal(sa.c_str());
    }

    if(!(preg_e_ = pcre_study(preg_, eflags_, &errbuf)) )
    {
		if (errbuf)
		{
			pcre_free(preg_);
			StringAccum sa;
			sa << "error pcre study " << name_ << " -- /" << pattern_ << "/\n";
			sa << "error message: " << errbuf;
			errh->fatal(sa.c_str());
		}
	}

	int res;
	if( (res = pcre_fullinfo(preg_, preg_e_, PCRE_INFO_MINLENGTH, &pminsize_)) < 0)
	{
		pcre_free_study(preg_e_);
		pcre_free(preg_);
		StringAccum sa;
		sa << "error pcre_fullinfo: " << res;
		errh->fatal(sa.c_str());
	}
	if( (res = pcre_fullinfo(preg_, preg_e_, PCRE_INFO_OPTIONS, &poptions_)) < 0)
	{
		pcre_free_study(preg_e_);
		pcre_free(preg_);
		StringAccum sa;
		sa << "error pcre_fullinfo: " << res;
		errh->fatal(sa.c_str());
	}

    mark_ = AppMarks::default_instance()->lookup_by_proto_name(name_.c_str());
    if (mark_ == AppMarks::empty_mark)
    {
        StringAccum sa;
        sa << "error looking up AppMark for protocol name: " << name_;
        errh->fatal(sa.c_str());
    }

    initialized = true;
}

bool
L7Pattern::matches(char* buffer, uint buflen)
{
	StringAccum sa;
	short ret = pcre_exec(preg_, preg_e_, buffer, buflen, 0, eflags_, NULL, 0);
	if ( ret < 0 && ret != PCRE_ERROR_NOMATCH) // we don't care about NOMATCH
	{
		switch(ret) {
			//~ case PCRE_ERROR_NOMATCH:
				//~ sa << "String did not match the pattern";
				//~ break;
			case PCRE_ERROR_NULL:
				sa << "Something was null";
				break;
			case PCRE_ERROR_BADOPTION:
				sa << "A bad option was passed";
				break;
			case PCRE_ERROR_BADMAGIC:
				sa << "Magic number bad (compiled re corrupt?)";
				break;
			case PCRE_ERROR_UNKNOWN_OPCODE:
				sa << "Something kooky in the compiled re";
				break;
			case PCRE_ERROR_NOMEMORY:
				sa << "Ran out of memory";
				break;
			default:
				sa << "Unknown error [" << ret << "]";
				break;
		}
	}  else if (ret == PCRE_ERROR_NOMATCH) { }
#if defined(CLICK_DEBUG_L7)
	else sa << "match found!";
#endif
	if (sa.length())
		click_chatter("L7Pattern::matches: %s", sa.c_str());
	return ret >= 0;
}


bool
L7Pattern::parse_pattern_string(const String& pattern_str, ErrorHandler* errh)
{
	if (pattern_str.length() == 0)
	{
		errh->fatal("empty patterns are not allowed");
	}

    String flags_str;
    int flags = pattern_str.find_left(String("/flags="));
    if (flags >= 0)
    {
		pattern_ = String(pattern_str.substring(0, flags - 1));
		
		if (!parse_flags(value(pattern_str.substring(flags + 7).c_str())))
		{
			return false;
		}
	} else {
		pattern_ = String(pattern_str.substring(0, pattern_str.length() -1));
	}
	
	click_chatter("[L7Pattern] added pattern: %s", pattern_.c_str());

	return true;
}


bool
L7Pattern::parse_flags(const String& line)
{
    String flag;
    cflags_ = 0;
    eflags_ = 0;
    
    for (int i=0; i<line.length(); ++i)
    {
        if (!isspace(line[i]))
            flag += line[i];
    
        if (isspace(line[i]) || i == line.length() - 1)
        {
            if (flag == "EXTENDED")
                cflags_ |= PCRE_EXTENDED;
            else if (flag == "ICASE")
                cflags_ |= PCRE_CASELESS;
            else if (flag == "NOSUB")
                cflags_ |= PCRE_NO_AUTO_CAPTURE;
            else if (flag == "NEWLINE")
                cflags_ |= PCRE_MULTILINE;
            else if (flag == "NOTBOL")
                eflags_ |= PCRE_NOTBOL;
            else if (flag == "NOTEOL")
                eflags_ |= PCRE_NOTEOL;
            else
            {
                StringAccum sa;
                sa << "Error: encountered unknown flag in pattern file \"" << flag << "\", skipping\n";
                click_chatter(sa.c_str());
                return false;
            }
            flag = "";
        }
    }
    return true;
}

String
L7Pattern::pre_process(const String& s)
{
    size_t len = s.length();
    char* result = new char[len + 1];
    size_t sindex = 0;
    size_t rindex = 0;
    
    while (sindex < len)
    {
        if (sindex + 3 < len && s[sindex] == '\\' && s[sindex + 1] == 'x' && isxdigit(s[sindex + 2]) && isxdigit(s[sindex + 3]))
        {
            result[rindex] = hex2dec(s[sindex + 2]) * 16 + hex2dec(s[sindex + 3]);
            
            StringAccum sa;
            switch (result[rindex])
            {
            case '$':
            case '(':
            case ')':
            case '*':
            case '+':
            case '.':
            case '?':
            case '[':
            case ']':
            case '^':
            case '|':
            case '{':
            case '}':
            case '\\':
                sa << "Warning: regexp contains a regexp control character, " << result[rindex];
                sa << ", in hex (\\x" << s[sindex + 2] << s[sindex + 3];
                sa << ".\nI recommend that you write this as " << result[rindex];
                sa << " or \\" << result[rindex] << " depending on what you meant." << "\n";
                break;
            case '\0':
                sa << "Warning: null (\\x00) in layer7 regexp. " << "A null terminates the regexp string!" << "\n";
                break;
            default:
                break;
            }
            sindex += 3; /* 4 total */

            click_chatter(sa.take_string().c_str());
        }
        else
            result[rindex] = s[sindex];
    
        ++sindex;
        ++rindex;
    }
    result[rindex] = '\0';
    String res = result;
    delete[] result;
    
    return res;
}


CLICK_ENDDECLS
#endif
