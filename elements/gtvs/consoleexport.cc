/*
 * consoleexport.{cc,hh} -- export data records to console
 *
 * Vito Piserchia
 *
 * Copyright (c) 2012-14444 Dreamlab Technologies AG
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of Dreamlab Technologies AG nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <click/config.h>
#include <click/error.hh>
#include <click/args.hh>
#include <click/confparse.hh>


#include "consoleexport.hh"

CLICK_DECLS

ConsoleExport::~ConsoleExport()
{
}

void* ConsoleExport::cast(const char* n)
{
    if (strcmp(n, "ConsoleExport") == 0)
        return static_cast<ConsoleExport*>(this);
    else if (strcmp(n, "DataExport") == 0)
        return static_cast<DataExport*>(this);
    else
        return Element::cast(n);
}

int ConsoleExport::configure(Vector<String>& conf, ErrorHandler* errh)
{
    class Element* next;
    if(Args(conf, this, errh)
        .read("NEXT", ElementCastArg("DataExport"), next)
        .complete() < 0 )
        return -1;
    
    if (next && !(next_ = static_cast<DataExport*>(next->cast("DataExport"))))
        return errh->error("NEXT must be a DataExport element");

    return 0;
}

void ConsoleExport::add_column(const Column& col)
{
    sa_ << col.name.c_str() << " " ;
}

void ConsoleExport::write(const String& val)
{
    sa_ << val.c_str() << " ";
}

void ConsoleExport::write_null()
{
    sa_ << "\\N ";
}

void ConsoleExport::done()
{
    click_chatter("%s\\n", sa_.c_str());
}

EXPORT_ELEMENT(ConsoleExport)

CLICK_ENDDECLS

