#ifndef CLICK_FLOWTABLE_HH
#define CLICK_FLOWTABLE_HH

#define HAS_MEMORY_POOL

#include <click/config.h>

#include <utility>
#include <stdint.h>

#include "hash.hh"
#include "flowtableiterators.hh"
#include "counter.hh"

#ifdef HAS_MEMORY_POOL
#include "memorypool.hh"
#endif


CLICK_DECLS

class FlowTable
{
    static const size_t default_max_flows = 1048576;
    static const size_t default_max_buckets = 1048576;
    static const size_t default_max_bucket_length = 10;
    static const size_t default_initial_buckets = 1024;
    static const size_t default_resizing_factor = 2;

#ifdef FLOWTABLE_STATS // Bucket structure with statistics members

    struct Bucket
    {
        Bucket(): list(0), max_len(0), cur_len(0)
        {}

        FlowListEntry* list;
        size_t max_len;
        size_t cur_len;

        Bucket& operator++()
        {
            ++cur_len;

            if (cur_len > max_len)
                max_len = cur_len;

            return *this;
        }

        Bucket& operator--()
        {
            --cur_len;

            return *this;
        }

        void reset_statistics()
        {
            cur_len = 0;
            max_len = 0;
        }

        void reset()
        {
            list = 0;
            reset_statistics();
        }

        void insert_head(FlowListEntry* elm);
    };

#else // Bucket structure without statistics members

    struct Bucket
    {
        Bucket(): list(0), cur_len(0) {}

        FlowListEntry* list;

        size_t cur_len;

        Bucket& operator++()
        {
            ++cur_len;

            return *this;
        }

        Bucket& operator--()
        {
            --cur_len;

            return *this;
        }

        void reset()
        {
            list = 0;
            cur_len = 0;
        }

        void insert_head(FlowListEntry* elm);
    };

#endif

public:
    typedef FlowTableIterator iterator;
    typedef FlowTableReverseIterator reverse_iterator;

    FlowTable();
    ~FlowTable();

    void set_max_flows(size_t max_flows)
    {
        max_flows_ = max_flows;
    }

    void set_max_buckets(size_t max_buckets)
    {
        max_buckets_ = max_buckets;
    }

    void set_max_bucket_length(size_t max_length)
    {
        max_bucket_length_ = max_length;
    }

    void set_dynamic_resizing(bool resizable)
    {
        resizable_ = resizable;
    }

    void set_initial_buckets(size_t initial_buckets)
    {
        initial_buckets_ = initial_buckets;
    }

    void set_resizing_factor(size_t resizing_factor)
    {
        resizing_factor_ = resizing_factor;
    }

    size_t size() const
    {
        return size_;
    }

    bool empty() const
    {
        return size_ == 0;
    }

    size_t buckets() const
    {
        return n_buckets_;
    }

#ifdef FLOWTABLE_STATS // Statistics member functions

    size_t bucket_cur_length(size_t idx) const
    {
        return buckets_[idx].cur_len;
    }

    size_t bucket_max_length() const
    {
        return max_buck_list_len_;
    }
    size_t bucket_max_length_index() const
    {
        return max_buck_list_len_index_;
    }

    size_t bucket_max_length(size_t idx) const
    {
        return buckets_[idx].max_len;
    }

    size_t max_size() const
    {
        return max_size_;
    }

    size_t num_finds() const
    {
        return counter_.num_samples();
    }

    uint64_t find_max() const
    {
        return counter_.max();
    }

    uint64_t find_min() const
    {
        return counter_.min();
    }

    uint64_t find_avg() const
    {
        return counter_.avg();
    }

    uint64_t find_e_avg() const
    {
        return counter_.e_avg();
    }

    uint64_t find_total() const
    {
        return counter_.total();
    }

    void reset_statistics()
    {
        max_buck_list_len_ = 0;

        for (size_t i=0; i<n_buckets_; ++i)
            buckets_[i].reset_statistics();

        counter_.reset();
    }

#endif

    iterator insert_not_existent(uint32_t host0, uint32_t host1, uint16_t port0, uint16_t port1, bool reverse, uint32_t aggregate, size_t num_states);
    iterator find(uint32_t host0, uint32_t host1, uint16_t port0, uint16_t port1);
    iterator remove(iterator& iter);
    reverse_iterator remove(reverse_iterator& iter);

    void clear();

    iterator begin() const
    {
        return iterator(lru_first_);
    }

    iterator end() const
    {
        return iterator(0);
    }

    reverse_iterator rbegin() const
    {
        return reverse_iterator(lru_last_);
    }

    reverse_iterator rend() const
    {
        return reverse_iterator(0);
    }
private:
    FlowTable(const FlowTable&);
    FlowTable& operator=(const FlowTable&);

    void allocate();
    void resize();
    void remove(FlowListEntry* entry);

    size_t bucket(uint32_t h0, uint32_t h1, uint16_t p0, uint16_t p1) const
    {
        uint32_t p = ((uint32_t)p0 << 16) | p1;

        return (static_cast<size_t>(hashword(h0, h1, p, 0))) % n_buckets_;
    }

    void move_to_bucket_front(size_t idx, FlowListEntry* cur);
    void move_to_lru_front(FlowListEntry* e);
private:
    size_t max_flows_;
    size_t max_buckets_;
    size_t max_bucket_length_;
    size_t initial_buckets_;
    size_t resizing_factor_;

    bool resizable_;

#ifdef HAS_MEMORY_POOL
    MemoryPool<FlowListEntry> pool_;
#endif
	
    Bucket* buckets_;

    size_t n_buckets_;
    size_t size_;

#ifdef FLOWTABLE_STATS

    size_t max_buck_list_len_;
    size_t max_buck_list_len_index_;
    size_t max_size_;

    TimeCounter counter_;

#endif

    uint64_t max_;

    FlowListEntry* lru_first_;
    FlowListEntry* lru_last_;

    friend class FlowTableIterator;
    friend class FlowTableReverseIterator;
};

CLICK_ENDDECLS
#endif

