#ifndef CLICK_APPMARKS_HH
#define CLICK_APPMARKS_HH

#include <click/element.hh>
#include <click/error.hh>
#include <click/vector.hh>
#include <click/hashtable.hh>

#include <click/glue.hh>

#include "appmark.hh"
#include "dataexport.hh"

CLICK_DECLS

//#define DEBUG_APPMARKS

/*
 * AppMarks holds the configuration of used AppMark-s
 */

class AppMarks: public Element
{
public:

    AppMarks();
    
    const char* class_name() const { return "AppMarks"; }

    int configure(Vector<String>& conf, ErrorHandler* errh);

    AppMark lookup(const String& n);
    AppMark lookup_by_class_name(const String& cn);
    AppMark lookup_by_class_group_names(const String& cn, const String& gn);
    AppMark lookup_by_proto_name(const String& pn);
    
    String resolve_mark(const AppMark& mark);
    String resolve_proto(const AppMark& mark);
    
    DataExport::Column column(const String& name, bool nullable = false);
    
    static AppMarks* default_instance()
    {
        if (AppMarks::default_instance_ == 0)
            ErrorHandler::default_handler()->fatal("AppMarks was not initialized! "
                "Did you insert it in the config file?");
        return AppMarks::default_instance_;
    }
    
    static const AppMark empty_mark;
    static const AppMark unknown_mark;
private:

    typedef HashTable<String, AppMark> MarksTable;
    typedef Vector<String> SVector;
    
    void register_mark(const String& s, ErrorHandler* errh);
    void dump_marks();
    
    MarksTable marks_;
    SVector classes_;
    SVector groups_;
    SVector protos_;

    static AppMarks* default_instance_;
};

template <> inline String to_string(AppMark val)
{
    return AppMarks::default_instance()->resolve_mark(val);
}

CLICK_ENDDECLS

#endif

