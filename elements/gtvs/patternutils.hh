#ifndef CLICK_PATTERNUTILS_HH
#define CLICK_PATTERNUTILS_HH

/*
 * patternutils.hh -- utils files for mangling the L7 patterns
 * 
 * Vito Piserchia
 *
 * Copyright (c) 2013-14 by Dreamlab Technologies AG
 * 
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of Dreamlab Technologies AG nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <pcre.h>
#include "l7patterninfo.hh"
#include "appmarks.hh"

CLICK_DECLS

namespace
{
	String find_pattern(const String& protocol_stro, L7PatternInfo* pinfo);
    String get_protocol_name(const String& line);
    String attribute(const String& line);
    bool is_comment(const String& line);
    String value(const String& line);
    int hex2dec(char c);
	String unescape_pattern(const String&);
}

namespace
{
    String find_pattern(const String& protocol, L7PatternInfo* pinfo)
    {
		
		String pattern;
		DynamicNameDB *db = (DynamicNameDB *)
			NameInfo::getdb(L7PatternInfo::T_L7_PATTERN, pinfo, MAX_PATTERN_LENGTH, false);
		
		if (!db)
		{
			StringAccum sa;
			sa << "Error: no pattern DB found";
			ErrorHandler::default_handler()->fatal(sa.c_str());
		} else {
			if ( !db->query(protocol, &pattern, MAX_PATTERN_LENGTH))
			{
				StringAccum sa;
				sa << "Error: no pattern definition for protocol " << protocol;
				ErrorHandler::default_handler()->fatal(sa.c_str());
			}
			//click_chatter("[find_pattern] found pattern for protocol %s %s", protocol.c_str(), pattern.c_str()); 
		}
		return pattern;
    }
    
    String get_protocol_name(const String& line)
    {
        String name;
        
        for (int i=0; i<line.length(); ++i)
        {
            if (!isspace(line[i]))
                name += line[i];
            else
                break;
        }
        return name;
    }
    
    // Returns true if the line (from a pattern file) is a comment
    bool is_comment(const String& line)
    {
        // blank lines are comments
        if (line.length() == 0)
            return true;
        
        // lines starting with # are comments
        if (line[0] == '#')
            return true;
        
        // lines with only whitespace are comments
        for (int i=0; i<line.length(); ++i)
            if (!isspace(line[i]))
                return false;
        
        return true;
    }
    
    String attribute(const String& line)
    {
        return line.substring(0, line.find_left('='));
    }
    
    // Returns, e.g. ".*foo" if the line is "userspace pattern=.*foo"
    String value(const String& line)
    {
        return line.substring(line.find_left('=') + 1);
    }
    
    // parse the compiling and matching flags
    // Returns true on sucess, false if any unrecognized flags were encountered
    
    int hex2dec(char c)
    {
        switch (c)
        {
        case '0'
        ...'9':
            return c - '0';
        case 'a'
        ...'f':
            return c - 'a' + 10;
        case 'A'
        ...'F':
            return c - 'A' + 10;
        default:
            StringAccum sa;
            sa << "Bad hex digit, " << String(c) << "  in regular expression!\n";
            ErrorHandler::default_handler()->fatal(sa.c_str());
            return -1;
        }
    }

	String unescape_pattern(const String& s)
	{
		size_t len = s.length();
		char* result = new char[len + 1];
		size_t sindex = 0;
		size_t rindex = 0;
		
		while (sindex < len)
		{
			if (sindex + 2 < len && s[sindex] == '\\')
			{
				sindex +=1 ;
			}
			result[rindex] = s[sindex];
			++rindex;
			++sindex;
		}
		result[rindex] = '\0';
		String res = result;
		delete[] result;

		return res;
	}

}

CLICK_ENDDECLS
#endif
