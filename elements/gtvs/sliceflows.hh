#ifndef CLICK_SLICEFLOWS_HH
#define CLICK_SLICEFLOWS_HH

#include <click/element.hh>
#include <clicknet/tcp.h>

#include "flowcache.hh"
#include "dataexport.hh"

CLICK_DECLS

class SliceFlows: public Element, FlowCache::FlowStateHolder
{
    uint32_t max_pkts;

    FlowCache* flow_cache;

    class FlowState;
    friend class FlowState;

public:
    SliceFlows(): flow_cache(0) {}
    ~SliceFlows() {}

    const char* class_name() const { return "SliceFlows"; }
    const char* port_count() const { return "1/1-2"; }
    const char* processing() const { return AGNOSTIC; }

    int configure(Vector<String>& conf, ErrorHandler* errh);

    Packet* simple_action(Packet* p);

    void write_header(DataExport&) const {}
    void write_flow_state(DataExport&, const BaseFlowState*) const {}
};

CLICK_ENDDECLS

#endif

