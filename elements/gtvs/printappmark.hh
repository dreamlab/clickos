#ifndef CLICK_MARKPRINT
#define CLICK_MARKPRINT

#include <click/element.hh>
#include <click/vector.hh>

#include "appmark.hh"

CLICK_DECLS

class PrintAppMark : public Element {
public:
        PrintAppMark();
        ~PrintAppMark();

        const char *class_name() const { return "PrintAppMark"; }
        const char *port_count() const { return PORTS_1_1; }
        const char *processing() const { return AGNOSTIC; }

        int configure(Vector<String> &,ErrorHandler*);
        Packet *simple_action(Packet *);

private:
        bool active_;
        String label_;
};

CLICK_ENDDECLS
#endif
