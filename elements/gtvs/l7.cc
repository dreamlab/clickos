/*
 * l7.{cc,hh} -- classify TCP/UDP flows using the l7-filter engine
 * 
 * Enrico Badino
 * Marco Canini
 * Sergio Mangialardi
 * 
 * Vito Piserchia
 *
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 * Copyright (c) 2013-14 by Dreamlab Technologies AG
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 * http://www.gnu.org/licenses/gpl.txt
 *
 */
/*
 * Portions of the file are taken from the userspace version of the
 * original l7-filter userspace version 0.3, distributed under the GPL license.
 * Copyright (c) 2006-2007 by Ethan Sommer <sommere@users.sf.net> and
 * Matthew Strait <quadong@users.sf.net>
 * http://l7-filter.sf.net
 */

/* Changes were made by Vito Piserchia to work with the ClickOS XEN miniOS kernel.*/


#include <click/config.h>
#include <click/error.hh>
#include <click/hashmap.hh>
#include <click/straccum.hh>
#include <click/confparse.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <click/packet_anno.hh>
#include <click/handlercall.hh>
#include <click/error.hh>
#include <click/nameinfo.hh>

#include "l7patterninfo.hh"
#include "appmark_anno.hh"
#include "l7.hh"
#include "nipquad.hh"

CLICK_DECLS

int
L7::configure(Vector<String>& conf, ErrorHandler* errh)
{
	String patterns;
	use_appmarks_ = true;
	Element* pinfo_element = 0;
	Element* af_element = 0;
	initialized_ = false;

	if (cp_va_kparse(conf, this, errh,
			"PROTOCOLS", cpkP+cpkM, cpString, &patterns,
			"PATTERN_INFO", cpkP, cpElement, &pinfo_element,
			"FLOWCACHE", cpkP, cpElement, &af_element,
			"MAX_PKTS", 0, cpUnsigned, &buffer_count_,
			"MAX_BYTES", 0, cpUnsigned, &byte_count_,
			"USE_APPMARKS", 0, cpBool, &use_appmarks_,
			cpEnd) < 0)
		return -1;

    if (af_element && !(flow_cache_ = (FlowCache*)(af_element->cast("FlowCache"))))
        return errh->error("FLOWCACHE must be a FlowCache element");

    if (!flow_cache_)
        flow_cache_ = FlowCache::upstream_instance(this);

    if (!flow_cache_)
        return errh->error("Initialization failure!"
				"no FLOWCACHE specified and neither a FlowCache element found on the upstream");

	flow_cache_->register_flow_state_holder(this, name());

	if ( !pinfo_element || !(pinfo_ = (L7PatternInfo*)(pinfo_element->cast("L7PatternInfo"))) )
		return errh->error("L7 element needs an L7PatternInfo");

	cp_spacevec(patterns, patternNames_);

	return 0;
}

int
L7::initialize(ErrorHandler* errh)
{
	if (initialized_)
		return 0;
	int nrules = 0;
	
	for (Vector<String>::iterator it = patternNames_.begin();
		it != patternNames_.end(); ++it)
	{
		if (add_pattern((*it), find_pattern((*it), pinfo_)))
		{
			++nrules;
		}
	}
	if (nrules < 1)
		errh->fatal("No valid rules, exiting");

	// DO NOT REMOVE!!
	for (Vector<L7Pattern>::iterator iter=patterns_.begin();
		iter != patterns_.end(); ++iter)
	{
		click_chatter("[L7] using pattern protocol: \"%s\" with mark: \"%s\"",
				iter->get_name().c_str(), AppMarks::default_instance()->resolve_mark(iter->get_mark()).c_str());
	}
	initialized_ = true;
	return 0;
}

void
L7::write_header(DataExport& exporter) const
{
	if (use_appmarks_) {
		exporter += AppMarks::default_instance()->column("L7Mark");
	} else {
		DataExport::Column::EnumType et;
		et.push_back("NC+" + String(buffer_count_));
		et.push_back("NC-" + String(buffer_count_));
		for (Vector<String>::const_iterator it = patternNames_.begin();
			it != patternNames_.end(); ++it)
		{
			et.push_back(*it);
		}
		exporter += column("L7Mark", et, false);
	}

	exporter += constraint("L7Mark", DataExport::Constraint::KEY);
}

void
L7::write_flow_state(DataExport& exporter, const BaseFlowState* fs_) const
{

	const FlowState* fs = static_cast<const FlowState*>(fs_);
	if (use_appmarks_) {
		exporter << *((const AppMark *) fs);
	} else {
		if (*fs == AppMarks::empty_mark)
			exporter << "NC-" + String(buffer_count_);
		else if (*fs == AppMarks::unknown_mark)
			exporter << "NC+" + String(buffer_count_);
		else
			exporter << AppMarks::default_instance()->resolve_proto(*fs);
	}
}

Packet*
L7::simple_action(Packet* p)
{
	FlowState* fs = flow_cache_->lookup_state<FlowState>(this, p);
	if (!fs )
	{
		fs = new FlowState();
		flow_cache_->state(this, p) = fs;

		if (!fs)
		{
			click_chatter("out of memory!");
			p->kill();

			return 0;
		}
	}

	fs->handle_packet(p, this);

	return p;
}

L7::FlowState::FlowState()
{
	packet_counter[0] = 0;
	packet_counter[1] = 0;
	lengthsofar[0] = 0;
	lengthsofar[1] = 0;
	buffer[0] = 0;
	buffer[1] = 0;

	set_mark(AppMarks::empty_mark);
}

L7::FlowState::~FlowState()
{
	delete [] buffer[0];
	delete [] buffer[1];
}

void
L7::FlowState::handle_packet(Packet* p, L7* parent)
{
	if (*this == AppMarks::empty_mark)
	{
		int thlen = 8; /* UDP header length */
		const click_ip* iph = p->ip_header();

		if (iph->ip_p == IP_PROTO_TCP)
			thlen = p->tcp_header()->th_off * 4;

		int dir = PAINT_ANNO(p);
		packet_counter[dir]++;

		int payload = p->length() - (p->ip_header_length() + thlen);
		
		if (payload > 0)
		{
			if (buffer[dir] == 0)
				buffer[dir] = new char[parent->byte_count_ + 1];
			
			const char* start = reinterpret_cast<const char*>(p->transport_header() + thlen);
			append_to_buffer(start, payload, parent->byte_count_, buffer[dir], lengthsofar[dir]);

			set_mark(parent->classify(buffer[dir], lengthsofar[dir]));
		}
		
		if (*this != AppMarks::empty_mark)
		{
			SET_APPMARK_ANNO(p, get_mark());
			
			if (packet_counter[dir] > parent->max_pkt_match_)
				parent->max_pkt_match_ = packet_counter[dir];
		}
		else if (packet_counter[dir] >= parent->buffer_count_ || lengthsofar[dir] == parent->byte_count_)
		{
			set_mark(AppMarks::unknown_mark);
		}

		if (*this != AppMarks::empty_mark)
		{
			delete [] buffer[0];
			delete [] buffer[1];
			buffer[0] = buffer[1] = 0;
		}
	}

	SET_APPMARK_ANNO(p, get_mark());
}

void
L7::FlowState::append_to_buffer(const char* app_data, int appdatalen, uint32_t& max_bytes, char* buffer, uint32_t& lengthsofar)
{
	uint32_t length = 0;
	uint32_t oldlength = lengthsofar;
	uint32_t ln = max_bytes - lengthsofar;

	/*
	 * Strip nulls.  Add it to the end of the current data.
	 */
	uint32_t len = static_cast<uint32_t>(appdatalen);
	
	for (uint32_t i=0; length<ln && i<len; ++i)
	{
		if (app_data[i] != '\0')
		{
			buffer[length + oldlength] = app_data[i];
			++length;
		}
	}

	buffer[length + oldlength] = '\0';
	lengthsofar += length;
}

AppMark
L7::classify(char* buffer, uint len)
{
	Vector<L7Pattern>::iterator end = patterns_.end();
	for (Vector<L7Pattern>::iterator iter=patterns_.begin();
		iter != end; ++iter)
	{
		if (iter->matches(buffer, len))
		{
			return iter->get_mark();
		}
	}

	return AppMarks::empty_mark;
}

// Returns true on sucess, false on failure
bool L7::add_pattern(const String& name, const String& pattern_str)
{
	L7Pattern pattern = L7Pattern(name, pattern_str);
	if (pattern.initialized)
		patterns_.push_back(pattern);

	return pattern.initialized;
}


/*
 * HANDLERS
 */

String
L7::read_max_pkt_match(Element* e, void*)
{
	L7* cf = static_cast <L7*>(e);
	
	return String(cf->max_pkt_match_);
}

void
L7::add_handlers()
{
	add_read_handler("max_pkt_match", read_max_pkt_match, 0);
}

ELEMENT_REQUIRES(FlowCache L7PatternInfo)
EXPORT_ELEMENT(L7)

CLICK_ENDDECLS

