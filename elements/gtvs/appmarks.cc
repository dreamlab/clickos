/*
 * appmarks.{cc,hh} -- holds the configuration of used AppMark-s
 * 
 * Marco Canini
 *
 * Copyright (c) 2008-09 by University of Genova - DIST - TNT laboratory
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: appmarks.cc 2214 2009-05-06 18:04:23Z marco $
 */

#include <click/config.h>
#include <click/error.hh>
#include <click/hashmap.hh>
#include <click/straccum.hh>
#include <click/confparse.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <click/packet_anno.hh>
#include <click/handlercall.hh>

#include <click/glue.hh>

#include "appmarks.hh"

CLICK_DECLS

namespace {
    void string_split(const String &str, int delim, Vector<String> &v);
    const char *skip_token(const char *s, const char *end, int delim);
}

const AppMark AppMarks::unknown_mark(1, 0);
const AppMark AppMarks::empty_mark(1, 1);

AppMarks* AppMarks::default_instance_;

AppMarks::AppMarks()
{
    assert(AppMarks::default_instance_ == 0);
    AppMarks::default_instance_ = this;
}

int AppMarks::configure(Vector<String>& conf, ErrorHandler* errh)
{
    register_mark("UNKNOWN:empty:", errh);
    assert(resolve_mark(unknown_mark) == "UNKNOWN::");
    assert(resolve_mark(empty_mark) == "UNKNOWN:empty:");
    for (int argno = 0; argno < conf.size(); argno++) {
        register_mark(conf[argno], errh);
    }

#ifdef DEBUG_APPMARKS
    dump_marks();
#endif

    return 0;
}

AppMark AppMarks::lookup(const String& n)
{
    MarksTable::const_iterator it = marks_.find(n);
    if (it == marks_.end())
        return AppMarks::empty_mark;
    return it->second;
}

AppMark AppMarks::lookup_by_class_name(const String& cn)
{
    return lookup(cn + "::");
}

AppMark AppMarks::lookup_by_class_group_names(const String& cn, const String& gn)
{
    return lookup(cn + ":" + gn + ":");
}

/** @brief Finds an AppMark for the given proto name. If multiple marks were registered
 * with the given proto name, returns the first one.
 * 
 * Returns AppMarks::empty_mark if not found.
 * NOTE: This method is only meant to be run at initialization time. */
AppMark AppMarks::lookup_by_proto_name(const String& pn)
{
	uint16_t p = 1;
    SVector::const_iterator it;
    
    for (it = protos_.begin();
        it != protos_.end(); ++it, ++p)
    {
		if (*it == pn)
            break;
    }
    if (it == protos_.end() || *it != pn)
        return AppMarks::empty_mark;
    
    for (MarksTable::const_iterator it2 = marks_.begin();
        it2 != marks_.end(); ++it2)
    {
        const AppMark& m = it2->second;
        if (m.get_proto() == p)
            return m;
    }
    assert(false);
    return AppMarks::empty_mark;
}

String AppMarks::resolve_mark(const AppMark& mark)
{
    StringAccum sa;
    assert(mark.get_class() > 0);
#ifdef DEBUG_APPMARKS
    if (mark.get_class() > classes_.size())
        return resolve_mark(AppMarks::unknown_mark);
#endif
    sa << classes_[mark.get_class() - 1] << ':';

#ifdef DEBUG_APPMARKS
    if (mark.get_group() > groups_.size())
        return resolve_mark(AppMarks::unknown_mark);
#endif
    if (mark.get_group() > 0)
        sa << groups_[mark.get_group() - 1];
    sa << ':';

#ifdef DEBUG_APPMARKS
    if (mark.get_proto() > protos_.size())
        return resolve_mark(AppMarks::unknown_mark);
#endif
    if (mark.get_proto() > 0)
        sa << protos_[mark.get_proto() - 1];
    return sa.take_string();
}

String AppMarks::resolve_proto(const AppMark& mark)
{
#ifdef DEBUG_APPMARKS
    if (mark.get_proto() > protos_.size())
        return "NC";
#endif
    if (mark.get_proto() > 0)
        return protos_[mark.get_proto() - 1];
    return "NC";
}

/** @brief Creates an enum column with the defined app marks.
 * 
 * TODO for performance and space-saving we purposes might want to avoid the
 * cost of resolving an AppMark before exporting it. So we should seek for a
 * flexible solution that allows us to export either the AppMark code or its
 * string representation. This configuration should happen in AppMarks.
 */
DataExport::Column AppMarks::column(const String& name, bool nullable)
{
    DataExport::Column::EnumType et;
    for (MarksTable::const_iterator it = marks_.begin();
        it != marks_.end(); ++it)
    {
        et.push_back(it->first);
    }

    return ::column(name, et, nullable);
}

/** @brief Registers a new application mark represented with the string @a str
 * 
 * It associates the strings "<cn>::", "<cn>:<gn>:" and "<cn>:<gn>:<pn>" with 
 * a new AppMark instance and adds them to the marks table.
 * <cn> is the class name component, <gn> the group name and <pn> the proto name.
 * The string "<cn>:<gn>:" is added only if @a str contains a non empty <gn>.
 * Similarly, "<cn>:<gn>:<pn>" is added if @a str defines a non empty <pn>.
 * The member vectors classes_, groups_ and protos_ are also updated.
 * The code associated to each class is 1 plus the index given to the class in the
 * classes_ array. The same applies from groups and protos. */
void AppMarks::register_mark(const String& s, ErrorHandler* errh)
{
    SVector mv;
    string_split(s, ':', mv);
    if (mv.size() == 2)
        mv.push_back("");
    
    if (mv.size() != 3)
    {
        errh->error("Mark %s is not in the form class:group:proto.", s.c_str());
        return;
    }
    
    String cn = mv[0];
    String gn = mv[1];
    String pn = mv[2];
    if (cn.length() == 0)
    {
        errh->error("Mark %s doesn't specify a class name.", s.c_str());
        return;
    }
    
    MarksTable::const_iterator it = marks_.find(cn + ":" + gn + ":" + pn);
    if (it != marks_.end())
    {
        errh->error("Mark %s already defined.", s.c_str());
        return;
    }
    
    //StringAccum sa;
    //sa << "--- " << s.c_str() << "\n";
	//ErrorHandler::default_handler()->message(sa.c_str());

    /* Adds <cn>:: to marks_ and <cn> to classes_ */
    it = marks_.find(cn + "::");
    if (it == marks_.end())
    {
        classes_.push_back(cn);
        AppMark m;
        m.set_class(classes_.size());
        marks_[cn + "::"] = m;
        //StringAccum sa;
        //sa << "marks_[\"" << cn.c_str() << + "::\"] = " << (int) m.get_class() << " ";
        //sa << (int) m.get_group() << " ";
        //sa << (int) m.get_proto() << "\n";
        //errh->message(sa.c_str());
    }

    if (gn.length() > 0)
    {
        /* Adds <cn>:<gn>: to marks_ and <gn> to groups_ */
        it = marks_.find(cn + ":" + gn + ":");
        if (it == marks_.end())
        {
            groups_.push_back(gn);
            AppMark m;
            m.set_class(marks_[cn + "::"].get_class());
            m.set_group(groups_.size());
            marks_[cn + ":" + gn + ":"] = m;
            StringAccum sa;
            //sa << "marks_[\"" << cn.c_str() << + ":" << gn.c_str() << ":\"] = " << (int) m.get_class() << " ";
            //sa << (int) m.get_group() << " ";
            //sa << (int) m.get_proto() << "\n";
            //errh->message(sa.c_str());
        }
    }
    
    if (pn.length() > 0)
    {
        /* Adds <cn>:<gn>:<pn> to marks_ and <pn> to protos_ */
        protos_.push_back(pn);
        AppMark m;
        m.set_class(marks_[cn + "::"].get_class());
        //StringAccum sa
        //sa << gn.c_str() << "\n";
        if (gn.length() > 0)
            m.set_group(marks_[cn + ":" + gn + ":"].get_group());
        m.set_proto(protos_.size());
        marks_[cn + ":" + gn + ":" + pn] = m;
        //sa << "marks_[\"" << cn.c_str() << + ":" << gn.c_str() << ":" << pn.c_str() << "\"] = " << (int) m.get_class() << " ";
        //sa << (int) m.get_group() << " ";
        //sa << (int) m.get_proto() << "\n";
        //errh->message(sa.c_str());
    }
}

void AppMarks::dump_marks()
{
    StringAccum os;
    for (SVector::const_iterator it = classes_.begin();
        it != classes_.end(); ++it)
    {
        os << "class: " << it->c_str() << "\n";
    }
    for (SVector::const_iterator it = groups_.begin();
        it != groups_.end(); ++it)
    {
        os << "group: " << it->c_str() << "\n";
    }
    for (SVector::const_iterator it = protos_.begin();
        it != protos_.end(); ++it)
    {
        os << "proto: " << it->c_str() << "\n";
    }
    for (MarksTable::const_iterator it = marks_.begin();
        it != marks_.end(); ++it)
    {
        const AppMark& m = it->second;
        os << it->first.c_str() << " ";
        os << (int) m.get_class() << " ";
        os << (int) m.get_group() << " ";
        os << (int) m.get_proto() << " ";
        os << "res: `" << resolve_mark(m).c_str() << '`' << "\n";
        
        assert(resolve_mark(m) == it->first);
    }
    click_chatter(os.c_str());

}

namespace {
    void string_split(const String &str, int delim, Vector<String> &v)
    {
      if (str.length() == 0)
        return;
    
      const char *s = str.data();
      const char *end = str.end();
      while (s < end) {
        const char *t = skip_token(s, end, delim);
        v.push_back(str.substring(s, t));
        s = t + 1;
      }
    }
    
    const char *skip_token(const char *s, const char *end, int delim)
    {
      while (s < end && *s != delim) { s++; }
      return s;
    }
}    

EXPORT_ELEMENT(AppMarks)

#include <click/vector.cc>

CLICK_ENDDECLS

