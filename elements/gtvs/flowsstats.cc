/*
 * flowsstats.{cc,hh} -- collect a number of TCP/UDP flow statistics
 * 
 * Enrico Badino
 * Marco Canini
 * Sergio Mangialardi
 *
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: flowsstats.cc 2211 2009-05-06 18:01:53Z marco $
 */

#include <click/config.h>
#include <click/error.hh>
#include <click/hashmap.hh>
#include <click/straccum.hh>
#include <click/confparse.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <click/packet_anno.hh>
#include <click/handlercall.hh>

#include "flowsstats.hh"
#include "nipquad.hh"

CLICK_DECLS

class FlowsStats::TCPOptParser
{
public:
    uint16_t mss;
    uint8_t wscale;

    TCPOptParser() : mss(0), wscale(0) {}
    void parse(const click_tcp* tcph);
    void parse(const uint8_t* opt, int opt_len);
};

void FlowsStats::TCPOptParser::parse(const click_tcp* tcph)
{
    parse(reinterpret_cast<const uint8_t*>(tcph + 1), (tcph->th_off * 4) - sizeof(click_tcp));
}

void FlowsStats::TCPOptParser::parse(const uint8_t* opt, int opt_len)
{
    const uint8_t *end_opt = opt + opt_len;

    while (opt < end_opt)
        switch (*opt)
        {
        case TCPOPT_EOL:
            goto done;
        case TCPOPT_NOP:
            opt++;
            break;
        case TCPOPT_MAXSEG:
            if (opt + opt[1] > end_opt || opt[1] != TCPOLEN_MAXSEG)
                goto bad_opt;
            mss = ((opt[2] << 8) | opt[3]);
            opt += TCPOLEN_MAXSEG;
            break;
        case TCPOPT_WSCALE:
            if (opt + opt[1] > end_opt || opt[1] != TCPOLEN_WSCALE)
                goto bad_opt;
            wscale = opt[2];
            opt += TCPOLEN_WSCALE;
            break;
        case TCPOPT_SACK_PERMITTED:
            if (opt + opt[1] > end_opt || opt[1] != TCPOLEN_SACK_PERMITTED)
                goto bad_opt;
            opt += TCPOLEN_SACK_PERMITTED;
            break;
        case TCPOPT_SACK:
        {
            if (opt + opt[1] > end_opt || (opt[1] % 8 != 2))
                goto bad_opt;

            const uint8_t *end_sack = opt + opt[1];

            for (opt += 2; opt < end_sack; opt += 8)
            {
            }
            break;
        }
        case TCPOPT_TIMESTAMP:
        {
            if (opt + opt[1] > end_opt || opt[1] != TCPOLEN_TIMESTAMP)
                goto bad_opt;
            opt += TCPOLEN_TIMESTAMP;
            break;
        }
        default:
        {
            if (opt + opt[1] > end_opt || opt[1] < 2)
                goto bad_opt;
            const uint8_t *end_this_opt = opt + opt[1];
            for (opt += 2; opt < end_this_opt; opt++)
            {
            }
            break;
        }
        }

    done: bad_opt: return;
}

class FlowsStats::FlowState: public BaseFlowState
{
public:
    uint64_t payload_bytes[2];
    uint8_t tcp_flags[2];
    uint16_t mss;

    FlowState();
    ~FlowState();

    void handle_packet(Packet* p);
};

FlowsStats::FlowsStats(): flow_cache(0)
{
}

FlowsStats::~FlowsStats()
{
}

int FlowsStats::configure(Vector<String>& /*conf*/, ErrorHandler* errh)
{
    flow_cache = FlowCache::upstream_instance(this);

    if (!flow_cache)
        return errh->error("Initialization failure!");

    flow_cache->register_flow_state_holder(this);

    return 0;
}

Packet* FlowsStats::simple_action(Packet* p)
{
    FlowState* fs = static_cast<FlowState*>(flow_cache->state(this, p));

    if (!fs)
    {
        fs = new FlowState();
        flow_cache->state(this, p) = fs;

        if (!fs)
        {
            click_chatter("out of memory!");
            p->kill();

            return 0;
        }
    }
    fs->handle_packet(p);

    return p;
}

void FlowsStats::write_header(DataExport& exporter) const
{
    exporter += column<uint64_t>("PayloadBytes0", false);
    exporter += column<uint64_t>("PayloadBytes1", false);
    exporter += column<uint8_t>("TcpFlags0", false);
    exporter += column<uint8_t>("TcpFlags1", false);
    exporter += column<uint16_t>("MSS", false);
}

void FlowsStats::write_flow_state(DataExport& exporter, const BaseFlowState* fs_) const
{
    const FlowState* fs = static_cast<const FlowState*>(fs_);
    
    exporter << fs->payload_bytes[0] << fs->payload_bytes[1];
    exporter << fs->tcp_flags[0] << fs->tcp_flags[1];
    exporter << fs->mss;
}

FlowsStats::FlowState::FlowState()
{
    payload_bytes[0] = 0;
    payload_bytes[1] = 0;
    tcp_flags[0] = 0;
    tcp_flags[1] = 0;
    mss = 0;
}

FlowsStats::FlowState::~FlowState()
{
}

void FlowsStats::FlowState::handle_packet(Packet* p)
{
    int thlen = 8; /* UDP header length */
    const click_ip *iph = p->ip_header();
    const click_tcp *tcph = p->tcp_header();
    int dir = PAINT_ANNO(p);

    if (iph->ip_p == IP_PROTO_TCP)
    {
        thlen = tcph->th_off * 4;
        tcp_flags[dir] |= tcph->th_flags;

        if (tcph->th_flags & TH_SYN)
        {
            TCPOptParser optp;
            optp.parse(tcph);
            if (mss > optp.mss || mss == 0)
                mss = optp.mss;

            /*
             if (pkts[dir] > 0)
             click_chatter("%{element}: warning: SYN packet received after flow started %lu %d", parent, AGGREGATE_ANNO(p), dir);
             */
        }
    }

    int len = ntohs(iph->ip_len);

    int payload = len - (p->ip_header_length() + thlen);

    payload_bytes[dir] += payload;
}

ELEMENT_REQUIRES(FlowCache)
EXPORT_ELEMENT(FlowsStats)

#include <click/vector.cc>

CLICK_ENDDECLS

