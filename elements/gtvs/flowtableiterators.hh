#ifndef CLICK_FLOWTABLEITERATORS_HH
#define CLICK_FLOWTABLEITERATORS_HH

CLICK_DECLS


class FCFlowInfo;
class FlowTable;
struct FlowListEntry;

class FlowTableIterator
{
public:
    FlowTableIterator(const FlowTableIterator& other);
    FlowTableIterator& operator=(const FlowTableIterator& other);
    FlowTableIterator operator++(int);
    FlowTableIterator& operator++();
    FlowTableIterator operator--(int);
    FlowTableIterator& operator--();
    FCFlowInfo& operator*();
    const FCFlowInfo& operator*() const;
    FCFlowInfo* operator->();
    const FCFlowInfo* operator->() const;
    bool operator==(const FlowTableIterator& o) const;
    bool operator!=(const FlowTableIterator& o) const;
private:
    FlowListEntry* entry_;
    FlowTableIterator(FlowListEntry* entry);
    friend class FlowTable;
};

class FlowTableReverseIterator
{
public:
    FlowTableReverseIterator(const FlowTableReverseIterator& other);
    FlowTableReverseIterator& operator=(const FlowTableReverseIterator& other);
    FlowTableReverseIterator operator++(int);
    FlowTableReverseIterator& operator++();
    FlowTableReverseIterator operator--(int);
    FlowTableReverseIterator& operator--();
    FCFlowInfo& operator*();
    const FCFlowInfo& operator*() const;
    FCFlowInfo* operator->();
    const FCFlowInfo* operator->() const;
    bool operator==(const FlowTableReverseIterator& o) const;
    bool operator!=(const FlowTableReverseIterator& o) const;
private:
    FlowListEntry* entry_;
    FlowTableReverseIterator(FlowListEntry* entry);
    friend class FlowTable;
};

CLICK_ENDDECLS
#endif

