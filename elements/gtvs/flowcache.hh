// -*- c-basic-offset: 4 -*-
#ifndef CLICK_FLOWCACHE_HH
#define CLICK_FLOWCACHE_HH

#include <click/element.hh>
#include <clicknet/tcp.h>
#include <click/packet_anno.hh>
#include <click/straccum.hh>

// Enable this to compute and export the table statistics
#define FLOWTABLE_STATS

// Enable this to compile the debug code for this element
#define FLOWCACHE_DEBUG

#include "flowcacheinfo.hh"
#include "dataexport.hh"
#include "flowtable.hh"

#include "nipquad.hh"
               
CLICK_DECLS

/*
=c

FlowCache([I<KEYWORDS>])

=s tnt

aggregates IP packets into flows and maintains per-flow state information

=d                                            

FlowCache monitors TCP and UDP flows, setting the aggregate annotation
on every passing packet to a flow number, setting the flow info annotation to a
flow state pointer, and the paint annotation to a direction indication.
Non-TCP/UDP packets and short packets are emitted on output 1, or dropped if
there is no output 1.
                                    
FlowCache uses source and destination addresses and source and
destination ports to distinguish flows. Reply packets get the same flow
number, but a different paint annotation. Old flows die after a configurable
timeout, after which new packets with the same addresses and ports get a new
flow number. UDP, active TCP, and completed TCP flows have different timeouts.

Flow numbers are assigned sequentially, starting from 1. Different flows get
different numbers. Paint annotations are set to 0 or 1, depending on whether
packets are on the forward or reverse subflow. (The first packet seen on each
flow gets paint color 0; reply packets get paint color 1.) The single
exception is when the first packet seen is a TCP packet with SYN+ACK flags
set; in this case this is assumed to be the reverse subflow and therefore is
assigned with paint 1.

Keywords are:

=over 8

=item TCP_TIMEOUT

The timeout for active TCP flows, in seconds. Default is 1 minute.

=item TCP_DONE_TIMEOUT

The timeout for just initiated and completed TCP flows, in seconds. A just
initiated TCP flows has seen SYN packets up to a successful tripe hand-shake.
A completed TCP flow has seen FIN flags on both subflows. Default is 30 seconds.

=item UDP_TIMEOUT

The timeout for UDP connections, in seconds. Default is 1 minute.

=item WRRI_TIMEOUT

The worth reporting timeout for flows, in seconds, Default is 300 seconds.

=item REAP

The garbage collection interval. Default is 20 minutes of packet time.

=item WRRI

The worth record reporting interval. Default is disabled (0). When greater
than 0, it sets the interval (of packet time) for exporting flow records
that are worth reporting (active and not yet reported for the past
WRRI_TIMEOUT seconds).

=item DATAEXPORTER

A valid DATAEXPORTER element which will be used to export flow records.
If not provided, flow records are not exported.

=item STATS

The statistics reporting interval. Default is disabled (0).

=item DOTTEDIP

Boolean. If true, IP addresses are exported in the dot notation, otherwise
they are exported as unsigned integers. The default is false.

=item USE_PAINTANNO

Boolean. If true, use the PAINT ANNOTATION as the flow direction. The default is false.

=item ANNO

Integer. Use the packet's PAINT annotation by default, but the ANNO argument can
specify any one-byte annotation.

=back

=h clear write-only                

Clears all flow information. Future packets will get new aggregate annotation
values. If DATAEXPORT is set, all remaining flows are first exported and then
cleared.

=h next_flow_id read-only

Returns the next flow number (this value represents the flow count minus 1).

=h tcp_active_flows read-only

Returns the number of active TCP flows.

=h udp_active_flows read-only

Returns the number of active UDP flows.

=h tcp_map_nbuckets read-only

Returns the number of buckets of the TCP flow hashtable.

=h udp_map_nbuckets read-only

Returns the number of buckets of the UDP flow hashtable.

=e

This configuration collects a number of statistics for each flow in a trace,
and outputs them on standard output.

ex :: FileExport(-);
FromDump(tracefile.dump, STOP true, FORCE_IP true)
-> fc :: FlowCache(DATAEXPORTER ex)
-> FlowsStats
-> Discard;
DriverManager(wait, write fc.clear)

The DriverManager element waits for FromDump to request a driver stop, then
calls the C<fc.clear> handler to export any remaining flow information.

=a

FlowsStats, DriverManager
*/


class FlowCache: public Element
{
public:
    FlowCache();
    ~FlowCache();

    const char* class_name() const { return "FlowCache"; }
    void* cast(const char* n);
    const char* port_count() const { return "1/1-2"; }
    const char* processing() const { return "a/ah"; }

    int configure(Vector<String>& conf, ErrorHandler* errh);
    int initialize(ErrorHandler* errh);

    void add_handlers();

    void cleanup(CleanupStage);
    void push(int, Packet*);
    Packet* pull(int);

    class Flow;

    class FlowStateHolder
    {
        int id_;
        int columns_;
    public:
        FlowStateHolder(): id_(-1), columns_(0) {}
        virtual ~FlowStateHolder() {}

        int get_id() const { return id_; }
        int get_columns() const { return columns_; }

        virtual void write_header(DataExport& exporter) const = 0;
        virtual void write_flow_state(DataExport& exporter, const BaseFlowState* fs) const = 0;

        virtual void pre_write_flow_state(BaseFlowState*) {}
        virtual void post_write_flow_state(BaseFlowState*) {}

        virtual bool ignore_null_state() const { return false; }
        
        //virtual void export_flow_state() {}
        virtual void flow_over(const Flow&, BaseFlowState*) {}
        
    private:

        void set_id(int id) { id_ = id; }
        void set_columns(int columns) { columns_ = columns; }
		
        friend class FlowCache;
        friend class FlowCache::Flow;
    };

    class Flow
    {
    public:
        Flow(const FCFlowInfo& fi, uint8_t ip_p, bool dotted_ip,
            bool active, bool offline): fi_(fi), ip_p_(ip_p), dotted_ip_(dotted_ip),
            offline_(offline), active_(active) {}

        uint32_t src_ip() const
        {
            return !fi_._reverse ? fi_.host0 : fi_.host1;
        }

        uint32_t dst_ip() const
        {
            return !fi_._reverse ? fi_.host1 : fi_.host0;
        }

        uint16_t src_port() const
        {
            return !fi_._reverse ? fi_.port0 : fi_.port1;
        }

        uint16_t dst_port() const
        {
            return !fi_._reverse ? fi_.port1 : fi_.port0;
        }

        uint32_t host0() const
        {
            return fi_.host0;
        }

        uint32_t host1() const
        {
            return fi_.host1;
        }

        uint32_t port0() const
        {
            return fi_.port0;
        }

        uint32_t port1() const
        {
           return fi_.port1;
        }

        uint8_t ip_proto() const { return ip_p_; }
        bool is_tcp() const { return ip_p_ == IP_PROTO_TCP; }
        bool is_udp() const { return ip_p_ == IP_PROTO_UDP; }

        Timestamp first_pkt_ts() const { return fi_._first_timestamp; }

        Timestamp last_pkt_ts() const { return fi_._last_timestamp; }

        uint32_t pkts() const { return fi_.pkts[0] + fi_.pkts[1]; }
        uint32_t pkts(int dir) const { return fi_.pkts[dir]; }

        uint64_t bytes() const { return fi_.bytes[0] + fi_.bytes[1]; }
        uint64_t bytes(int dir) const { return fi_.bytes[dir]; }

        bool is_ths_completed() const { return fi_.ths_state == 2; }

        void write(DataExport& exporter) const;
        static void write_header(DataExport& exporter, bool dotted_ip, bool offline);

        template<typename T>
        inline const T* lookup_state(FlowStateHolder* fsh) const
        {
            int id = fsh->get_id();

            return static_cast<const T*>(fi_.state(id));
        }
        
        static String s(const Flow& flow)
        {
			StringAccum sa;
			uint32_t src = ntohl(flow.src_ip());
			if (flow.dotted_ip_)
			{       
				StringAccum ss;
				ss.snprintf(15, "%u.%u.%u.%u", NIPQUAD(src));
				sa << ss.take_string();
			}
			else    
				sa << src;
				
			sa << ":" << ntohs(flow.src_port());
			
			sa  << " - ";

			uint32_t dst = ntohl(flow.dst_ip());
			if (flow.dotted_ip_)
			{   
				StringAccum ss;
				ss.snprintf(15, "%u.%u.%u.%u", NIPQUAD(dst));
				sa << ss.take_string();
			}
			else
				sa << dst;

			sa << ":" << ntohs(flow.dst_port());
			if (flow.ip_proto() == IP_PROTO_TCP)
				sa << " tcp";
			else
				sa << " udp";

			return sa.c_str();
		}

    private:
        const FCFlowInfo& fi_;
        uint8_t ip_p_;
        bool dotted_ip_;
        bool offline_;
        bool active_;
    };

    void register_flow_state_holder(FlowStateHolder* fsh, const String& name = "");

    inline const Flow lookup_flow(Packet* p) const
    {
        FCFlowInfo* ti = reinterpret_cast<FCFlowInfo*>(FLOW_INFO_ANNO(p));
        if (!ti)
            click_chatter("Not valid FLOW_INFO annotation");

        return Flow(*ti, p->ip_header()->ip_p, dotted_ip_, true, offline_);
    }


    // TODO: deprecate!
    inline BaseFlowState*& state(FlowStateHolder* fsh, Packet* p)
    {
        FCFlowInfo* ti = reinterpret_cast<FCFlowInfo*>(FLOW_INFO_ANNO(p));
        if (!ti)
            click_chatter("Not valid FLOW_INFO annotation");

        int id = fsh->get_id();
        if (id < 0)
            click_chatter("Not valid state index");

        return ti->state(id);
    }

    template<typename T>
    inline T* lookup_state(FlowStateHolder* fsh, Packet* p)
    {
        FCFlowInfo* ti = reinterpret_cast<FCFlowInfo*>(FLOW_INFO_ANNO(p));
        if (!ti)
            click_chatter("Not valid FLOW_INFO annotation");

        int id = fsh->get_id();
        if (id < 0)
            click_chatter("Not valid state index");

        return static_cast<T*>(ti->state(id));
    }

    int tcp_timeout() const { return _tcp_timeout; }
    int udp_timeout() const { return _udp_timeout; }

    static FlowCache* upstream_instance(Element* e);

private:
    FlowTable _tcp_map;
    FlowTable _udp_map;

    bool dotted_ip_;
    bool offline_;

    uint32_t _next;
    unsigned int _active_sec;
    unsigned int _gc_sec;
    unsigned int _stats_sec;
    unsigned int wrri_sec_;

    int _tcp_timeout;
    int _tcp_done_timeout;
    int _udp_timeout;
    int wrri_timeout_;
    int _smallest_timeout;

    unsigned int _gc_interval;
    unsigned int _stats_interval;
    unsigned int wrri_interval_;

    bool _handle_icmp_errors : 1;
    bool _timestamp_warning : 1;
    
    uint8_t _anno;
    bool _use_anno;

    DataExport* exporter_;

    bool initialized;
    Vector<FlowStateHolder*> flow_state_holders;

#if defined(FLOWCACHE_DEBUG) || defined(FLOWTABLE_STATS)
    ErrorHandler *logger_;
#endif

    static const click_ip* icmp_encapsulated_header(const Packet* p);

    void clean_map(FlowTable& table);
    void reap_map(FlowTable& table, uint32_t timeout, uint32_t done_timeout);
    void reap();
    void wrri();
    void wrri_map(FlowTable& table, uint32_t timeout, uint32_t exp_timeout);

    void log_stats();

    int relevant_timeout(const FCFlowInfo& finfo, const FlowTable& m) const;
    void flow_start_hook(const Packet* p, const click_ip* iph, FCFlowInfo& finfo);
    void packet_emit_hook(const Packet* p, const click_ip* iph, int paint, FCFlowInfo& finfo);
    void write_flowinfo(DataExport& exporter, const FCFlowInfo& finfo, uint8_t ip_p, bool active);
    void notify_flow_over(const FCFlowInfo& finfo, uint8_t ip_p, bool active);
    FlowTable::iterator find_flow_info(uint32_t host0, uint32_t host1, uint16_t port0, uint16_t port1, FlowTable& m, bool flipped, Packet* p);

    enum
    {
        ACT_EMIT, ACT_DROP, ACT_NONE
    };

    int handle_packet(Packet* p);

    inline uint8_t which_ip_proto(const FlowTable& table) const;

    static int write_handler(const String&, Element* e, void* thunk, ErrorHandler*);
    static int export_data(const String&, Element* e, void* thunk, ErrorHandler*);
    static String read_next_flow_id(Element* e, void*);

    static String read_tcp_map_nbuckets(Element* e, void*);
    static String read_udp_map_nbuckets(Element* e, void*);
    static String read_tcp_active_flows(Element* e, void*);
    static String read_udp_active_flows(Element* e, void*);
    
#ifdef FLOWTABLE_STATS

    static String read_tcp_map_max_list_len(Element* e, void*);
    static String read_udp_map_max_list_len(Element* e, void*);
    static String read_tcp_map_max_size(Element* e, void*);
    static String read_udp_map_max_size(Element* e, void*);
    static String read_tcp_map_all_max_list_len(Element* e, void*);
    static String read_udp_map_all_max_list_len(Element* e, void*);
    static String read_tcp_map_all_cur_list_len(Element* e, void*);
    static String read_udp_map_all_cur_list_len(Element* e, void*);
    static String read_tcp_map_num_finds(Element* e, void*);
    static String read_udp_map_num_finds(Element* e, void*);
    static String read_tcp_map_find_max(Element* e, void*);
    static String read_udp_map_find_max(Element* e, void*);
    static String read_tcp_map_find_min(Element* e, void*);
    static String read_udp_map_find_min(Element* e, void*);
    static String read_tcp_map_find_avg(Element* e, void*);
    static String read_udp_map_find_avg(Element* e, void*);
    static String read_tcp_map_find_e_avg(Element* e, void*);
    static String read_udp_map_find_e_avg(Element* e, void*);
    static String read_tcp_map_find_total(Element* e, void*);
    static String read_udp_map_find_total(Element* e, void*);

#endif

};

CLICK_ENDDECLS
#endif

