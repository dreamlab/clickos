#ifndef PRIVATE_STATE_USER
#define PRIVATE_STATE_USER

class PrivateStateUser
{
public:
    PrivateStateUser(): _state_id(-1) {}
    virtual ~PrivateStateUser() {}
protected:
    int _state_id;
};

#endif

