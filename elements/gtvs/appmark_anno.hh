#ifndef CLICK_APPMARK_ANNO_HH
#define CLICK_APPMARK_ANNO_HH

#include <click/packet_anno.hh>
/*
 * NOTE: 24 is also used for EXTRA_PACKETS_ANNO
 */
#define APPMARK_ANNO_OFFSET       0
#define APPMARK_ANNO(p)           ((p)->anno_u32(APPMARK_ANNO_OFFSET))
#define SET_APPMARK_ANNO(p, v)    ((p)->set_anno_u32(APPMARK_ANNO_OFFSET, (v)))

#endif

