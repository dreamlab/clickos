/*
 * flowcache.{cc,hh} -- aggregate packets TCP/UDP into flows
 *
 * Sergio Mangialardi
 * Marco Canini
 *
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: flowcache.cc 2219 2009-05-06 18:06:34Z marco $
 */
/*
 * Portions of the file are taken from the AggregateIPFlows,
 * written by Eddie Kohler, they are copyrighted by
 *
 * Copyright (c) 2001-2003 International Computer Science Institute
 * Copyright (c) 2005 Regents of the University of California
 *
 * and distributed with following license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, subject to the conditions
 * listed in the Click LICENSE file. These conditions include: you must
 * preserve this copyright notice, and you cannot mention the copyright
 * holders in advertising related to the Software without their permission.
 * The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
 * notice is a summary of the Click LICENSE file; the license in that file is
 * legally binding.
 */

#include <click/config.h>
#include <click/error.hh>
#include <click/hashmap.hh>
#include <click/straccum.hh>
#include <click/confparse.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <clicknet/icmp.h>
#include <click/packet_anno.hh>
#include <click/handlercall.hh>
#include <click/elemfilter.hh>
#include <click/routervisitor.hh>
#include <click/algorithm.hh>
#include <click/args.hh>

#include "flowcache.hh"

CLICK_DECLS


#define SEC_OLDER(s1, s2) ((int)(s1 - s2) < 0)


// operations on host pairs and ports values

namespace
{
    inline const click_ip* good_ip_header(const Packet* p)
    {
        // called when we already know the packet is good
        const click_ip* iph = p->ip_header();

        if (iph->ip_p == IP_PROTO_ICMP)
            return reinterpret_cast<const click_ip *>(p->icmp_header() + 1); // know it exists
        else
            return iph;
    }

    inline void print_stats(const FlowTable& table)
    {
        (void) table;
#ifdef FLOWTABLE_STATS
		StringAccum sa;
        sa << "max number of flow info: " << table.max_size() << "\n";
        sa << "max bucket length: " << table.bucket_max_length() << "\n";
        sa << "number offind: " << table.num_finds() << " - max: " << table.find_max();
        sa << " - avg: " << table.find_avg();
        sa << " - exp avg: " << table.find_e_avg() << "\n";
        
        click_chatter(sa.c_str());
#endif
    }
}

void FlowCache::Flow::write(DataExport& exporter) const
{
    exporter << fi_._aggregate;

    if (!offline_)
        exporter << active_;

    uint32_t src = ntohl(src_ip());
    if (dotted_ip_)
    {
        StringAccum sa;
        sa.snprintf(15, "%u.%u.%u.%u", NIPQUAD(src));
        exporter << sa.take_string();
    }
    else
        exporter << src;

    exporter << ntohs(src_port());

    uint32_t dst = ntohl(dst_ip());
    if (dotted_ip_)
    {
        StringAccum sa;
        sa.snprintf(15, "%u.%u.%u.%u", NIPQUAD(dst));
        exporter << sa.take_string();
    }
    else
        exporter << dst;

    exporter << ntohs(dst_port());
    if (ip_proto() == IP_PROTO_TCP)
        exporter << "tcp";
    else
        exporter << "udp";

    exporter << first_pkt_ts() << last_pkt_ts();

    exporter << pkts(0) << pkts(1);
    exporter << bytes(0) << bytes(1);
}

void FlowCache::Flow::write_header(DataExport& exporter, bool dotted_ip, bool offline)
{
    /* Ultimately FlowId should be configurable to be either primary_key, unique or key.
     * That is going to be useful for using MySQL 5.1 table partitioning where one cannot
     * use unique columns */
    exporter += column<uint32_t>("FlowId", false);
    exporter += constraint("FlowId", DataExport::Constraint::PRIMARY_KEY);

    if (!offline)
        exporter += column<bool>("Active", false);

    if (dotted_ip)
        exporter += column<String>("SrcIp", 15, false);
    else
        exporter += column<uint32_t>("SrcIp", false);
    exporter += constraint("SrcIp", DataExport::Constraint::KEY);

    exporter += column<uint16_t>("SrcPort", false);
    exporter += constraint("SrcPort", DataExport::Constraint::KEY);

    if (dotted_ip)
        exporter += column<String>("DstIp", 15, false);
    else
        exporter += column<uint32_t>("DstIp", false);
    exporter += constraint("DstIp", DataExport::Constraint::KEY);

    exporter += column<uint16_t>("DstPort", false);
    exporter += constraint("DstPort", DataExport::Constraint::KEY);

    DataExport::Column::EnumType et;
    et.push_back("tcp");
    et.push_back("udp");
    exporter += column("IpProto", et, false);
    exporter += constraint("IpProto", DataExport::Constraint::KEY);

    exporter += column<Timestamp>("FirstPktTs", false);
    exporter += column<Timestamp>("LastPktTs", false);

    exporter += column<uint32_t>("Pkts0", false);
    exporter += column<uint32_t>("Pkts1", false);
    exporter += column<uint64_t>("TotalBytes0", false);
    exporter += column<uint64_t>("TotalBytes1", false);
}

FlowCache::FlowCache(): _next(1), exporter_(0), initialized(false)
{
}

FlowCache::~FlowCache()
{
#if defined(FLOWCACHE_DEBUG)
    print_stats(_tcp_map);
#endif

    clean_map(_tcp_map);
    clean_map(_udp_map);
}

void* FlowCache::cast(const char* n)
{
    if (strcmp(n, "FlowCache") == 0)
        return static_cast<FlowCache*>(this);
    else
        return Element::cast(n);
}

FlowCache* FlowCache::upstream_instance(Element* e)
{

    ElementCastTracker filter(e->router(), "FlowCache");
    Vector<Element*> elements;
    int ok = e->router()->visit_upstream(e, 0, &filter);
    
    if (ok == -1)
        return 0;

    elements = filter.elements();

    if (elements.size() != 1)
        return 0;

    return static_cast<FlowCache*>(elements[0]->cast("FlowCache"));
}

int FlowCache::configure(Vector<String>& conf, ErrorHandler* errh)
{
    dotted_ip_ = true;
    offline_ = true;
    _tcp_timeout = 60;
    _tcp_done_timeout = 30;
    _udp_timeout = 60;
    wrri_timeout_ = 300;

    _gc_interval = 20 * 60;

    _stats_interval = 0;
    bool handle_icmp_errors = false;
    Element* exporter = 0;

    size_t tcp_max_flows = 1048576;
    size_t tcp_max_buckets = 1048576;
    size_t tcp_max_bucket_length = 10;
    size_t tcp_initial_buckets = 1024;
    size_t tcp_resizing_factor = 2;
    bool tcp_resizable = true;

    size_t udp_max_flows = 1048576;
    size_t udp_max_buckets = 1048576;
    size_t udp_max_bucket_length = 10;
    size_t udp_initial_buckets = 1024;
    size_t udp_resizing_factor = 2;
    bool udp_resizable = true;
    
    int anno = PAINT_ANNO_OFFSET;
    bool use_anno = false;

    if (cp_va_kparse(conf, this, errh,
            "TCP_TIMEOUT", 0, cpSeconds, &_tcp_timeout,
            "TCP_DONE_TIMEOUT", 0, cpSeconds, &_tcp_done_timeout,
            "UDP_TIMEOUT", 0, cpSeconds, &_udp_timeout,
            "WRRI_TIMEOUT", 0, cpSeconds, &wrri_timeout_,
            "REAP", 0, cpSeconds, &_gc_interval,
            "WRRI", 0, cpSeconds, &wrri_interval_,
            "ICMP", 0, cpBool, &handle_icmp_errors, // FIXME
            "DATAEXPORTER", 0, cpElement, &exporter,
            "STATS", 0, cpSeconds, &_stats_interval,
            "DOTTEDIP", 0, cpBool, &dotted_ip_,
            "OFFLINE", 0, cpBool, &offline_,
            "RESOURCE_TCP_MAX_FLOWS", 0, cpUnsigned, &tcp_max_flows,
            "RESOURCE_TCP_MAX_BUCKETS", 0, cpUnsigned, &tcp_max_buckets,
            "RESOURCE_TCP_MAX_BUCKET_LENGTH", 0, cpUnsigned, &tcp_max_bucket_length,
            "RESOURCE_TCP_INITIAL_BUCKETS", 0, cpUnsigned, &tcp_initial_buckets,
            "RESOURCE_TCP_RESIZING_FACTOR", 0, cpUnsigned, &tcp_resizing_factor,
            "RESOURCE_TCP_RESIZABLE", 0, cpBool, &tcp_resizable,
            "RESOURCE_UDP_MAX_FLOWS", 0, cpUnsigned, &udp_max_flows,
            "RESOURCE_UDP_MAX_BUCKETS", 0, cpUnsigned, &udp_max_buckets,
            "RESOURCE_UDP_MAX_BUCKET_LENGTH", 0, cpUnsigned, &udp_max_bucket_length,
            "RESOURCE_UDP_INITIAL_BUCKETS", 0, cpUnsigned, &udp_initial_buckets,
            "RESOURCE_UDP_RESIZING_FACTOR", 0, cpUnsigned, &udp_resizing_factor,
            "RESOURCE_UDP_RESIZABLE", 0, cpBool, &udp_resizable,
            "USE_PAINTANNO", 0, cpBool, &use_anno,
            "ANNO", 0, AnnoArg(1), &_anno,
            cpEnd) < 0)
        return -1;

    if (exporter && !(exporter_ = (DataExport*)(exporter->cast("DataExport"))))
        return errh->error("DATAEXPORTER must be a DataExport element");

    if (tcp_max_buckets < tcp_initial_buckets)
        return errh->fatal("RESOURCE_TCP_MAX_BUCKETS must be >= RESOURCE_TCP_INITIAL_BUCKETS");

    if (udp_max_buckets < udp_initial_buckets)
        return errh->fatal("RESOURCE_UDP_MAX_BUCKETS must be >= RESOURCE_UDP_INITIAL_BUCKETS");

    _smallest_timeout = (_tcp_timeout < _tcp_done_timeout ? _tcp_timeout : _tcp_done_timeout);
    _smallest_timeout = (_smallest_timeout < _udp_timeout ? _smallest_timeout : _udp_timeout);
    _handle_icmp_errors = handle_icmp_errors;

    _tcp_map.set_max_flows(tcp_max_flows);
    _tcp_map.set_max_buckets(tcp_max_buckets);
    _tcp_map.set_max_bucket_length(tcp_max_bucket_length);
    _tcp_map.set_initial_buckets(tcp_initial_buckets);
    _tcp_map.set_resizing_factor(tcp_resizing_factor);
    _tcp_map.set_dynamic_resizing(tcp_resizable);

    _udp_map.set_max_flows(udp_max_flows);
    _udp_map.set_max_buckets(udp_max_buckets);
    _udp_map.set_max_bucket_length(udp_max_bucket_length);
    _udp_map.set_initial_buckets(udp_initial_buckets);
    _udp_map.set_resizing_factor(udp_resizing_factor);
    _udp_map.set_dynamic_resizing(udp_resizable);

	// use annotation?
	_anno = anno;
	_use_anno = use_anno;

    return 0;
}

int FlowCache::initialize(ErrorHandler* errh)
{
    initialized = true;

    _active_sec = _gc_sec = _stats_sec = 0;
    wrri_sec_ = 0;

    _timestamp_warning = false;

    if (exporter_)
    {
        int columns0, columns1;
        // write first part of flows info header
        Flow::write_header(*exporter_, dotted_ip_, offline_);

        // write other parts of the flows info file header: each instance of FlowStateHolder
        // can add a part
        columns0 = exporter_->column_count();
        for (Vector<FlowStateHolder*>::iterator iter = flow_state_holders.begin();
            iter < flow_state_holders.end(); ++iter)
        {
            FlowStateHolder* fsh = *iter;
            fsh->write_header(*exporter_);
            columns1 = exporter_->column_count();
            fsh->set_columns(columns1 - columns0);
            columns0 = columns1;
        }
        *exporter_ += DataExport::end;
    }

#if defined(FLOWCACHE_DEBUG) || defined(FLOWTABLE_STATS)
    logger_ = errh;
#endif

    return 0;
}

void FlowCache::cleanup(CleanupStage)
{
    clean_map(_tcp_map);
    clean_map(_udp_map);
}

void FlowCache::register_flow_state_holder(FlowStateHolder* fsh, const String& name)
{
	(void) name;
#ifdef FLOWCACHE_DEBUG
    if (name != "")
        logger_->debug("register_flow_state_holder: %s", name.c_str());
    else
        logger_->debug("register_flow_state_holder");
#endif

    if (initialized)
    {
        ErrorHandler::default_handler()->fatal("Cannot register FlowStateHolder when FlowCache is already initialized");
    }
    else
    {
        int id = flow_state_holders.size();
        fsh->set_id(id);
        flow_state_holders.push_back(fsh);
    }
}

void FlowCache::notify_flow_over(const FCFlowInfo& finfo, uint8_t ip_p, bool active)
{
    Flow flow(finfo, ip_p, dotted_ip_, active, offline_);

    for (Vector<FlowStateHolder*>::iterator iter = flow_state_holders.begin();
        iter < flow_state_holders.end(); ++iter)
    {
        FlowStateHolder* fsh = *iter;
        BaseFlowState* fs = finfo.state(fsh->get_id());
        
        if (fs != 0 || fsh->ignore_null_state())
            fsh->flow_over(flow, fs);
    }
}

void FlowCache::write_flowinfo(DataExport& exporter, const FCFlowInfo& finfo, uint8_t ip_p, bool active)
{
    Flow flow(finfo, ip_p, dotted_ip_, active, offline_);
    flow.write(exporter);

    for (Vector<FlowStateHolder*>::iterator iter = flow_state_holders.begin(); iter < flow_state_holders.end(); ++iter)
    {
        FlowStateHolder* fsh = *iter;
        BaseFlowState* fs = finfo.state(fsh->get_id());

        if (fs != 0 || fsh->ignore_null_state())
        {
            fsh->pre_write_flow_state(fs);
            fsh->write_flow_state(exporter, fs);
            fsh->post_write_flow_state(fs);
        }
        else
        {
            // export NULL for each column when no state has been created for this flow
            int columns = fsh->get_columns();
            while (columns-- > 0)
            {
                exporter << DataExport::NULL_VALUE;
            }
            //ErrorHandler::default_handler()->error("Flow %d: No flow state", finfo._aggregate);
        }
    }

    exporter << DataExport::end;
}

inline void FlowCache::flow_start_hook(const Packet* p, const click_ip* iph, FCFlowInfo& finfo)
{
	if (!_use_anno)
		return;
    /* Here p is the first packet of this new flow.
     * For TCP there are 3 notewhorty cases:
     * 1) p has SYN and NOT ACK => this is really the start of flow, _reverse is correct
     * 2) p has SYN and ACK => ok, we missed the first packet but this is the second, swap _reverse
     * 3) p has NOT SYN => seems we missed the start of flow, trust flipped OR
     *    use a port-number based heuristic (TODO) */
    if (iph->ip_p == IP_PROTO_TCP)
    {
        if (p->tcp_header()->th_flags & TH_SYN && p->tcp_header()->th_flags & TH_ACK)
        {
            finfo._reverse ^= 1;
        }
    }
}

inline void FlowCache::packet_emit_hook(const Packet* p, const click_ip* iph, int paint,
    FCFlowInfo& finfo)
{
    // check whether this indicates the flow is over
    if (iph->ip_p == IP_PROTO_TCP && IP_FIRSTFRAG(iph) &&
    /* 3.Feb.2004 - NLANR dumps do not contain full TCP headers! So relax
     the following length check to just make sure the flags are
     there. */
        p->transport_length() >= 14 && paint < 2)
    {
        // ignore ICMP errors
        const click_tcp *tcph = p->tcp_header();
        if (tcph->th_flags & TH_RST)
            finfo._flow_over = 3;
        else if (tcph->th_flags & TH_FIN)
            finfo._flow_over |= (1 << paint);
        else if (tcph->th_flags & TH_SYN)
        {
            finfo._flow_over = 0;
            finfo.ths_state = 0;
            finfo.syn_seq[paint] = ntohl(tcph->th_seq);
        }
        else if (finfo.ths_state == 0 && tcph->th_flags & TH_ACK)
        {
            // There are 3 cases that we're interested to check:
            // 1) we've seen SYN pkt and this is ACK pkt to SYN+ACK pkt
            // 2) we've seen SYN+ACK pkt and this is ACK pkt to it
            // 3) we've missed both SYN and SYN+ACK or this is not the final packet of 3-way
            //    hand shake
            if (paint == 0 && finfo.syn_seq[0] == (ntohl(tcph->th_seq) - 1))
            {
                finfo.ths_state = 1;
            }
            else if (paint == 0 && finfo.syn_seq[1] == (ntohl(tcph->th_ack) - 1))
            {
                finfo.ths_state = 1;
            }
            else
            {
                finfo.ths_state = 2;
            }
        }
        else if (finfo.ths_state == 1)
        {
            finfo.ths_state = 2;
        }
    }

    // update flowinfo stats
    finfo._last_timestamp = p->timestamp_anno();
    finfo.pkts[paint & 0x1] += 1;
    finfo.bytes[paint & 0x1] += ntohs(iph->ip_len);

}

void FlowCache::clean_map(FlowTable& table)
{
    table.clear();
}

void FlowCache::reap_map(FlowTable& table, uint32_t timeout, uint32_t done_timeout)
{
    timeout = _active_sec - timeout;
    done_timeout = _active_sec - done_timeout;
    FlowTable::reverse_iterator end = table.rend();

    // free completed flows
    for (FlowTable::reverse_iterator iter = table.rbegin(); iter != end;)
    {
        uint32_t relevant_timeout;

        if (iter->_flow_over == 3 || (&table == &_tcp_map && iter->ths_state != 2))
            relevant_timeout = done_timeout;
        else
            relevant_timeout = timeout;

        if (SEC_OLDER(iter->_last_timestamp.sec(), relevant_timeout))
        {
            notify_flow_over(*iter, which_ip_proto(table), false);
            if (exporter_)
            {
                write_flowinfo(*exporter_, *iter, which_ip_proto(table), false);
            }

            iter = table.remove(iter);
        }
        else
        {
            break;
        }
    }
}

void FlowCache::wrri_map(FlowTable& table, uint32_t timeout, uint32_t exp_timeout)
{
    timeout = _active_sec - timeout;
    exp_timeout = _active_sec - exp_timeout;
    FlowTable::iterator end = table.end();

    for (FlowTable::iterator iter = table.begin(); iter != end; ++iter)
    {
        if (SEC_OLDER(iter->last_export_, timeout) && SEC_OLDER(iter->last_export_, iter->_last_timestamp.sec()))
        {
            // TODO: notify the flowstateholders
            //notify_flow_export(*iter, which_ip_proto(table), true);
            if (exporter_)
            {
                write_flowinfo(*exporter_, *iter, which_ip_proto(table), true);
            }
            iter->last_export_ = _active_sec;
        }
        else if (SEC_OLDER(exp_timeout, iter->_last_timestamp.sec()))
            break;
    }
}

void FlowCache::reap()
{
    if (_gc_sec)
    {
        reap_map(_tcp_map, _tcp_timeout, _tcp_done_timeout);
        reap_map(_udp_map, _udp_timeout, _udp_timeout);
    }
    _gc_sec = _active_sec + _gc_interval;
}

void FlowCache::wrri()
{
    if (wrri_sec_)
    {
        wrri_map(_tcp_map, wrri_timeout_, _tcp_timeout);
        wrri_map(_udp_map, wrri_timeout_, _udp_timeout);
    }
    wrri_sec_ = _active_sec + wrri_interval_;
}

void FlowCache::log_stats()
{
    if (_stats_sec)
    {
#ifdef FLOWTABLE_STATS
        logger_->message("tcp map stats: %u %d %d %d %d", _stats_sec,
            _tcp_map.buckets(), _tcp_map.size(), _tcp_map.bucket_max_length(), _tcp_map.find_max());
        logger_->message("udp map stats: %u %d %d %d %d %d", _stats_sec,
            _udp_map.buckets(), _udp_map.size(), _udp_map.bucket_max_length(), _udp_map.find_max());
#endif
    }
    _stats_sec = _active_sec + _stats_interval;
}

const click_ip* FlowCache::icmp_encapsulated_header(const Packet* p)
{
    const click_icmp* icmph = p->icmp_header();

    if (icmph && (icmph->icmp_type == ICMP_UNREACH || icmph->icmp_type == ICMP_TIMXCEED || icmph->icmp_type == ICMP_PARAMPROB || icmph->icmp_type == ICMP_SOURCEQUENCH
	|| icmph->icmp_type == ICMP_REDIRECT) )
    {
        const click_ip* embedded_iph = reinterpret_cast<const click_ip*>(icmph + 1);
        unsigned embedded_hlen = embedded_iph->ip_hl << 2;

        if ((unsigned)p->transport_length() >= sizeof(click_icmp) + embedded_hlen && embedded_hlen >= sizeof(click_ip))
            return embedded_iph;
    }
    return 0;
}

int FlowCache::relevant_timeout(const FCFlowInfo& finfo, const FlowTable& m) const
{
    if (&m == &_udp_map)
        return _udp_timeout;
    else if (finfo._flow_over == 3 || finfo.ths_state != 2)
        return _tcp_done_timeout;
    else
        return _tcp_timeout;
}

uint8_t FlowCache::which_ip_proto(const FlowTable& m) const
{
    if (&m == &_udp_map)
        return IP_PROTO_UDP;
    else
        return IP_PROTO_TCP;
}

FlowTable::iterator FlowCache::find_flow_info(uint32_t host0, uint32_t host1, uint16_t port0, uint16_t port1, FlowTable& m, bool flipped, Packet* p)
{
    const click_ip* iph = p->ip_header();
    FlowTable::iterator iter = m.find(host0, host1, port0, port1);

    if (iter != m.end())
    {
        // if this flow is actually dead (but has not yet been garbage
        // collected), then kill it for consistent semantics
        int age = p->timestamp_anno().sec() - iter->_last_timestamp.sec();
        // 4.Feb.2004 - Also start a new flow if the old flow closed off,
        // and we have a SYN.
        // NOTE: _flow_over is always 0 for UDP
        if ((age > _smallest_timeout && age > relevant_timeout(*iter, m)) ||
            (iter->_flow_over == 3 && (p->tcp_header()->th_flags & TH_SYN)))
        {
            // old aggregate has died
            notify_flow_over(*iter, iph->ip_p, false);
            if (exporter_)
            {
                write_flowinfo(*exporter_, *iter, iph->ip_p, false);
            }

            iter->reset();

            iter->_aggregate = _next;
            _next++;
            iter->_first_timestamp = p->timestamp_anno();
            
            if (!_use_anno)
				iter->_reverse = flipped;
            flow_start_hook(p, iph, *iter);

            //notify(iter->aggregate(), AggregateListener::NEW_AGG, p);
        }

        return iter;
    }

    // make and install new FCFlowInfo pair
    iter = m.insert_not_existent(host0, host1, port0, port1, flipped, _next, flow_state_holders.size());

    if (iter == m.end()) // out of pool memory
    {
#ifdef FLOWCACHE_DEBUG
        logger_->warning("Out of flowinfo memory");
#endif
        return iter;
    }

    _next++;

    iter->_first_timestamp = p->timestamp_anno();
    iter->last_export_ = p->timestamp_anno().sec();
    if (!_use_anno)
		iter->_reverse = flipped;

    //        logger_->debug("flipped=%d", flipped);
    //        logger_->debug("ports=%x", ports);

    flow_start_hook(p, iph, *iter);

    //notify(iter->aggregate(), AggregateListener::NEW_AGG, p);
    return iter;
}

int FlowCache::handle_packet(Packet* p)
{
    const click_ip* iph = p->ip_header();
    int paint = _use_anno ? PAINT_ANNO(p) : 0;

    // check whether the packet is IP
    if (!iph)
        return ACT_DROP;

    // assign timestamp if no timestamp given
    if (!p->timestamp_anno())
    {
        if (!_timestamp_warning)
        {
            click_chatter("%{element}: warning: packet received without timestamp", this);
            _timestamp_warning = true;
        }
        p->timestamp_anno().assign_now();
    }

    // extract encapsulated ICMP header if appropriate
    if (_handle_icmp_errors && iph && iph->ip_p == IP_PROTO_ICMP && IP_FIRSTFRAG(iph))
    {
        iph = icmp_encapsulated_header(p);
        paint = 2;
    }

    // check for truncated IP header
    // cast to int so very large packet length is interpreted as negative
    if ((int) p->length() < (int)sizeof(click_ip))
        return ACT_DROP;

    // check for IP version, only 4 is supported
    if (iph->ip_v != 4)
        return ACT_DROP;

    // check validity of IP header length
    unsigned iph_len = iph->ip_hl << 2;
    if (iph_len < sizeof(click_ip) || p->length() < iph_len)
        return ACT_DROP;

    // check validity of IP addresses
    if (iph->ip_src.s_addr == 0 && iph->ip_dst.s_addr == 0)
        return ACT_DROP;

    // check for fragment
    if (IP_ISFRAG(iph) && !IP_FIRSTFRAG(iph))
    {
        return ACT_DROP;
    }

    // check whether the packet is proper TCP or UDP
    unsigned len = ntohs(iph->ip_len) - iph_len;
    if (iph->ip_p == IP_PROTO_TCP)
    {
        const click_tcp *tcph = p->tcp_header();
        unsigned tcph_len = tcph->th_off << 2;
        if (tcph_len < sizeof(click_tcp) || len < tcph_len || p->length() < iph_len + tcph_len)
            return ACT_DROP;
    }
    else if (iph->ip_p == IP_PROTO_UDP)
    {
        if (len < sizeof(click_udp) || p->length() < iph_len + sizeof(click_udp))
            return ACT_DROP;
    }
    else
    {
        return ACT_DROP;
    }

    // find relevant FCFlowInfo
    FlowTable& m = (iph->ip_p == IP_PROTO_TCP ? _tcp_map : _udp_map);
    uint32_t host0 = iph->ip_src.s_addr;
    uint32_t host1 = iph->ip_dst.s_addr;
    const uint16_t* ports = reinterpret_cast<const uint16_t*>(p->transport_header());
    //assert(ports == reinterpret_cast<const uint16_t*>(iph) + (iph->ip_hl << 1));
    uint16_t port0 = ports[0];
    uint16_t port1 = ports[1];

	if (!_use_anno)
		if (host0 > host1)
		{
			paint ^= 1;
			// swap host addresses
			click_swap(host0, host1);
			// swap ports
			click_swap(port0, port1);
		}

    FlowTable::iterator iter = find_flow_info(host0, host1,  port0, port1, m, _use_anno ? 0 : paint & 1, p);

    if (iter == m.end()) // out of pool memory
        return ACT_DROP;

	if (!_use_anno)
		if (iter->_reverse)
			paint ^= 1;

    // packet emit hook
    packet_emit_hook(p, iph, paint, *iter);

    // mark packet with aggregate number and paint
    _active_sec = p->timestamp_anno().sec();
    SET_AGGREGATE_ANNO(p, iter->_aggregate);
    // NB: need a PAINT ANNO already set!!
    if(!_use_anno)
		SET_PAINT_ANNO(p, paint);
    SET_FLOW_INFO_ANNO(p, reinterpret_cast<long>(&(*iter)));

    return ACT_EMIT;
}

void FlowCache::push(int, Packet* p)
{
    int action = handle_packet(p);

    // GC if necessary
    if (_active_sec >= _gc_sec)
        reap();

    if (wrri_interval_ > 0 && _active_sec >= wrri_sec_)
        wrri();

    if (_stats_interval > 0 && _active_sec >= _stats_sec)
        log_stats();

    if (action == ACT_EMIT)
        output(0).push(p);
    else if (action == ACT_DROP)
        checked_output_push(1, p);
}

Packet* FlowCache::pull(int)
{
    Packet* p = input(0).pull();

    int action = (p ? handle_packet(p) : ACT_NONE);

    // GC if necessary
    if (_active_sec >= _gc_sec)
        reap();

    if (wrri_interval_ > 0 && _active_sec >= wrri_sec_)
        wrri();

    if (_stats_interval > 0 && _active_sec >= _stats_sec)
        log_stats();

    if (action == ACT_EMIT)
        return p;
    else if (action == ACT_DROP)
        checked_output_push(1, p);

    return 0;
}

enum
{
    H_CLEAR
};

int FlowCache::write_handler(const String&, Element* e, void* thunk, ErrorHandler*)
{
    FlowCache* af = static_cast<FlowCache*>(e);

    switch ((intptr_t)thunk)
    {
    case H_CLEAR:
    {
        int active_sec = af->_active_sec;
        int gc_sec = af->_gc_sec;
        af->_active_sec = af->_gc_sec = 0x7FFFFFFF;
        af->reap();
        af->_active_sec = active_sec;
        af->_gc_sec = gc_sec;

        return 0;
    }
    default:
        return -1;
    }
}

enum
{
    H_TCP,
    H_UDP
};

int FlowCache::export_data(const String&, Element* e, void* thunk, ErrorHandler*)
{
    FlowCache* af = static_cast<FlowCache*>(e);
    FlowTable* table = 0;

    switch ((intptr_t)thunk)
    {
    case H_TCP:
        table = &af->_tcp_map;
        break;
    case H_UDP:
        table = &af->_udp_map;
        break;
    default:
        return -1;
    }

    if (af->exporter_)
    {
        FlowTable::iterator end = table->end();
    
        for (FlowTable::iterator iter = table->begin(); iter!=end; ++iter)
        {
            af->write_flowinfo(*af->exporter_, *iter, af->which_ip_proto(*table), true);
        }
    }

    return 0;
}

String FlowCache::read_tcp_map_nbuckets(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.buckets());
}

String FlowCache::read_tcp_active_flows(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.size());
}

String FlowCache::read_udp_map_nbuckets(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.buckets());
}

String FlowCache::read_udp_active_flows(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.size());
}

#ifdef FLOWTABLE_STATS

String FlowCache::read_tcp_map_max_list_len(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.bucket_max_length());
}

String FlowCache::read_udp_map_max_list_len(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.bucket_max_length());
}

String FlowCache::read_tcp_map_max_size(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.max_size());
}

String FlowCache::read_udp_map_max_size(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.max_size());
}

String FlowCache::read_tcp_map_all_max_list_len(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);
    StringAccum sa;
    int n = ag->_tcp_map.buckets();

    for (int i=0; i<n; ++i)
        sa << "Bucket n " << i << " = " << ag->_tcp_map.bucket_max_length(i) << "\n";

    return sa.take_string();
}

String FlowCache::read_udp_map_all_max_list_len(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);
    StringAccum sa;
    int n = ag->_udp_map.buckets();

    for (int i=0; i<n; ++i)
        sa << "Bucket n " << i << " = " << ag->_udp_map.bucket_max_length(i) << "\n";

    return sa.take_string();
}

String FlowCache::read_tcp_map_all_cur_list_len(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);
    StringAccum sa;
    int n = ag->_tcp_map.buckets();

    for (int i=0; i<n; ++i)
        sa << "Bucket n " << i << " = " << ag->_tcp_map.bucket_cur_length(i) << "\n";

    return sa.take_string();
}

String FlowCache::read_udp_map_all_cur_list_len(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);
    StringAccum sa;
    int n = ag->_udp_map.buckets();

    for (int i=0; i<n; ++i)
        sa << "Bucket n " << i << " = " << ag->_udp_map.bucket_cur_length(i) << "\n";

    return sa.take_string();
}

String FlowCache::read_tcp_map_num_finds(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.num_finds());
}

String FlowCache::read_udp_map_num_finds(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.num_finds());
}

String FlowCache::read_tcp_map_find_max(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.find_max());
}

String FlowCache::read_udp_map_find_max(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.find_max());
}

String FlowCache::read_tcp_map_find_min(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.find_min());
}

String FlowCache::read_udp_map_find_min(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.find_min());
}

String FlowCache::read_tcp_map_find_avg(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.find_avg());
}

String FlowCache::read_udp_map_find_avg(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.find_avg());
}

String FlowCache::read_tcp_map_find_e_avg(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.find_e_avg());
}

String FlowCache::read_udp_map_find_e_avg(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.find_e_avg());
}

String FlowCache::read_tcp_map_find_total(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_tcp_map.find_total());
}

String FlowCache::read_udp_map_find_total(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_udp_map.find_total());
}

#endif

String FlowCache::read_next_flow_id(Element* e, void*)
{
    FlowCache* ag = static_cast<FlowCache*>(e);

    return String(ag->_next);
}

void FlowCache::add_handlers()
{
    add_write_handler("clear", write_handler, (void*)H_CLEAR);
    add_write_handler("export_tcp", export_data, (void*)H_TCP);
    add_write_handler("export_udp", export_data, (void*)H_UDP);
    add_read_handler("next_flow_id", read_next_flow_id, 0);
    add_read_handler("tcp_map_nbuckets", read_tcp_map_nbuckets, 0);
    add_read_handler("tcp_active_flows", read_tcp_active_flows, 0);
    add_read_handler("udp_map_nbuckets", read_udp_map_nbuckets, 0);
    add_read_handler("udp_active_flows", read_udp_active_flows, 0);
#ifdef FLOWTABLE_STATS
    add_read_handler("tcp_map_max_list_len", read_tcp_map_max_list_len, 0);
    add_read_handler("udp_map_max_list_len", read_udp_map_max_list_len, 0);
    add_read_handler("tcp_map_max_size", read_tcp_map_max_size, 0);
    add_read_handler("udp_map_max_size", read_udp_map_max_size, 0);
    add_read_handler("tcp_map_all_max_list_len", read_tcp_map_all_max_list_len, 0);
    add_read_handler("udp_map_all_max_list_len", read_udp_map_all_max_list_len, 0);
    add_read_handler("tcp_map_all_cur_list_len", read_tcp_map_all_cur_list_len, 0);
    add_read_handler("udp_map_all_cur_list_len", read_udp_map_all_cur_list_len, 0);
    add_read_handler("tcp_map_num_finds", read_tcp_map_num_finds, 0);
    add_read_handler("udp_map_num_finds", read_udp_map_num_finds, 0);
    add_read_handler("tcp_map_find_max", read_tcp_map_find_max, 0);
    add_read_handler("udp_map_find_max", read_udp_map_find_max, 0);
    add_read_handler("tcp_map_find_min", read_tcp_map_find_min, 0);
    add_read_handler("udp_map_find_min", read_udp_map_find_min, 0);
    add_read_handler("tcp_map_find_avg", read_tcp_map_find_avg, 0);
    add_read_handler("udp_map_find_avg", read_udp_map_find_avg, 0);
    add_read_handler("tcp_map_find_e_avg", read_tcp_map_find_e_avg, 0);
    add_read_handler("udp_map_find_e_avg", read_udp_map_find_e_avg, 0);
    add_read_handler("tcp_map_find_total", read_tcp_map_find_total, 0);
    add_read_handler("udp_map_find_total", read_udp_map_find_total, 0);
#endif
}

//~ ELEMENT_REQUIRES(userlevel)
EXPORT_ELEMENT(FlowCache)

#include <click/vector.cc>

CLICK_ENDDECLS

