#ifndef CLICK_CONSOLEEXPORT_HH
#define CLICK_CONSOLEEXPORT_HH

#include <click/string.hh>
#include <click/straccum.hh>

#include "dataexport.hh"

CLICK_DECLS

class ConsoleExport: public DataExport
{
public:
    ~ConsoleExport();

    const char* class_name() const { return "ConsoleExport"; }
    void* cast(const char* n);

    int configure(Vector<String>& conf, ErrorHandler* errh);

protected:
    void add_column(const Column& col);
    void write(const String& val);
    void write_null();
    void done();

private:
    StringAccum sa_;
};

CLICK_ENDDECLS
#endif

