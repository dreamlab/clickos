#ifndef CLICK_INTERARRIVALSTATS_HH
#define CLICK_INTERARRIVALSTATS_HH

#include <click/element.hh>
#include <click/ipflowid.hh>
#include <click/notifier.hh>
#include <clicknet/tcp.h>

#include <utility>

#include "flowcache.hh"
#include "dataexport.hh"

CLICK_DECLS

/*
=c

InterArrivalStats([I<KEYWORDS>])

=s tnt

collects the series of packet inter-arrival times of TCP/UDP flows

=d

InterArrivalStats collects the series of inter-packet arrival times of TCP/UDP
flows for the first SERIES_LENGTH packets of each flow.
InterArrivalStats uses the direction indication to set the sign of each
inter-packet arrival time. The positive sign is used for inter-packet arrival
times between a packet having as source address the originator of the TCP
connection (or UDP flow) and the previous packet.
Viceversa, the negative sign is used to represent inter-packet arrival times
when the current packet has been sent by the destination of the TCP connection.

Keywords are:

=over 8

=item SERIES_LENGTH

The length for the inter-packet arrival time series. Default is 10.

=item SINGLE_COLUMN_OUTPUT

Whether to output the inter-packet arrival time series as a single, colon
separated values string. Default is false.

=item SKIP_THS

For TCP traffic, whether the inter-packet arrival time series should start after
the triple handshake is observed. Default is true.

=item FLOWCACHE

Element. A FlowCache element that provides state tracking of TCP/UDP flows.
Default is the upstream FlowCache element.

=a

FlowCache
*/

class InterArrivalStats: public Element, public FlowCache::FlowStateHolder
{
public:

    InterArrivalStats();
    ~InterArrivalStats();

    const char* class_name() const { return "InterArrivalStats"; }
    const char* port_count() const { return PORTS_1_1; }
    const char* processing() const { return AGNOSTIC;  }

    int configure(Vector<String>& conf, ErrorHandler* errh);

    Packet* simple_action(Packet*);

    void write_header(DataExport& exporter) const;
    void write_flow_state(DataExport& exporter, const BaseFlowState*) const;

    int series_length() { return series_len; }
    
    class FlowState;
private:

    int series_len;
    bool single_col;
    bool skip_ths;

    FlowCache *flow_cache;
};

class InterArrivalStats::FlowState: public BaseFlowState
{
public:
    FlowState(int series_len);
    ~FlowState();

    int length() const { return count - 1; }
    
    std::pair<Timestamp,int> ia_time(int which) const
    {
        assert(which < count - 1);
        Timestamp& ts = ia_series[which];
        return (ts.sec() >= 0) ? std::pair<Timestamp,int>(ts, 0) :
            std::pair<Timestamp,int>(Timestamp(-ts), 1);
    }
private:
    void initialize(Packet* p);
    void handle_packet(Packet* p, int series_len);

    Timestamp* ia_series;
    int count;

    friend class InterArrivalStats;
};

CLICK_ENDDECLS

#endif

