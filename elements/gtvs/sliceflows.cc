/*
 * sliceflows.{cc,hh} -- strip packet payloads of TCP/UDP flows after the Nth
 *                       packet of the flow is observed
 * Enrico Badino
 * Marco Canini
 * Sergio Mangialardi
 *
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: sliceflows.cc 2209 2009-05-06 18:00:38Z marco $
 */

#include <click/config.h>
#include <click/error.hh>
#include <click/straccum.hh>
#include <click/confparse.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <click/packet_anno.hh>
#include <click/handlercall.hh>

#include "sliceflows.hh"
#include "ippackethelper.hh"

CLICK_DECLS

class SliceFlows::FlowState : public BaseFlowState
{
public:
    uint32_t pkts;

    FlowState() : pkts(0) {}
    ~FlowState() {}

    bool handle_packet(Packet* p, SliceFlows* parent);
};

int SliceFlows::configure(Vector<String>& conf, ErrorHandler* errh)
{
    if (cp_va_kparse(conf, this, errh,
            "MAXPACKETS", 0, cpUnsigned, &max_pkts,
            cpEnd) < 0)
        return -1;

    flow_cache = FlowCache::upstream_instance(this);

    if (!flow_cache)
        return errh->error("Initialization failure!");

    flow_cache->register_flow_state_holder(this);

    return 0;
}

Packet* SliceFlows::simple_action(Packet* p)
{
    FlowState* fs = static_cast<FlowState*>(flow_cache->state(this, p));

    if (!fs) {
        fs = new FlowState();
        flow_cache->state(this, p) = fs;

        if (!fs) {
            click_chatter("out of memory!");
            p->kill();

            return 0;
        }
    }
    if (fs->handle_packet(p, this) && noutputs() == 2) {
    	output(1).push(p);
    	return 0;
    }

    return p;
}

bool SliceFlows::FlowState::handle_packet(Packet* p, SliceFlows* parent) {
    if (pkts == parent->max_pkts) {
        int strip = IPPacketHelper::payload_length(p);

        if (strip > 0) {
        	SET_EXTRA_LENGTH_ANNO(p, p->length());
            p->take(strip);
        }
        return true;
    }
    pkts++;
    return false;
}



ELEMENT_REQUIRES(FlowCache)
EXPORT_ELEMENT(SliceFlows)

#include <click/vector.cc>

CLICK_ENDDECLS

