/*
 * l7pattern.{cc,hh} -- element stores L7 pattern information for later usage
 * 
 * Vito Piserchia
 *
 * Copyright (c) 2013-14 by Dreamlab Technologies AG
 * 
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of Dreamlab Technologies AG nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <click/config.h>
#include <click/error.hh>
#include <click/confparse.hh>

#include <click/elemfilter.hh>
#include <click/router.hh>
#include <click/routervisitor.hh>

#include "l7patterninfo.hh"
#include "patternutils.hh"

CLICK_DECLS

L7PatternInfo::L7PatternInfo()
{	
}

L7PatternInfo::~L7PatternInfo()
{
}

int
L7PatternInfo::configure(Vector<String> &conf, ErrorHandler *errh)
{	
	//~ pinfos_.clear();
	ArgContext context(this);

	for (int i = 0; i < conf.size(); i++) {
		String name = cp_shift_spacevec(conf[i]);
		
		// skip allow empty arguments and comments
		if (!name || name[0] == '#')
			continue;
		
		String pattern = unescape_pattern(conf[i]);
		click_chatter("[L7PatternInfo] parsing pattern string: \"%s\"", pattern.c_str());
		if (!pattern)
			errh->fatal("expected %<NAME PATTERN/[flags=FLAGS/]%>");
		
		if (pattern.length() > MAX_PATTERN_LENGTH)
			errh->fatal("%s pattern too long, should be less then %d", name.c_str());

		String *nfo = pinfos_.findp(name);
		if (!nfo) {
			pinfos_.insert(name, pattern);
		}
	}

    return 0;
}
int
L7PatternInfo::initialize(ErrorHandler* errh)
{
	DynamicNameDB *db;
	if ( !( db = (DynamicNameDB*) NameInfo::getdb(L7PatternInfo::T_L7_PATTERN, this, MAX_PATTERN_LENGTH, true)) )
		errh->fatal("[L7PatternInfo] out of memory");
		   
	for (L7PatternInfo::PinfoIter it = pinfos_.begin(); it.live(); it++)
	{
		String name = it.key();
		String pattern = it.value();

		if (!db->define(name, &pattern, MAX_PATTERN_LENGTH))
		errh->fatal("unable to define pattern for protocol %s \"%s\"", name.c_str(), pattern.c_str());
	}

	return 0;
}
void*
L7PatternInfo::cast(const char* n)
{
    if (strcmp(n, "L7PatternInfo") == 0)
        return static_cast<L7PatternInfo*>(this);
    else
        return Element::cast(n);
}

L7PatternInfo* L7PatternInfo::upstream_or_downstream_instance(Element* e)
{
    ElementCastTracker filter(e->router(), "L7PatternInfo");
    Vector<Element*> elements;
    int ok = e->router()->visit_upstream(e, 0, &filter);
    
    if (ok == -1)
    { 
		ok = e->router()->visit_downstream(e, 0, &filter);
		if (ok == -1)
		{ 
			return 0;
		}
	}

    elements = filter.elements();

    if (elements.size() != 1)
        return 0;

    return static_cast<L7PatternInfo*>(elements[0]->cast("L7PatternInfo"));
}


CLICK_ENDDECLS
EXPORT_ELEMENT(L7PatternInfo)
