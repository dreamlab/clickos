#ifndef CLICK_FLOWLISTENTRY_HH
#define CLICK_FLOWLISTENTRY_HH

/*
 * 
 * Marco Canini
 * Sergio Mangialardi
 *
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of Intel Corporation nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: flowlistentry.hh 2209 2009-05-06 18:00:38Z marco $
 */

#include <click/config.h>

CLICK_DECLS

#include "flowcache.hh"

struct FlowListEntry
{
    FlowListEntry(): buck_prev(0), buck_next(0), lru_prev(0), lru_next(0) {}
    FlowListEntry(uint32_t host0, uint32_t host1, uint16_t port0, uint16_t port1,
        bool reverse, uint32_t aggregate, size_t num_states, FlowListEntry* ln):
            finfo(host0, host1, port0, port1, reverse, aggregate, num_states), buck_prev(0),
            buck_next(0), lru_prev(0), lru_next(ln)
    {}

    void remove()
    {
        if (buck_next != 0)
            buck_next->buck_prev = buck_prev;
        *buck_prev = buck_next;
    }

    FCFlowInfo finfo;
    FlowListEntry** buck_prev;
    FlowListEntry* buck_next;
    FlowListEntry* lru_prev;
    FlowListEntry* lru_next;

};

CLICK_ENDDECLS
#endif

