#ifndef CLICK_DATAEXPORT_HH
#define CLICK_DATAEXPORT_HH

#include <click/element.hh>
#include <click/string.hh>
#include <click/straccum.hh>

#include <click/glue.hh>

#if CLICK_USERLEVEL
# include <stdio.h>
#endif

#include <typeinfo>

CLICK_DECLS

template <class T> inline String to_string(T val)
{
    StringAccum sa;
    sa << val;
    
    return sa.take_string();
}

template <> inline String to_string(char val)
{
    short v = val;

    return to_string(v); 
}

template <> inline String to_string(unsigned char val)
{
    short v = val;

    return to_string(v);
}

template <> inline String to_string(const String& s)
{
    return s; 
}

class DataExport: public Element
{
public:
    struct Column
    {
        typedef Vector<String> EnumType;

        Column(const String& n, const std::type_info* ti, size_t s, bool nb):
            name(n), type(ti), size(s), nullable(nb) {}
        
        String name;
        const std::type_info* type;
        size_t size;
        EnumType enum_type;
        bool nullable;
    };

    struct Constraint
    {
        enum Type {
            KEY,
            PRIMARY_KEY,
            UNIQUE_KEY
        };

        Constraint(const String& n, Type t): name(n), type(t) {}

        String name;
        Type type;
    };

    static Column NULL_VALUE;

    static DataExport& end(DataExport& exporter)
    {
        exporter.done();
        
        if (exporter.next_)
            exporter.next_->done();
            
        return exporter;
    }
    
    DataExport(): next_(0) {}
    ~DataExport() {}
    
    const char* class_name() const { return "DataExport"; }
    void* cast(const char* n);
    
    int column_count() const { return columns_; }
    
    DataExport& operator+=(const Column& col)
    {
        ++columns_;
        
        add_column(col);
        
        if (next_)
            *next_ += col;
        
        return *this;
    }

    DataExport& operator+=(const Constraint& con)
    {
        add_constraint(con);

        if (next_)
            *next_ += con;

        return *this;
    }
    
    DataExport& operator+=(DataExport& (*f)(DataExport&))
    {
        return f(*this);
    }
    
    template <class T> DataExport& operator<<(T val)
    {
        String v = to_string(val);
        write(v);

        if (next_)
            *next_ << v;
        
        return *this;
    }

    DataExport& operator<<(const Column& val)
    {
        assert(&val == &NULL_VALUE);
        write_null();

        if (next_)
            *next_ << val;

        return *this;
    }

    DataExport& operator<<(DataExport& (*f)(DataExport&))
    {
        return f(*this);
    }
protected:
    virtual void add_column(const Column&) {}
    virtual void add_constraint(const Constraint&) {}
    virtual void write(const String&) {}
    virtual void write_null() {}
    virtual void done() {}

    DataExport* next_;
private:
    int columns_;
};

template <class T> inline DataExport::Column column(const String& name,
    size_t size, bool nullable)
{
    return DataExport::Column(name, &typeid(T), size, nullable);
}

template <class T> inline DataExport::Column column(
    const String& name, bool nullable)
{
    return DataExport::Column(name, &typeid(T), sizeof(T) * 8, nullable);
}

template <> inline DataExport::Column column<String>(
    const String& name, bool nullable)
{
    return DataExport::Column(name, &typeid(String), 0, nullable);
}

//~ template <> inline DataExport::Column column<std::string>(
    //~ const String& name, bool nullable)
//~ {
    //~ return DataExport::Column(name, &typeid(std::string), 0, nullable);
//~ }

inline DataExport::Column column(const String& name,
    const DataExport::Column::EnumType& et, bool nullable)
{
    DataExport::Column col(name, &typeid(DataExport::Column::EnumType), 0,
        nullable);
    col.enum_type = et;
    return col;
}

inline DataExport::Constraint constraint(const String& name,
    const DataExport::Constraint::Type t)
{
    return DataExport::Constraint(name, t);
}

CLICK_ENDDECLS
#endif

