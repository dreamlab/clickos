/*
 * flowclassifier.{cc,hh} -- classifies packets according to the flow
 *                           classification
 * 
 *
 * Marco Canini
 *
 * Copyright (c) 2008-09 by University of Genova - DIST - TNT laboratory
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: flowclassifier.cc 2218 2009-05-06 18:06:10Z marco $
 */

#include <click/config.h>
#include <click/error.hh>

#include <click/elemfilter.hh>
#include <click/router.hh>
#include <click/routervisitor.hh>
#include "flowclassifier.hh"

CLICK_DECLS

int FlowClassifier::configure(Vector<String>&, ErrorHandler* errh)
{
    flow_cache_ = FlowCache::upstream_instance(this);

    if (!flow_cache_)
        return errh->error("Initialization failure: "
            "missing reference to a FlowCache element");

    flow_cache_->register_flow_state_holder(this, name());

    return 0;
}

FlowClassifier* FlowClassifier::upstream_instance(Element* e)
{

    ElementCastTracker filter(e->router(), "FlowClassifier");
    Vector<Element*> elements;
    int ok = e->router()->visit_upstream(e, 0, &filter);
    
    if (ok == -1)
        return 0;

    elements = filter.elements();

    if (elements.size() != 1)
        return 0;

    return static_cast<FlowClassifier*>(elements[0]->cast("FlowClassifier"));
}

void
FlowClassifier::attach_listener(Listener* l)
{
    for (int i = 0; i < _listeners.size(); i++)
        if (_listeners[i] == l)
            return;
    _listeners.push_back(l);
}

void
FlowClassifier::detach_listener(Listener* l)
{
    for (int i = 0; i < _listeners.size(); i++)
        if (_listeners[i] == l) {
            _listeners[i] = _listeners.back();
            _listeners.pop_back();
            return;
        }
}

void FlowClassifier::write_header(DataExport& exporter) const
{
    exporter += AppMarks::default_instance()->column("FlowClsMark");
    exporter += column<Timestamp>("FlowClsLatency", false);
    exporter += constraint("FlowClsMark", DataExport::Constraint::KEY);
}

void FlowClassifier::write_flow_state(DataExport& exporter, const BaseFlowState* fs_) const
{
    const FlowState* fs = static_cast<const FlowState*>(fs_);
    exporter << *((const AppMark *) fs);
    exporter << fs->latency;
}

Packet* FlowClassifier::simple_action(Packet* p)
{
    FlowState* fs = flow_cache_->lookup_state<FlowState>(this, p);

    if (!fs)
    {
        fs = new FlowState();
        flow_cache_->state(this, p) = fs;

        if (!fs)
        {
            click_chatter("out of memory!");
            p->kill();

            return 0;
        }
    }
    fs->handle_packet(p, this);

    return p;
}


FlowClassifier::FlowState::FlowState()
{
    set_mark(AppMarks::empty_mark);
}

void FlowClassifier::FlowState::handle_packet(Packet* p, FlowClassifier* fcls)
{
    AppMark m(APPMARK_ANNO(p));
    if (m.get_mark() && m != *this) {
        set_mark(m);
        const FlowCache::Flow flow = fcls->flow_cache_->lookup_flow(p);
        latency = p->timestamp_anno() - flow.first_pkt_ts();
        if (m != AppMarks::unknown_mark)
            fcls->notify_flow_classified(flow, m);
    }
}

ELEMENT_REQUIRES(FlowCache)
EXPORT_ELEMENT(FlowClassifier)

#include <click/vector.cc>
CLICK_ENDDECLS

