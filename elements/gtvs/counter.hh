#ifndef TIME_COUNTER_HH
#define TIME_COUNTER_HH

/*
 * 
 * Marco Canini
 * Sergio Mangialardi
 *
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of Intel Corporation nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: counter.hh 2205 2009-05-06 17:55:47Z marco $
 */

#include <limits>
#include <sys/types.h>

class TimeCounter
{
public:
    TimeCounter(): val_(0), avg_(0), 
        min_(std::numeric_limits<uint64_t>::max()), max_(0), n_(0) {}
    
    void start()
    {
        val_ = rdtsc();
    }
    
    uint64_t stop()
    {
        uint64_t end = rdtsc();
        val_ = end - val_;
        
        if (val_ > max_)
            max_ = val_;
        
        if (val_ < min_)
            min_ = val_;
        
        if (n_ != 0)
            avg_ = (6 * avg_) / 8 + (2 * val_) / 8;
        else
            avg_ = val_;
        
        total_ += val_;
        ++n_;
        
        return val_;
    }
    
    uint64_t min() const
    {
        return min_;
    }
    
    uint64_t max() const
    {
        return max_;
    }
    
    uint64_t avg() const
    {
        return (n_ != 0) ? total_ / n_ : 0;
    }
    
    uint64_t e_avg() const
    {
        return avg_;
    }
    
    uint64_t total() const
    {
        return total_;
    }
    
    size_t num_samples() const
    {
        return n_;
    }
    
    void reset()
    {
        val_ = 0;
        avg_ = 0;
        min_ = std::numeric_limits<uint64_t>::max();
        max_ = 0;
        total_ = 0;
        n_ = 0;
    }
private:
    static uint64_t rdtsc()
    {
        uint64_t rv;
        __asm __volatile(".byte 0x0f, 0x31" : "=A" (rv));
        
        return rv;
    }
private:
    uint64_t val_;
    uint64_t avg_;
    uint64_t min_;
    uint64_t max_;
    uint64_t total_;
    size_t n_;
};

#endif

