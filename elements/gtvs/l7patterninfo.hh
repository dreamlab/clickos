#ifndef CLICK_L7PATTERNINFO_HH
#define CLICK_L7PATTERNINFO_HH
#include <click/element.hh>
#include <click/nameinfo.hh>
#include <click/hashmap.hh>

#define MAX_PATTERN_LENGTH 2048

CLICK_DECLS

/*
=c

L7PatternInfo(NAME PATTERN/[FLAGS], ...)

=s information

stores named L7 pattern information

=io

None

=d

Lets you use mnemonic names for L7 Pattern.  Each argument has the form
`NAME PATTERN FLAGS', which associates the given PATTERN with the NAME,
If FLAGS is left off, no flags are applied.  For
example, in a configuration containing

   L7PatternInfo(http pattern1/flags, torrent pattern2),

configuration strings can use C<http> and C<torrent> as mnemonics for the pattern
C<pattern1> and C<pattern2>, respectively. Also C<flags> is the defined flags
for C<pattern1>, while C<pattern2> has none.

L7PatternInfo names are local with respect to compound elements.  That is, names
created inside a compound element apply only within that compound element and
its subelements.  For example:

XXX: TODO

=n

=a

*/

class L7PatternInfo : public Element { public:

    L7PatternInfo();
	~L7PatternInfo();
    const char *class_name() const	{ return "L7PatternInfo"; }
    void* cast(const char* n);

    int initialize(ErrorHandler *);
	
    int configure(Vector<String> &, ErrorHandler *);

    /** @brief Known name database types. */
    enum DBType {
        T_L7_PATTERN = 0x10000000       ///< L7Pattern names database
    };
	//~ bool query(const String&, void*);;
    static L7PatternInfo* upstream_or_downstream_instance(Element* e);

private:
	typedef HashMap<String, String> PinfoTable;
	typedef PinfoTable::const_iterator PinfoIter;

	PinfoTable pinfos_;

};

CLICK_ENDDECLS
#endif
