#ifndef CLICK_IPPACKETHELPER_HH
#define CLICK_IPPACKETHELPER_HH

#include <click/packet.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>

CLICK_DECLS

/**
 * Works only with TCP/UDP packets!
 */
namespace IPPacketHelper
{
    namespace
    {
        uint32_t transport_header_length(const Packet* p)
        {
            uint32_t thlen = 8; /* UDP header length */

            const click_ip* iph = p->ip_header();

            if (iph->ip_p == IP_PROTO_TCP)
            {
                thlen = p->tcp_header()->th_off * 4;
                
                if (thlen < 20) /* invalid TCP header */
                    return 0;
            }

            return thlen;
        }

        const unsigned char* payload(const Packet* p)
        {
            uint32_t thlen = transport_header_length(p);
            
            return p->transport_header() + thlen;
        }
        
        int payload_length(const Packet* p)
        {
            int pload = p->end_data() - payload(p);
            //payload = p->length() - (p->ip_header_length() + thlen);
            return pload;
        }

        bool check_header(const Packet* p)
        {
            const click_ip* iph = p->ip_header();

            // check for truncated IP header
            // cast to int so very large packet length is interpreted as negative
            if ((int) p->length() < (int)sizeof(click_ip))
                return false;

            // check for IP version, only 4 is supported
            if (iph->ip_v != 4)
                return false;

            // check validity of IP header length
            unsigned iph_len = iph->ip_hl << 2;
            
            if (iph_len < sizeof(click_ip) || p->length() < iph_len)
                return false;

            // check validity of IP addresses
            if (iph->ip_src.s_addr == 0 && iph->ip_dst.s_addr == 0)
                return false;

            return true;
        }

        bool check_transport_header(const Packet* p)
        {
            const click_ip* iph = p->ip_header();
            unsigned iph_len = iph->ip_hl << 2;

            // check whether the packet is proper TCP or UDP
            unsigned len = ntohs(iph->ip_len) - iph_len;
            
            if (iph->ip_p == IP_PROTO_TCP)
            {
                const click_tcp *tcph = p->tcp_header();
                unsigned tcph_len = tcph->th_off << 2;
                
                if (tcph_len < sizeof(click_tcp) || len < tcph_len || p->length() < iph_len + tcph_len)
                    return false;
            }
            else if (iph->ip_p == IP_PROTO_UDP)
            {
                if (len < sizeof(click_udp) || p->length() < iph_len + sizeof(click_udp))
                    return false;
            }
            else
            {
                return false;
            }

            return true;
        }
    };
};

CLICK_ENDDECLS

#endif

