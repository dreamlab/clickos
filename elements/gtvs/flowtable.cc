/*
 * flowtable.{cc,hh} -- A hashtable to store flow state information.
 *
 * Sergio Mangialardi
 * Marco Canini
 *
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: flowtable.cc 2206 2009-05-06 17:56:00Z marco $
 */

#include <click/config.h>

#include "flowcache.hh"
#include "flowtable.hh"
#include "flowlistentry.hh"

CLICK_DECLS

void FlowTable::Bucket::insert_head(FlowListEntry* elm)
{
    if ((elm->buck_next = list) != 0)
        list->buck_prev = &elm->buck_next;
    list = elm;
    elm->buck_prev = &list;
}

#ifdef FLOWTABLE_STATS // FlowTable constructor with statistics

FlowTable::FlowTable(): max_flows_(default_max_flows), max_buckets_(default_max_buckets),
    max_bucket_length_(default_max_bucket_length), initial_buckets_(default_initial_buckets),
    resizing_factor_(default_resizing_factor), resizable_(true), buckets_(0), n_buckets_(0),
    size_(0), max_buck_list_len_(0), max_buck_list_len_index_(0), max_size_(0),
    lru_first_(0), lru_last_(0)
{}

#else // FlowTable constructor without statistics

FlowTable::FlowTable(): max_flows_(default_max_flows), max_buckets_(default_max_buckets),
    max_bucket_length_(default_max_bucket_length), initial_buckets_(default_initial_buckets),
    resizing_factor_(default_resizing_factor), resizable_(true), buckets_(0), n_buckets_(0),
    size_(0), lru_first_(0), lru_last_(0)
{
}

#endif

FlowTable::~FlowTable()
{
    delete [] buckets_;
}

FlowTable::iterator FlowTable::insert_not_existent(uint32_t host0, uint32_t host1, uint16_t port0, uint16_t port1,
    bool reverse, uint32_t aggregate, size_t num_states)
{
    if (n_buckets_ == 0)
        allocate();

    size_t idx = bucket(host0, host1, port0, port1);

    if (buckets_[idx].cur_len >= max_bucket_length_)
    {
        if (resizable_ && n_buckets_ < max_buckets_)
        {
            resize();
            idx = bucket(host0, host1, port0, port1);

            if (buckets_[idx].cur_len >= max_bucket_length_)
            {
				StringAccum sa;
                sa << "List length > max_bucket_length after resize" << "\n";
                click_chatter(sa.c_str());
                return end();
            }
        }
        else
            return end(); // Out of bucket capacity
    }

    FlowListEntry* buck = buckets_[idx].list;

#ifdef  HAS_MEMORY_POOL
		FlowListEntry* entry = pool_.pop();
#else
		FlowListEntry* entry = new FlowListEntry(host0, host1, port0, port1, reverse, aggregate, num_states, lru_first_);
#endif

    if (!entry) // Out of pool memory
    {
#ifdef  HAS_MEMORY_POOL
        click_chatter("pool out of memory!");
#endif
        return end();
	}

#ifdef FLOWCACHE_DEBUG

    entry->finfo.idx = idx;

#endif

    assert(buck != entry);

#ifdef  HAS_MEMORY_POOL
    new (entry) FlowListEntry(host0, host1, port0, port1, reverse, aggregate, num_states, lru_first_);
#endif
    buckets_[idx].insert_head(entry);

    assert(entry->buck_next != entry);

    if (entry->buck_next != 0)
        assert(entry->buck_next != *entry->buck_prev);

    if (lru_first_)
        lru_first_->lru_prev = entry;

    lru_first_ = entry;

    if (!lru_last_)
        lru_last_ = entry;

    ++size_;
    ++(buckets_[idx]);

#ifdef FLOWTABLE_STATS

    if (size_ > max_size_)
        max_size_ = size_;

    if (buckets_[idx].max_len > max_buck_list_len_)
    {
        max_buck_list_len_ = buckets_[idx].max_len;
        max_buck_list_len_index_ = idx;
    }

#endif

    return iterator(entry);
}

FlowTable::iterator FlowTable::find(uint32_t host0, uint32_t host1, uint16_t port0, uint16_t port1)
{
    iterator iter = end();

    if (n_buckets_ == 0)
        return iter;

#ifdef FLOWTABLE_STATS

    counter_.start();

#endif

    size_t idx = bucket(host0, host1, port0, port1);
    FlowListEntry* buck = buckets_[idx].list;
    FlowListEntry* entry = buck;

    while (entry)
    {
        FCFlowInfo& finfo = entry->finfo;

        if (finfo.host0 == host0 && finfo.host1 == host1 && finfo.port0 == port0 && finfo.port1 == port1)
        {
            if (entry != buck)
                move_to_bucket_front(idx, entry);

            if (entry != lru_first_)
                move_to_lru_front(entry);

            iter = iterator(entry);
            break;
        }

        entry = entry->buck_next;
    }

#ifdef FLOWTABLE_STATS

    counter_.stop();

    if (counter_.max() > max_)
    {
        max_ = counter_.max();

        StringAccum sa;
        if (entry)
            sa << "list: " << buckets_[idx].cur_len << " max: " << max_ << " - flowid: " << entry->finfo._aggregate << "\n";
        else
            sa << "list: " << buckets_[idx].cur_len << " max: " << max_ << " - flow: " << host0 << " " << host1 << " " << port0 << " " << port1 << "\n";
        click_chatter(sa.c_str());
    }

#endif

    return iter;
}

FlowTable::iterator FlowTable::remove(iterator& iter)
{
    FlowListEntry* entry = iter.entry_;
    iterator it(entry->lru_next);
    remove(entry);

    return it;
}

FlowTable::reverse_iterator FlowTable::remove(reverse_iterator& iter)
{
    FlowListEntry* entry = iter.entry_;
    reverse_iterator it(entry->lru_prev);
    remove(entry);

    return it;
}

void FlowTable::remove(FlowListEntry* entry)
{
    assert(entry->buck_next != entry);
    entry->remove();
    assert(entry->buck_next != entry);

    if (entry->lru_prev)
        entry->lru_prev->lru_next = entry->lru_next;
    else
        lru_first_ = entry->lru_next;

    if (entry->lru_next)
        entry->lru_next->lru_prev = entry->lru_prev;
    else
        lru_last_ = entry->lru_prev;

    FCFlowInfo& finfo = entry->finfo;
    size_t idx = bucket(finfo.host0, finfo.host1, finfo.port0, finfo.port1);

#ifdef FLOWCACHE_DEBUG

    assert(idx == finfo.idx);

#endif

    --(buckets_[idx]);

#ifdef  HAS_MEMORY_POOL
        pool_.push(entry);
#else
        delete entry;
#endif   

    --size_;
}

void FlowTable::clear()
{
    FlowListEntry* iter = lru_first_;

    while (iter)
    {
        FlowListEntry* entry = iter;
        iter = iter->lru_next;
#ifdef  HAS_MEMORY_POOL
        pool_.push(entry);
#else
        delete entry;
#endif
    }

    lru_first_ = 0;
    lru_last_ = 0;

    for (size_t i=0; i<n_buckets_; ++i)
        buckets_[i].reset();

    size_ = 0;
}

void FlowTable::allocate()
{
#ifdef  HAS_MEMORY_POOL
		pool_.allocate(max_flows_);
#endif

    buckets_ = new Bucket[initial_buckets_];
    n_buckets_ = initial_buckets_;
}

void FlowTable::resize()
{
    delete [] buckets_;
    size_t num = n_buckets_ * resizing_factor_;

    if (num > max_buckets_)
        num = max_buckets_;

    buckets_ = new Bucket[num];
    n_buckets_ = num;

    FlowListEntry* entry = lru_first_;

    while (entry)
    {
        size_t idx = bucket(entry->finfo.host0, entry->finfo.host1, entry->finfo.port0, entry->finfo.port1);
        buckets_[idx].insert_head(entry);

#ifdef FLOWCACHE_DEBUG

        entry->finfo.idx = idx;

#endif

        assert(entry->buck_next != entry);

        ++buckets_[idx]; 

        entry = entry->lru_next;
    }
}

void FlowTable::move_to_bucket_front(size_t idx, FlowListEntry* e)
{
    assert(e->buck_next != e);
    e->remove();
    buckets_[idx].insert_head(e);
    assert(e->buck_next != e);
}

void FlowTable::move_to_lru_front(FlowListEntry* e)
{
    e->lru_prev->lru_next = e->lru_next;

    if (e->lru_next)
        e->lru_next->lru_prev = e->lru_prev;

    lru_first_->lru_prev = e;

    if (lru_last_ == e)
        lru_last_ = e->lru_prev;

    e->lru_next = lru_first_;
    e->lru_prev = 0;
    lru_first_ = e;
}

ELEMENT_PROVIDES(FlowTable)

CLICK_ENDDECLS

