/*
 * packetsizestats.{cc,hh} -- collect the series of packet sizes
 *                            of TCP/UDP flows
 * Marco Canini
 *
 * Copyright (c) 2007 by University of Genova - DIST - TNT laboratory
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: packetsizestats.cc 2217 2009-05-06 18:05:44Z marco $
 */

#include <click/config.h>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <click/packet_anno.hh>
#include <click/confparse.hh>
#include <click/straccum.hh>
#include <click/string.hh>

#include "packetsizestats.hh"

CLICK_DECLS

PacketSizeStats::PacketSizeStats()
{
}

PacketSizeStats::~PacketSizeStats()
{
}

int PacketSizeStats::configure(Vector<String>& conf, ErrorHandler* errh)
{
    Element* af_element = 0;
    series_len = 10;
    single_col = false;
    skip_ths = true;

    if (cp_va_kparse(conf, this, errh,
            "FLOWCACHE", cpkP, cpElement, &af_element,
            "SERIES_LENGTH", 0, cpUnsigned, &series_len,
            "SINGLE_COLUMN_OUTPUT", 0, cpBool, &single_col,
            "SKIP_THS", 0, cpBool, &skip_ths,
            cpEnd) < 0)
        return -1;

    if (af_element && !(flow_cache = (FlowCache*)(af_element->cast("FlowCache"))))
        return errh->error("FLOWCACHE must be a FlowCache element");

    if (!flow_cache)
        flow_cache = FlowCache::upstream_instance(this);

    if (!flow_cache)
        return errh->error("Initialization failure!");

    if (series_len == 0)
        return errh->error("SERIES_LENGTH must be greater then zero");

    flow_cache->register_flow_state_holder(this, name());

    return 0;
}

Packet* PacketSizeStats::simple_action(Packet* p)
{
    FlowState* fs = flow_cache->lookup_state<FlowState>(this, p);

    if (!fs)
    {
        fs = new FlowState(series_len);
        flow_cache->state(this, p) = fs;

        if (!fs)
        {
            click_chatter("out of memory!");
            p->kill();

            return 0;
        }
    }
    
    FlowCache::Flow f = flow_cache->lookup_flow(p);
    if (skip_ths == false || f.is_udp() || f.is_ths_completed())
        fs->push_payload_size(p, series_len);

    return p;
}

void PacketSizeStats::write_header(DataExport& exporter) const
{
    if (single_col == false) {
        for (int i = 0; i < series_len; ++i) {
            StringAccum name;
            name << "PayloadSize" << i;
            exporter += column<int16_t>(name.take_string(), true);
        }
    } else {
        StringAccum name;
        name << "PayloadSizes[" << series_len << "]";
    
        exporter += column<String>(name.take_string(), false);
    }
}

static void print_size(StringAccum& val, int32_t sz)
{
    if (sz >= 0)
    {
        val << ">" << sz;
    }
    else
    {
        val << "<" << -sz - 1;
    }
}

void PacketSizeStats::write_flow_state(DataExport& exporter, const BaseFlowState* fs_) const
{
    const FlowState* fs = static_cast<const FlowState*>(fs_);
    int l = fs->count;
    if (single_col == false) {
        int i = 0;
        while (i < l) {
            int16_t sz = fs->size_series[i];
            //exporter << ((sz >= 0) ? sz : (-sz - 1));
            exporter << sz;
            ++i;
        }
        while (i < series_len) {
            exporter << DataExport::NULL_VALUE;
            ++i;
        }
    } else {
        StringAccum val;
    
        if (l > 0)
        {
            for (int i = 0; i < l - 1; i++)
            {
                print_size(val, fs->size_series[i]);
                val << ":";
            }
            print_size(val, fs->size_series[l - 1]);
        }
        else
        {
            val << ":";
        }
    
        exporter << val.take_string();
    }
}

PacketSizeStats::FlowState::FlowState(int series_len): count(0)
{
    size_series = new int16_t[series_len];
    memset(size_series, 0, series_len * sizeof(int16_t));
}

PacketSizeStats::FlowState::~FlowState()
{
    delete[] size_series;
}

void PacketSizeStats::FlowState::push_payload_size(Packet* p, int series_len)
{
    if (count == series_len)
        return; // nothing to do

    const click_tcp* tcph;

    //int tlen = p->transport_length();

    const click_ip* ip = p->ip_header();
    int tlen = ntohs(ip->ip_len);
    tlen -= (ip->ip_hl << 2);
    switch (ip->ip_p)
    {
    case IPPROTO_TCP:
        tcph = p->tcp_header();
        tlen -= tcph->th_off * 4;
        break;

    case IPPROTO_UDP:
        tlen -= sizeof(click_udp);
        break;
    }

    if (PAINT_ANNO(p) == 1)
    {
        size_series[count] = (int16_t) -tlen - 1;
    }
    else
    {
        size_series[count] = (int16_t) tlen;
    }

    count++;
}

ELEMENT_REQUIRES(FlowCache)
EXPORT_ELEMENT(PacketSizeStats)

CLICK_ENDDECLS

