#ifndef CLICK_FLOWCLASSIFIER_HH
#define CLICK_FLOWCLASSIFIER_HH

#include <click/element.hh>

#include "flowcache.hh"
#include "appmarks.hh"

CLICK_DECLS

class FlowClassifier: public Element, public FlowCache::FlowStateHolder
{
public:
    FlowClassifier(): flow_cache_(0) {} 

    const char* class_name() const { return "FlowClassifier"; }
    const char* port_count() const { return "1-/1"; }
    const char* processing() const { return AGNOSTIC;  }

    int configure(Vector<String>& conf, ErrorHandler* errh);

    Packet* simple_action(Packet* p);

    void write_header(DataExport& exporter) const;
    void write_flow_state(DataExport& exporter, const BaseFlowState* fs_) const;

    class Listener
    {
    public:
        virtual void flow_classified(const FlowCache::Flow& flow, const AppMark& mark) = 0;
    };
    
    void attach_listener(Listener*);
    void detach_listener(Listener*);

	static FlowClassifier* upstream_instance(Element* e);

    class FlowState;
private:
    FlowCache* flow_cache_;
    Vector<Listener*> _listeners;
    
    inline void notify_flow_classified(const FlowCache::Flow& flow, const AppMark& mark)
    {
        for (int i = 0; i < _listeners.size(); i++)
            _listeners[i]->flow_classified(flow, mark);
    }

    friend class FlowState;
};

class FlowClassifier::FlowState: public BaseFlowState, public AppMark
{
public:
    Timestamp latency;
    
    FlowState();

    void handle_packet(Packet*, FlowClassifier*);
};

CLICK_ENDDECLS
#endif

