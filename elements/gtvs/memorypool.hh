#ifndef MEMORY_POOL_HH
#define MEMORY_POOL_HH

/*
 * Original authors:
 *    Marco Canini
 *    Sergio Mangialardi
 * 
 * ClickOS support:
 *    Vito Piserchia
 *
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 * Copyright (c) 2013-14 by Dreamlab Technologies AG
 *  
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of Intel Corporation nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * * Neither the name of Dreamlab Technologies AG nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <cassert>
#include <stddef.h>

#include "allocator.h"

template <class T, class U> struct IsSameType
{
    const static bool value = false;
};

template <class T> struct IsSameType<T, T>
{
    const static bool value = false;
};

template <class T, class U> class IsConvertible
{
    typedef char Small;
    typedef struct { char c[2]; } big;
    static Small test(U);
    static big test(...);
    static T makeT();
public:
    const static bool value = sizeof(test(makeT())) == sizeof(Small);
};

template <class B, class D> struct IsBasePointer
{
    const static bool value = IsSameType<B, D>::value ||
        (IsConvertible<D, B>::value && !IsSameType<B, const volatile void*>::value);
};

template <class T, class A = myallocator<char> > class SimpleStorage
{
    typedef A Allocator;

    struct Elem
    {
        T ptr;
        Elem* next;
    };
public:
    SimpleStorage(): store_(0), first_(0), start_(0), end_(0), num_(0), size_(0) {}

    explicit SimpleStorage(size_t num): store_(0), first_(0), num_(0), size_(0)
    {
        allocate(num);
    }

    ~SimpleStorage()
    {
        alloc_.deallocate(store_, size_);
    }

    void allocate(size_t num)
    {
        if (store_)
            return;

        size_t size = num * sizeof(Elem);
        store_ = alloc_.allocate(size);
        size_ = size;
        num_ = num;
        first_ = reinterpret_cast<Elem*>(store_);
        start_ = store_;
        end_ = store_ + size;
        first_->next = 0;
    }

    T* pop()
    {
        if (num_ < 1)
            return 0;

        Elem* ret = first_;
        --num_;

        if (first_->next)
            first_ = first_->next;
        else
        {
            ++first_;

            if (num_ > 0)
                first_->next = 0;
        }

        ret->next = 0;
        assert(start_ <= ret && ret <= end_);

        return &ret->ptr;
    }

    template <class P> void push(P*& ptr)
    {
        if (!ptr)
            return;

        check(ptr);
        destroy(ptr);
        reset(ptr);
        Elem* e = reinterpret_cast<Elem*>(ptr);
        e->next = first_;
        first_ = e;
        ++num_;
        ptr = 0;
    }

    size_t size() const
    {
        return num_;
    }
private:
    template <class P> void check(P* ptr)
    {
        assert((IsBasePointer<P*, T*>::value));
        assert((start_ <= ptr && ptr <= end_));
        assert((!reinterpret_cast<Elem*>(ptr)->next));
    }

    template <class P> void destroy(P* ptr)
    {
        ptr->~P();
    }

    void reset(void* ptr)
    {
        assert(memset(ptr, 0, sizeof(T)));
    }
private:
    Allocator alloc_;
    char* store_;
    Elem* first_;
    void* start_;
    void* end_;
    size_t num_;
    size_t size_;
};

template <class T, size_t num_blocks, class A = myallocator<char> > class BlockStorage
{
    typedef A Allocator;

    struct Elem
    {
        T ptr;
        Elem* next;
    };
public:
    BlockStorage(): first_(0), num_(0), capacity_(0), idx_(0)
    {
        for (int i=0; i<num_blocks; ++i)
            store_[i] = 0;
    }

    explicit BlockStorage(size_t num): first_(0), num_(0), capacity_(0), idx_(0)
    {
        for (int i=0; i<num_blocks; ++i)
            store_[i] = 0;

        allocate(num);
    }

    ~BlockStorage()
    {
        for (size_t i=0; i<idx_; ++i)
            alloc_.deallocate(store_[i], block_size_);
        if (idx_ == num_blocks -1)
            alloc_.deallocate(store_[idx_], last_block_size_);
    }

    void allocate(size_t num)
    {
        if (store_[0])
            return;

        block_num_ = num / num_blocks;
        block_size_ = block_num_ * sizeof(Elem);
        last_block_num_ = block_num_ + num % num_blocks;
        last_block_size_ = last_block_num_ * sizeof(Elem);
        store_[idx_] = alloc_.allocate(block_size_);
        num_ = num;
        capacity_ = block_num_;
        first_ = reinterpret_cast<Elem*>(store_[idx_]);
        first_->next = 0;
    }

    T* pop()
    {
        if (num_ < 1)
            return 0; // out of memory!

        if (capacity_ < 1)
        {
            if (idx_ < num_blocks - 1)
                realloc();
            else
                return 0;
        }

        Elem* ret = first_;
        --num_;
        --capacity_;

        if (first_->next)
            first_ = first_->next;
        else
        {
            ++first_;

            if (capacity_ > 0)
                first_->next = 0;
        }

        ret->next = 0;

        return &ret->ptr;
    }

    template <class P> void push(P*& ptr)
    {
        if (!ptr)
            return;

        check(ptr);
        destroy(ptr);
        reset(ptr);
        Elem* e = reinterpret_cast<Elem*>(ptr);
        e->next = first_;
        first_ = e;
        ++num_;
        ++capacity_;
        ptr = 0;
    }

    size_t size() const
    {
        return num_;
    }
private:
    void realloc()
    {
		
        size_t size = block_size_;
        size_t num = block_num_;
        ++idx_;
        if (idx_ == num_blocks - 1)
        {
            size = last_block_size_;
            num = last_block_num_;
        }

        store_[idx_] = alloc_.allocate(size);
        capacity_ += num;
        first_ = reinterpret_cast<Elem*>(store_[idx_]);
        first_->next = 0;
    }

    template <class P> void check(P* ptr)
    {
        assert((IsBasePointer<P*, T*>::value));
        assert((check_(reinterpret_cast<char*>(ptr))));
        assert((!reinterpret_cast<Elem*>(ptr)->next));
    }

    bool check_(char* ptr)
    {
        bool ret = false;

        for (int i=0; i<=idx_; ++i)
        {
            if (i < (num_blocks - 1))
                ret |= (store_[i] <= ptr && ptr <= (store_[i] + block_size_));
            else
                ret |= (store_[i] <= ptr && ptr <= (store_[i] + last_block_size_));
        }

        return ret;
    }

    template <class P> void destroy(P* ptr)
    {
        //--idx_; // Vito!
        ptr->~P();
    }

    void reset(void* ptr)
    {
        assert(memset(ptr, 0, sizeof(T)));
    }
private:
    Allocator alloc_;
    char* store_[num_blocks];
    Elem* first_;
    size_t num_;
    size_t block_num_;
    size_t last_block_num_;
    size_t block_size_;
    size_t last_block_size_;
    size_t capacity_;
    size_t idx_;
};

template <class T, class S = SimpleStorage<T, myallocator<char> > > class MemoryPool
{
    typedef S Storage;
public:
    MemoryPool() {}

    explicit MemoryPool(size_t num): store_(num) {}

    void allocate(size_t num)
    {
        store_.allocate(num);
    }
    
    T* pop()
    {
        return store_.pop();
    }

    template <class P> void push(P*& ptr)
    {
        store_.push(ptr);
    }

    size_t size() const
    {
        return store_.size();
    }
private:
    Storage store_;
};

template <class T, class S> void* operator new(size_t /*size*/, MemoryPool<T, S>& pool)
{
    return pool.pop();
}

template <class T, class S> void operator delete(void* ptr, MemoryPool<T, S>& pool)
{
    pool.push(ptr);
}

#endif

