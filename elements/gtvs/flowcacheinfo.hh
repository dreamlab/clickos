#ifndef CLICK_FLOWCACHEINFO_HH
#define CLICK_FLOWCACHEINFO_HH

/*
 * flowcacheinfo.hh -- flow struct to be used in a FlowCache
 * 
 * Original authors:
 *    Sergio Mangialardi
 *    Marco Canini
 * 
 * Modularization:
 *    Vito Piserchia
 * 
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 * Copyright (c) 2013-14 by Dreamlab Technologies AG
 * 
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * * Neither the name of Dreamlab Technologies AG nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <click/algorithm.hh>

CLICK_DECLS

# ifndef HAVE_INT64_IS_LONG
#  define FLOW_INFO_ANNO_OFFSET REV_RATE_ANNO_OFFSET
#  define FLOW_INFO_ANNO(p)   ((p)->anno_u32(FLOW_INFO_ANNO_OFFSET))
#  define SET_FLOW_INFO_ANNO(p, v)   ((p)->set_anno_u32(FLOW_INFO_ANNO_OFFSET, (v)))
# else
#  define FLOW_INFO_ANNO_OFFSET REV_RATE_ANNO_OFFSET
#  define FLOW_INFO_ANNO(p)   ((p)->anno_u64(FLOW_INFO_ANNO_OFFSET))
#  define SET_FLOW_INFO_ANNO(p, v)   ((p)->set_anno_u64(FLOW_INFO_ANNO_OFFSET, (v)))
# endif

class BaseFlowState
{
public:
    virtual ~BaseFlowState() {}
};

struct FCFlowInfo
{
    FCFlowInfo(): _aggregate(0), host0(0), host1(0), port0(0), port1(0), last_export_(0),
        _reverse(0), _num_states(0), _states(0) {}
    FCFlowInfo(uint32_t h0, uint32_t h1, uint16_t p0, uint16_t p1, bool reverse, uint32_t agg,
        size_t num_states);
    ~FCFlowInfo();

    uint32_t _aggregate;

    uint32_t host0;
    uint32_t host1;
    uint16_t port0;
    uint16_t port1;

    Timestamp _first_timestamp;
    Timestamp _last_timestamp;

    uint32_t pkts[2];
    uint64_t bytes[2];

    unsigned int last_export_;

    unsigned int _flow_over: 2;
    unsigned int _reverse: 1;
    unsigned int ths_state: 2; // whether a TCP has completed the triple hand shake

    size_t _num_states;

    tcp_seq_t syn_seq[2];

#ifdef FLOWCACHE_DEBUG

    size_t idx; // auxiliary variable to debug flowtable

#endif

    inline BaseFlowState*& state(int id) const
    {
        assert(id >= 0);
        return _states[id];
    }

    // have 24 bytes; statistics would double
    // + 8 + 8 = 16 bytes

    void reverse()
    {
        click_swap(host0, host1);
        click_swap(port0, port1);
        _reverse ^= _reverse;
    }

    void reset();
private:
	BaseFlowState** _states;
    friend class FlowCache;
};

FCFlowInfo::FCFlowInfo(uint32_t h0, uint32_t h1, uint16_t p0, uint16_t p1, bool reverse, uint32_t agg,
    size_t num_states): _aggregate(agg), host0(h0), host1(h1), port0(p0), port1(p1), _flow_over(0),
    _reverse(reverse), ths_state(0), _num_states(num_states), _states(0)
{
    pkts[0] = 0;
    pkts[1] = 0;
    bytes[0] = 0;
    bytes[1] = 0;

    if (_num_states > 0)
        _states = new BaseFlowState*[_num_states];

    for (size_t i=0; i<_num_states; ++i)
        _states[i] = 0;
}

FCFlowInfo::~FCFlowInfo()
{
    reset();
    delete[] _states;
}

void FCFlowInfo::reset()
{
    _flow_over = 0;
    ths_state = 0;

    pkts[0] = 0;
    pkts[1] = 0;
    bytes[0] = 0;
    bytes[1] = 0;

    for (size_t i = 0; i<_num_states; ++i) {
        delete _states[i];
        _states[i] = 0;
    }
}


CLICK_ENDDECLS

#endif
