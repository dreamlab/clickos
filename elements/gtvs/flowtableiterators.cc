/*
 * flowtableiterators.{cc,hh} -- A hashtable to store flow state information.
 *
 * Sergio Mangialardi
 * Marco Canini
 *
 * Copyright (c) 2007-09 by University of Genova - DIST - TNT laboratory
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of University of Genova nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: flowtableiterators.cc 2206 2009-05-06 17:56:00Z marco $
 */

#include <click/config.h>

#include "flowtableiterators.hh"
#include "flowlistentry.hh"

CLICK_DECLS

FlowTableIterator::FlowTableIterator(FlowListEntry* entry): entry_(entry) {}

FlowTableIterator::FlowTableIterator(const FlowTableIterator& other): entry_(other.entry_) {}

FlowTableIterator& FlowTableIterator::operator=(const FlowTableIterator& other)
{
    entry_ = other.entry_;

    return *this;
}

FlowTableIterator FlowTableIterator::operator++(int)
{
    FlowTableIterator tmp(*this);
    entry_ = entry_->lru_next;

    return tmp;
}

FlowTableIterator& FlowTableIterator::operator++()
{
    entry_ = entry_->lru_next;

    return *this;
}

FlowTableIterator FlowTableIterator::operator--(int)
{
    FlowTableIterator tmp(*this);
    entry_ = entry_->lru_prev;

    return tmp;
}

FlowTableIterator& FlowTableIterator::operator--()
{
    entry_ = entry_->lru_prev;

    return *this;
}

FCFlowInfo& FlowTableIterator::operator*()
{
    return entry_->finfo;
}

const FCFlowInfo& FlowTableIterator::operator*() const
{
    return entry_->finfo;
}

FCFlowInfo* FlowTableIterator::operator->()
{
    return &(entry_->finfo);
}

const FCFlowInfo* FlowTableIterator::operator->() const
{
    return &(entry_->finfo);
}

bool FlowTableIterator::operator==(const FlowTableIterator& o) const
{
    return entry_ == o.entry_;
}

bool FlowTableIterator::operator!=(const FlowTableIterator& o) const
{
    return entry_ != o.entry_;
}

FlowTableReverseIterator::FlowTableReverseIterator(FlowListEntry* entry): entry_(entry) {}

FlowTableReverseIterator::FlowTableReverseIterator(const FlowTableReverseIterator& other): entry_(other.entry_) {}

FlowTableReverseIterator& FlowTableReverseIterator::operator=(const FlowTableReverseIterator& other)
{
    entry_ = other.entry_;

    return *this;
}

FlowTableReverseIterator FlowTableReverseIterator::operator++(int)
{
    FlowTableReverseIterator tmp(*this);
    entry_ = entry_->lru_prev;

    return tmp;
}

FlowTableReverseIterator& FlowTableReverseIterator::operator++()
{
    entry_ = entry_->lru_prev;

    return *this;
}

FlowTableReverseIterator FlowTableReverseIterator::operator--(int)
{
    FlowTableReverseIterator tmp(*this);
    entry_ = entry_->lru_next;

    return tmp;
}

FlowTableReverseIterator& FlowTableReverseIterator::operator--()
{
    entry_ = entry_->lru_next;

    return *this;
}

FCFlowInfo& FlowTableReverseIterator::operator*()
{
    return entry_->finfo;
}

const FCFlowInfo& FlowTableReverseIterator::operator*() const
{
    return entry_->finfo;
}

FCFlowInfo* FlowTableReverseIterator::operator->()
{
    return &(entry_->finfo);
}

const FCFlowInfo* FlowTableReverseIterator::operator->() const
{
    return &(entry_->finfo);
}

bool FlowTableReverseIterator::operator==(const FlowTableReverseIterator& o) const
{
    return entry_ == o.entry_;
}

bool FlowTableReverseIterator::operator!=(const FlowTableReverseIterator& o) const
{
    return entry_ != o.entry_;
}

ELEMENT_PROVIDES(FlowTableIterators)

CLICK_ENDDECLS

