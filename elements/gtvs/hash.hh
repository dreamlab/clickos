
#ifndef HASH_HH_
#define HASH_HH_

#include <click/config.h>

CLICK_DECLS

static inline uint32_t rot(uint32_t x, uint32_t k)
{
    return (x << k) | (x >> (32-k));
}

static inline void final(uint32_t& a, uint32_t& b, uint32_t& c)
{
    c ^= b;
    c -= rot(b, 14);
    a ^= c;
    a -= rot(c, 11);
    b ^= a;
    b -= rot(a, 25);
    c ^= b;
    c -= rot(b, 16);
    a ^= c;
    a -= rot(c, 4);
    b ^= a;
    b -= rot(a, 14);
    c ^= b;
    c -= rot(b, 24);
}

static inline  uint32_t hashword(uint32_t k0, uint32_t k1, uint32_t k2, uint32_t initval)
{
    uint32_t a,b,c;

    /* Set up the internal state */
    a = b = c = 0xdeadbeef + (((uint32_t)3)<<2) + initval;

    c+=k2;
    b+=k1;
    a+=k0;
    final(a,b,c);

    return c;
}

CLICK_ENDDECLS
#endif

