#ifndef CLICK_STATELESSFLOWPINNER_HH
#define CLICK_STATELESSFLOWPINNER_HH

#include <click/element.hh>

/*
=c

StatelessFlowPinner([, ANNO])

=s classifier
load balance packets using the annotated jhash if it exists. Depending by the hash value and the number
of output ports with a modulo algorithm. If no hash annotation is found or with no IP packets a round-robin
algorithm is used instead. 

=d
Expects IP packets as input to take advantage of the hash annotation. Should be placed downstream a StoreIPJHash.
Use the jenkins hash value (if exists) annotated in the packet.

This element gets the packet's EXTRA_PACKETS_ANNO annotation by default, but the ANNO argument can
specify any 4-byte annotation.

a= StoreIPJHash

*/

CLICK_DECLS

class StatelessFlowPinner : public Element {

	public:

		StatelessFlowPinner();
		~StatelessFlowPinner();

		const char *class_name() const                { return "StatelessFlowPinner"; }
		const char *port_count() const                { return "1-2/1-"; }
		const char *processing() const                { return PUSH; }

		int configure(Vector<String> &, ErrorHandler *);

		int initialize(ErrorHandler *);

		void push(int, Packet *);

	private:

		int _queues;
		int _next;
		bool _fastmod;
		int _anno;

		inline int _get_queue();         // get output queue in a Round Robin fasion
		inline int _get_queue(uint32_t); // get output queue with Hash calculation

};

CLICK_ENDDECLS
#endif
