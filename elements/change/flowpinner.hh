#ifndef CLICK_FLOWPINNER_HH
#define CLICK_FLOWPINNER_HH

/*
=c
FlowPinner()

=s claassifier
load balance packets by using the Jenkins' hash value of the flow. Depending by the hash value and the number
of output ports with a modulo algorithm. If no hash annotation is found or with no IP packets a round-robin
algorithm is used instead. 

=d
This element expects IP packets and chooses the output based on the Flow hash code of the packet 

=processing
push

*/

#include <click/element.hh>

CLICK_DECLS

class FlowPinner : public Element {

	public:

		FlowPinner();
		~FlowPinner();

		const char *class_name() const                { return "FlowPinner"; }
		const char *port_count() const                { return "1-2/1-"; }
		const char *processing() const                { return PUSH; }

		int configure(Vector<String> &, ErrorHandler *);
		
		int initialize(ErrorHandler *);

		void push(int, Packet *);
		
		//~ void add_handlers();
		//~ static String read_handler(Element *, void *);
		//~ static int write_handler(const String&, Element*, void*, ErrorHandler*);
		
	private:
		
		int _queues;
		int _next;
		bool _fastmod;
		//~ Vector<int> _counters;

		inline int _get_queue();
		inline int _get_queue(uint32_t);
		//~ uint32_t getFlowHashKey(Packet* p);

};

CLICK_ENDDECLS
#endif
