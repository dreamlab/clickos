#ifndef CLICK_SIGMATCHER_HH
#define CLICK_SIGMATCHER_HH

/*
=c
SigMatcher()

=s
A signature matcher that accepts a very restricted version of the Suricata
signature definitions. The signature can be dynamically added and removed
with the "add"/"del" write handler.
 
A signature MUST be in the following form:

 allow tcp 192.168.0.1 80 > 192.168.20.10 6238 - pcre:pattern
 drop tcp 192.168.0.1 443 < 192.168.20.10 5238 - pcre:pattern/flags=A
 alert pop3 10.0.0.1 8080 <> 10.10.20.10 65512 - pcre:pattern
 drop html * * <> * * - pcre:pattern/flags=i

When a Flow is identified by the upstream FlowCache element, all the packets,
belonging to it, will be matched against the list of the installed signatures.
Once a match is found, the signature action will be executed.

=item actions

Possible signature action are:
  - allow - allow all the packets of the identified flow
  - alert - like allow but an alert is fired 
  - drop  - "drop" all the packet of the identified flow

Allowed and Alerted packets are sent to the output port 0 while the
each dropped once is sent to the output port 1 (if it exists) for 
further analysis or discarded immediately.

=item protocols

Only TCP or UDP is allowed or the ones specified in the AppMark element.

=item Addresses and Ports
Only IPv4 addresses are supported. the characted "*" can be used to allow any
address and port.

=item direction of the flow
It is possible to specified a direction for the flow to match.

Possible values are:
    - >,  match on src to dst packets
    - <,  match dst to src packets
    - <>, match on both  directions

=item modifier
At the moment only the "pcre:" modifier is supported. A set of flags to be used
in the expression matching can also be provided, after the string "/flags="

Supported flags are:

   - N, PCRE_NO_AUTO_CAPTURE
   - A, PCRE_ANCHORED
   - E, PCRE_DOLLAR_ENDONLY
   - G, PCRE_UNGREEDY
   - i, PCRE_CASELESS
   - m, PCRE_MULTILINE
   - s, PCRE_DOTALL
   - x, PCRE_EXTENDED
   - B, PCRE_NOTBOL
   - e, PCRE_NOTEOL

For an complete explanation of the above flags, see pcre(3).

Keywords are:

=over 8

=item MAX_PKTS

The maximum number of packets which are used to search for the configured
signatures. Default 10.

=item MAX_BYTES

The maximum number of payload bytes which are used to search for the
configured signatures. Default 2048.

=item USE_APPMARKS

Boolean. Whether the output is an APPMARK value or the l7-filter's pattern
name.

=item FLOWCACHE

Element. A FlowCache element that provides state tracking of TCP/UDP flows.
Default is the upstream FlowCache element.

=h list read-only
Returns a human-readable list of the signature to match the packets.

=h next_id read-only
Returns the next available ID for a signature. Using this ID, one can be
able to remove it or get its stats. 

=h number read-only
Returns the number of actual installed signatures.

=h stats read-only
Return the stats of the signature matcher itself, that is: drop/alert/allow packet
and byte counters.

=h sig_stats read-write
Returns the stats of all the signatures.

=h sig_stat read-write
Returns the stats of the signature with ID specified as parameter in the handler call.

=h add write-only
Adds a new signature. The new signature is compared with the others, and if one 
results the same (thaat is, same IPs, ports,  protocol, flags ) this one is 
replaced. If a flow was  already matched agaist the modified signature the same
old action will be taken for the subsequent packets of the same flow. The new action
will be taken only after the expiration of the previous one.


=h del write-only
Remove a signature specified by the ID used as parameter. Same consideration as the
add handler are applied.

=h signature_clean write-only
Removes all the signature installed.

=a
FlowCache,AppMarks,L7

*/

#include <click/element.hh>
#include <click/hashtable.hh>
#include "signature.hh"

CLICK_DECLS

#include <pcre.h>
// flow cache support
#include "../gtvs/flowcache.hh"
#include "../gtvs/appmarks.hh"
#include "../gtvs/patternutils.hh"
// flowclassifier support
#include "../gtvs/flowclassifier.hh"
// appmark support
#include "../gtvs/appmarks.hh"
// hash support
#include "flowkey.hh"

//#define SIGMATCHER_DEBUG

class SigMatcher : public Element, FlowCache::FlowStateHolder //, FlowClassifier::Listener
{

	public:

		SigMatcher();
		~SigMatcher();

		const char *class_name() const                { return "SigMatcher"; }
		const char *port_count() const                { return "1/1-2"; }
		const char *processing() const                { return PUSH; }

		int configure(Vector<String> &, ErrorHandler *);
		int initialize(ErrorHandler *);
		void cleanup(CleanupStage);

		void push(int, Packet *);

		void add_handlers();		
		
		// FlowClassifier::Listener
		//void flow_classified(const FlowCache::Flow& flow, const AppMark& mark);

		void write_header(DataExport& exporter) const;
		void write_flow_state(DataExport& exporter, const BaseFlowState* fs_) const;

		class FlowState;

	private:

		uint32_t _buffer_count;
		uint32_t _byte_count;
		//uint32_t _num_signatures;
		uint32_t _next_sig_id;
		FlowCache* _flow_cache;
		//FlowClassifier * _flow_classifier
		bool _use_appmarks;
		bool _dotted_ip;

		//typedef HashTable<uint32_t, uint8_t> FlowHashTable;
		//FlowHashTable _flows;
		Vector<Signature*> _signatures;

		//uint32_t _max_pkt_match;
		//stats
		uint32_t _alert_pkts;
		uint32_t _alert_bytes;
		uint32_t _drop_pkts;
		uint32_t _drop_bytes;
		uint32_t _allow_pkts;
		uint32_t _allow_bytes;

		friend class FlowState;
		
		/* handlers */
		enum { H_ADD,
				H_DEL,
				H_CLEAN,
				H_STATS,
				H_SIG_STATS,
				//H_RESET,
				H_LIST,
				H_NEXT_ID,
				H_NUM};

		void  update_stats(uint8_t action, uint32_t byte_count);

		int add_signature(Signature* , ErrorHandler *);
		int del_signature(uint32_t , ErrorHandler *);

		static String read_handler(Element* e, void*);
		static int add_signature_handler(const String&, Element*, void*, ErrorHandler*);
		static int del_signature_handler(const String&, Element*, void*, ErrorHandler*);
		static int clean_signature_handler(const String&, Element*, void*, ErrorHandler*);
		static int read_signature_stats_handler(int, String& s, Element* e, const Handler*, ErrorHandler* errh);

};

#define SIG_NOT_MATCHED       0x00
#define SIG_MATCHED           0x01
#define PROTO_MATCH           0x02
#define UPDATE_STATS          0x04
#define LIMIT_REACH           0x08

class SigMatcher::FlowState: public BaseFlowState, public AppMark
{
	public:

		FlowState();
		~FlowState();

		uint8_t handle_packet(Packet*, SigMatcher*);
		void append_to_buffer(const char* app_data, int appdatalen, uint32_t& max_bytes, char* buffer, uint32_t& lengthsofar);

	private:
		uint32_t packet_counter[2];
		uint32_t lengthsofar[2];
		uint32_t offset[2];
		char* buffer[2];
		uint8_t action;
		
		//FlowHashKey hash;
		//Vector<uint32_t> sig_ids;
		Signature* signature;
		uint8_t status;

};

CLICK_ENDDECLS
#endif
