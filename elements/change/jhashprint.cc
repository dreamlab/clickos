/*
 * jhashprint.{cc,hh} -- element print packets hash annotation
 * Vito Piserchia
 *
 * Copyright (c) 2013-2014 Dreamlab Technologies AG
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, subject to the conditions
 * listed in the Click LICENSE file. These conditions include: you must
 * preserve this copyright notice, and you cannot mention the copyright
 * holders in advertising related to the Software without their permission.
 * The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
 * notice is a summary of the Click LICENSE file; the license in that file is
 * legally binding.
 */

#include <click/config.h>
#include <click/error.hh>
#include <click/args.hh>
#include <click/straccum.hh>

#include "hash_anno.hh"

#include "jhashprint.hh"

#if CLICK_USERLEVEL
# include <stdio.h>
#endif

CLICK_DECLS

JHashPrint::JHashPrint()
{ }

JHashPrint::~JHashPrint()
{ }

int
JHashPrint::configure(Vector<String> &conf, ErrorHandler *errh)
{
	_active = true;
	int anno = JHASH_PACKETS_ANNO_OFFSET;
	
	if ((Args(conf, this, errh)
            .read_mp("LABEL", _label)
            .read_p("ACTIVE", _active)
            .read_p("ANNO", AnnoArg(1), anno)
            .complete() < 0))
	{
		return -1;
	}

	_anno = anno;
	return 0;
}

Packet *JHashPrint::simple_action(Packet *p)
{
	if (!_active)
		return p;

	StringAccum sa;

	if (_label)
		sa << _label << ": ";

	sa << p->anno_u32(_anno);

	click_chatter("%s", sa.c_str());

	return p;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(JHashPrint)
