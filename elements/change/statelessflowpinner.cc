/*
 * statelessflowpinner.{cc,hh} -- load balance packet using a Jenkins' hash previously set up.
 * Vito Piserchia
 *
 * Copyright (c) 2013-2014 Dreamlab Technologies AG
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, subject to the conditions
 * listed in the Click LICENSE file. These conditions include: you must
 * preserve this copyright notice, and you cannot mention the copyright
 * holders in advertising related to the Software without their permission.
 * The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
 * notice is a summary of the Click LICENSE file; the license in that file is
 * legally binding.
 */

/* click common header files */
#include <click/config.h>
//#include <click/error.hh>
#include <click/args.hh>

/* packet headers */
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>

#include "hash_anno.hh"
#include "statelessflowpinner.hh"

CLICK_DECLS

StatelessFlowPinner::StatelessFlowPinner()
  : _queues(1), _next(-1), _fastmod(false)
{ }

StatelessFlowPinner::~StatelessFlowPinner()
{ }

int
StatelessFlowPinner::configure(Vector<String> &conf, ErrorHandler *errh)
{
	int anno = JHASH_PACKETS_ANNO_OFFSET;
	if ((Args(conf, this, errh).read_p("ANNO", AnnoArg(4), anno).complete() < 0))
	{
		return -1;
	}

	_anno = anno;
    return 0;
};

int
StatelessFlowPinner::initialize(ErrorHandler *errh)
{
	_queues = noutputs();
	/*
	 * noutputs() is a power of 2?
	 * 
	 * From www.exploringbinary.com/ten-ways-to-check-if-an-integer-is-a-power-of-two-in-c/
	 * 
	 * 10: Complement and Compare
	 */	
	_fastmod = ((_queues != 0) && ((_queues & (~_queues + 1)) == _queues));
	
	return 0;
};

inline int
StatelessFlowPinner::_get_queue()
{	
	++_next;
	int queue = _fastmod ? _next & (_queues - 1) : _next % _queues;
	_next = queue; 
	return queue;
};

inline int
StatelessFlowPinner::_get_queue(uint32_t key)
{
	return _fastmod ? key & (_queues - 1) : key % _queues;
};

void
StatelessFlowPinner::push(int unused, Packet *p)
{
	const click_ip *ip_hdr;

	if (!p->has_network_header())
	{
		output(_get_queue()).push(p);
		return;
	}
	ip_hdr = p->ip_header();

	/* not an UDP or TCP ?*/
	if ( !( ip_hdr->ip_p == IP_PROTO_TCP || ip_hdr->ip_p == IP_PROTO_UDP) )
	{
		output(_get_queue()).push(p);
		return;
	}
	
	output(_get_queue(p->anno_u32(_anno))).push(p);
};

CLICK_ENDDECLS
EXPORT_ELEMENT(StatelessFlowPinner)
