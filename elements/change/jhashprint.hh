#ifndef CLICK_JHASHPRINT_HH
#define CLICK_JHASHPRINT_HH
#include <click/element.hh>
CLICK_DECLS
/*
=c
JHashPrint(LABEL [, ANNO, ACTIVE])

=s
pretty-prints Jenkins hash

=d
Prints out Jenkinks hash (if present) in a human-readable format.
Should be placed downstream of a StoreIPJHash.

This element sets the packet's EXTRA_PACKETS_ANNO annotation by default, but the ANNO argument can
specify any 4-byte annotation.

This element is active by default, but it can be deactivate using the ACTIVE param; 0 means deactive.

A label can be added to the ouput message using the LABEL param.

=processing
agnostic

=a StoreIPJHash, FlowPinner */

class JHashPrint : public Element {
public:
	JHashPrint();
	~JHashPrint();

	const char *class_name() const { return "JHashPrint"; }
	const char *port_count() const { return PORTS_1_1; }
	const char *processing() const { return AGNOSTIC; }

	int configure(Vector<String> &,ErrorHandler*);
	Packet *simple_action(Packet *);

private:
	String _label;
	bool _active;
	int _anno;
};

CLICK_ENDDECLS
#endif
