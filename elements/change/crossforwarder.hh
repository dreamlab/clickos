#ifndef CLICK_CROSSFORWARDER_HH
#define CLICK_CROSSFORWARDER_HH

/*
=c
CrossForwarder()

=s
simple packet forwarder from input 0  to output 1 and viceversa.

=a

*/

#include <click/element.hh>

CLICK_DECLS

class CrossForwarder : public Element {

public:
		CrossForwarder();
		~CrossForwarder();

		const char *class_name() const { return "CrossForwarder"; }
		const char *port_count() const { return "2/2"; }
		const char *processing() const { return "hl/lp"; }
		const char *flow_code()  const { return "xy/yx"; } 

		Packet *simple_action(Packet *);

};

CLICK_ENDDECLS
#endif
