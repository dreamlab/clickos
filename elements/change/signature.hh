#ifndef CLICK_SIGNATURE_HH
#define CLICK_SIGNATURE_HH
/*
 * signature.hh          signature definition and utilities 
 * 
 * Vito Piserchia
 *
 * Copyright (c) 2013-2014 Dreamlab Technologies AG
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of Dreamlab Technologies AG nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <click/config.h>
#include <click/confparse.hh>
#include <click/string.hh>
#include <click/straccum.hh>
#include <click/glue.hh>
#include <click/ipaddress.hh>
#include <click/handler.hh>
#include <click/string.hh>
#include <click/error.hh>
#include <click/glue.hh>

CLICK_DECLS

#include <pcre.h>
//#include "flowdesc.hh"

// appmark support
#include "../gtvs/appmarks.hh"
#include "../gtvs/patternutils.hh"
#include "../gtvs/nipquad.hh"

/* some definition for matching purpose */

#define EXCEPT_SRC_IP          0x01
#define EXCEPT_DST_IP          0x02
#define ANY_SRC_PORT           0x04
#define ANY_DST_PORT           0x08
#define ANY_FLAGS              0x10
#define EXCEPT_SRC_PORT        0x20
#define EXCEPT_DST_PORT        0x40
#define BIDIRECTIONAL          0x80
#define ANY_SRC_IP            0x100
#define ANY_DST_IP            0x200
#define SIGNATURE_TYPE_REGEX  0x400
#define SIGNATURE_TYPE_STRING 0x800
#define APP_MARK_SET         0x1000
#define ANY_PROTO            0x2000

#define SRC_TO_DST             0x01
#define DST_TO_SRC             0x02

class Signature {
	
	public:
		struct RegularExpression
		{
			String pattern;     /*< string pattern itself */
			uint pminsize;      /*< pcre_fullinfo PCRE_INFO_MINLENGTH */
			int poptions;       /*< pcre_fullinfo PCRE_INFO_OPTIONS */
			int cflags;         /*< for regcomp */
			int eflags;         /*< for regexec */
			pcre *preg;         /*< compiled regular expression */
			pcre_extra *preg_e; /*< extra compiled stuff */
			
			unsigned initialized;
			
		};
	
	public:
		Signature(uint32_t signature_id, uint32_t src_addr, uint32_t dst_addr, uint16_t src_port, uint16_t dst_port, uint16_t protocol, bool dotted_ip);
		//Signature(uint32_t signature_id, const FlowDesc  &flow, bool dotted_ip);
		
		~Signature();
		// fields
		inline uint32_t src_addr() const { return _src_addr; }
		inline uint32_t dst_addr() const { return _dst_addr; }
		inline uint16_t src_port() const { return _src_port; }
		inline uint16_t dst_port() const { return _dst_port; }
		inline uint16_t protocol() const { return _protocol; }
		inline uint8_t action() const { return _action; }
		inline uint32_t flags() const { return _flags; }
		inline uint32_t signature_id() const{ return _signature_id; }
		inline AppMark mark() const { return _mark; };

		inline bool active() const { return _active; }
		inline RegularExpression regex() const { return _regex; }
		
		inline uint32_t pkt_count() const { return _pkt_count; }
		inline uint32_t byte_count() const { return _byte_count; }
		
		inline void set_active(bool active) { _active = active; }
		inline void set_action(uint8_t action) { _action = action; }
		inline void set_flags(uint32_t flags) { _flags = flags; }
		inline void set_pattern(const String &, ErrorHandler *);
		inline void set_mark(const AppMark&  mark) { _mark = mark; }
		

		inline void update_stats(uint32_t byte_count);
		inline void add_flow();
		inline void del_flow();

		inline bool operator==(const Signature &) const;
		inline bool operator!=(const Signature &) const;
		inline bool equalto(uint32_t src_addr, uint32_t dst_addr, uint16_t src_port,uint16_t dst_port, uint16_t protocol, AppMark mark, uint32_t flags);

		void reset_counts();
		String s() const;
		
		String dump_stats() const;

		//int match_flow(FlowDesc &flow, const unsigned char *, uint32_t);
		int match_data(char* buffer, uint buflen, int offset);

		enum {
			SIG_TYPE_NOT_DEFINED,
			SIG_TYPE_REGEX,
			SIG_TYPE_STRING
		};
		
		enum {
			SIG_ACTION_ALLOW = 0,
			SIG_ACTION_DROP,
			SIG_ACTION_ALERT
		};

		static Signature* parse_signature(uint32_t , const String&, bool, ErrorHandler* );
		static String action2str(uint8_t);

	private:

		uint32_t _signature_id;		
		uint32_t _src_addr;
		uint32_t _dst_addr;
		uint16_t _src_port;
		uint16_t _dst_port;
		uint16_t _protocol;
		AppMark _mark;
		
		bool _dotted_ip;
		bool _active;		
		uint32_t _flags;
				
		uint8_t _action;
		RegularExpression _regex;

		// stats
		uint32_t _pkt_count;
		uint32_t _byte_count;
		uint32_t _matching_flows;

		ErrorHandler *_errh;
		
		bool parse_flags(const String& line, ErrorHandler* errh);
		bool parse_pattern_string(const String& pattern_str, ErrorHandler* errh);
		String pre_process(const String& s);


};

inline bool
Signature::equalto(uint32_t src_addr,
			uint32_t dst_addr,
			uint16_t src_port,
			uint16_t dst_port,
			uint16_t protocol,
			AppMark mark,
			uint32_t flags)
{
	bool e =  ( ( _src_addr == src_addr) && 
			(_dst_addr      == dst_addr) &&
			(_src_port      == src_port) &&
			(_dst_port      == dst_port) &&
			(_flags         == flags));
	if (_flags & APP_MARK_SET)
	{
		e &= _mark == mark; 
	}
	else
	{
		e &= _protocol == protocol;
	}
	return e;
}

inline bool
Signature::operator==(const Signature &rhs) const
{
	bool e =  ( ( _src_addr == rhs.src_addr()) && 
			(_dst_addr      == rhs.dst_addr()) &&
			(_src_port      == rhs.src_port()) &&
			(_dst_port      == rhs.dst_port()) &&
			(_flags         == rhs.flags()) );
	if (_flags & APP_MARK_SET)
	{
		e &= _mark == rhs.mark(); 
	}
	else
	{
		e &= _protocol == rhs.protocol();
	}
	return e;
}


inline bool
Signature::operator!=(const Signature &rhs) const
{
	return !(*this == rhs);
}

Signature::Signature(uint32_t signature_id, uint32_t src_addr, uint32_t dst_addr,
						uint16_t src_port, uint16_t dst_port, uint16_t protocol, bool dotted_ip)
	: _signature_id(signature_id)
	, _src_addr(src_addr)
	, _dst_addr(dst_addr)
	, _src_port(src_port)
	, _dst_port(dst_port)
	, _protocol(protocol)
	, _dotted_ip(dotted_ip)
	, _active(false)
	, _flags(0x00)
	, _pkt_count(0)
	, _byte_count(0)
	, _matching_flows(0)
{
	//regex
	_regex.pminsize = 0;
	_regex.poptions = 0;
	_regex.cflags = PCRE_EXTENDED;
	_regex.eflags = 0;
	_regex.initialized = 0;

	//mark
	_mark = AppMarks::empty_mark;
		
}

//Signature::Signature(uint32_t signature_id, const FlowDesc &flow, bool dotted_ip)
	//: _signature_id(signature_id)
	//, _dotted_ip(dotted_ip)
	//, _active(false)
	//, _flags(0x00)
	//, _pkt_count(0)
	//, _byte_count(0)
	//, _matching_flows(0)
//{
	//_src_addr = flow.src_addr();
	//_dst_addr = flow.dst_addr();
	//_src_port = flow.src_port();
	//_dst_port = flow.dst_port();
	//_protocol = flow.protocol();
	//
	////regex
	//_regex.pminsize = 0;
	//_regex.poptions = 0;
	//_regex.cflags = PCRE_EXTENDED;
	//_regex.eflags = 0;
	//_regex.initialized = 0;
	//
	////mark
	//_mark = AppMarks::empty_mark;
//}

Signature::~Signature()
{
	if ((_flags & SIGNATURE_TYPE_REGEX) && _regex.initialized)
		pcre_free(_regex.preg);
}

void 
Signature::reset_counts()
{
	_pkt_count = 0;
	_byte_count = 0;
}

inline void 
Signature::update_stats(uint32_t byte_count)
{
	_byte_count += byte_count;
	++_pkt_count;
}

inline void 
Signature::add_flow()
{
	++_matching_flows;
}


inline void 
Signature::del_flow()
{
	--_matching_flows;
}

String
Signature::action2str(uint8_t type)
{
	static const String labels[] = {
		"ALLOW",
		"DROP",
		"ALERT"
	};

	return String(labels[type]);
}

String
Signature::s() const
{
	StringAccum sa;

	sa << "SIGID: " <<  _signature_id << " ";
	sa << "ACTION: " << Signature::action2str(_action) << " - ";
	if (_flags & ANY_SRC_IP)
		sa << "ANY";
	else
	{
		uint32_t src = ntohl(_src_addr);
		if (_dotted_ip)
		{       
			StringAccum ss;
			ss.snprintf(15, "%u.%u.%u.%u", NIPQUAD(src));
			sa << ss.take_string();
		}
		else    
			sa << src;
	}
	if (_flags & ANY_SRC_PORT)
		sa << ":" << "ANY";
	else
		sa << ":" << ntohs(_src_port);

	if (_flags & BIDIRECTIONAL)
		sa << " <-> ";
	else
		sa << " -> ";
	
	if (_flags & ANY_DST_IP)
		sa << "ANY";
	else
	{
		uint32_t dst = ntohl(_dst_addr);
		if (_dotted_ip)
		{   
			StringAccum ss;
			ss.snprintf(15, "%u.%u.%u.%u", NIPQUAD(dst));
			sa << ss.take_string();
		}
		else
			sa << dst;
	}
	
	if (_flags & ANY_DST_PORT)
		sa << ":" << "ANY";
	else
		sa << ":" << ntohs(_dst_port);

	sa << " (";
	// proto or appmark?
	if (_flags & APP_MARK_SET)
	{
		sa << AppMarks::default_instance()->resolve_proto(_mark).c_str();
		
	}
	else if (_protocol == IP_PROTO_TCP)
		sa << "tcp";
	else
		sa << "udp";
	
	sa << ") ";
	sa << "[" << _regex.pattern << "]";

	return sa.c_str();
}

String
Signature::dump_stats() const
{
	StringAccum sa;
	sa << "SIGID: " << _signature_id << " - [flows:" << _matching_flows << ", pkts: " << _pkt_count  << ", bytes: " << _byte_count << "]";
	return sa.take_string();
}

bool
Signature::parse_pattern_string(const String& pattern_str, ErrorHandler* errh)
{
	if ( pattern_str.length() == 0 )
	{
		errh->error("empty patterns are not allowed");
		return false;
	}

    String flags_str;
    String value;
    int flags = pattern_str.find_left(String("/flags="));
    if ( flags >= 0 )
    {
		_regex.pattern = String(pattern_str.substring(0, flags));
		
		String value = pattern_str.substring(flags + 7);
		if ( !parse_flags(value.c_str(), errh) )
		{
			return false;
		}
	} else {
		_regex.pattern = String(pattern_str.substring(0, pattern_str.length()));
	}
	
	return true;
}


bool
Signature::parse_flags(const String& line, ErrorHandler* errh)
{
    const char *op = line.c_str();
    _regex.cflags = 0;
    _regex.eflags = 0;
    
    if (op != NULL)
    {
		while(*op)
		{
			switch (*op) {
				
				case 'N':
					_regex.cflags |= PCRE_NO_AUTO_CAPTURE;
					break;
				case 'A':
					_regex.cflags |= PCRE_ANCHORED;
                    break;
                case 'E':
                    _regex.cflags |= PCRE_DOLLAR_ENDONLY;
                    break;
                case 'G':
                    _regex.cflags |= PCRE_UNGREEDY;
                    break;
                case 'i':
                    _regex.cflags |= PCRE_CASELESS;
                    break;
                case 'm':
                    _regex.cflags |= PCRE_MULTILINE;
                    break;
                case 's':
                    _regex.cflags |= PCRE_DOTALL;
                    break;
                case 'x':
                    _regex.cflags |= PCRE_EXTENDED;
                    break;
                case 'B':
                    _regex.eflags |= PCRE_NOTBOL;
                    break;
				case 'e':
                    _regex.eflags |= PCRE_NOTEOL;
                    break;
				default:
				    errh->error("<error: encountered unknown flag in pattern \"%c\". "
									"MUST be one of N A E G i m s x B e. Skipping", *op);
					break;
			}
			op++;
		}
	}
    return true;
}

String
Signature::pre_process(const String& s)
{
    size_t len = s.length();
    char* result = new char[len + 1];
    size_t sindex = 0;
    size_t rindex = 0;
    
    while (sindex < len)
    {
        if (sindex + 3 < len && s[sindex] == '\\' && s[sindex + 1] == 'x' && isxdigit(s[sindex + 2]) && isxdigit(s[sindex + 3]))
        {
            result[rindex] = hex2dec(s[sindex + 2]) * 16 + hex2dec(s[sindex + 3]);
            
            StringAccum sa;
            switch (result[rindex])
            {
            case '$':
            case '(':
            case ')':
            case '*':
            case '+':
            case '.':
            case '?':
            case '[':
            case ']':
            case '^':
            case '|':
            case '{':
            case '}':
            case '\\':
                sa << "Warning: regexp contains a regexp control character, " << result[rindex];
                sa << ", in hex (\\x" << s[sindex + 2] << s[sindex + 3];
                sa << ".\nI recommend that you write this as " << result[rindex];
                sa << " or \\" << result[rindex] << " depending on what you meant." << "\n";
                break;
            case '\0':
                sa << "Warning: null (\\x00) in layer7 regexp. " << "A null terminates the regexp string!" << "\n";
                break;
            default:
                break;
            }
            sindex += 3; /* 4 total */

            click_chatter(sa.take_string().c_str());
        }
        else
            result[rindex] = s[sindex];
    
        ++sindex;
        ++rindex;
    }
    result[rindex] = '\0';
    String res = result;
    delete[] result;
    
    return res;
}

void
Signature::set_pattern(const String &pattern_str, ErrorHandler *errh)
{
	const char* errbuf;
	int erroff;
	int res;
	
	// free the old regex expression if this signature is a PCRE and it's initialized
	if ( (_flags & SIGNATURE_TYPE_REGEX) && _regex.initialized )
	{
		if (_regex.preg)
			pcre_free(_regex.preg);
		if (_regex.preg_e)
			pcre_free_study(_regex.preg_e);
	}
	
	// regex parsing
	_regex.initialized = 0;
	if (_flags & SIGNATURE_TYPE_REGEX )
	{
		if (!parse_pattern_string(pattern_str.c_str(), errh) )
		{
			StringAccum sa;
			sa << "error parsing /" << pattern_str << "/";
			errh->error(sa.c_str());
			return;
		}

		if (!(_regex.preg = pcre_compile(pre_process(_regex.pattern).c_str(), _regex.cflags, &errbuf, &erroff, NULL)))
		{
			StringAccum sa;
			sa << "error compiling /" << _regex.pattern << "/\n";
			sa << "error offset: " << erroff << "\n";
			sa << "error message: " << errbuf;
			errh->error(sa.c_str());
			return;
		}

		if(!(_regex.preg_e = pcre_study(_regex.preg, _regex.eflags, &errbuf)) )
		{
			if (errbuf)
			{
				pcre_free(_regex.preg);
				StringAccum sa;
				sa << "error pcre study /" << _regex.pattern << "/\n";
				sa << "error message: " << errbuf;
				errh->error(sa.c_str());
				return;
			}
		}

		if( (res = pcre_fullinfo(_regex.preg, _regex.preg_e, PCRE_INFO_MINLENGTH, &_regex.pminsize)) < 0)
		{
			pcre_free_study(_regex.preg_e);
			pcre_free(_regex.preg);
			StringAccum sa;
			sa << "error pcre_fullinfo: " << res;
			errh->error(sa.c_str());
			return;
		}
		if( (res = pcre_fullinfo(_regex.preg, _regex.preg_e, PCRE_INFO_OPTIONS, &_regex.poptions)) < 0)
		{
			pcre_free_study(_regex.preg_e);
			pcre_free(_regex.preg);
			StringAccum sa;
			sa << "error pcre_fullinfo: " << res;
			errh->error(sa.c_str());
			return;
		}
		
		_regex.initialized = 1; // TODO: move up in the signature!!
	}

}

//int
//Signature::match_flow(FlowDesc &flow, const unsigned char * data, uint32_t len)
//{
	//int result = Signature::SIG_ACTION_ALLOW;
	////check the 5 tuple
	//if ( flow.equalto(_src_addr, _dst_addr, _src_addr, _dst_port, _protocol) )
	//{
		//result = match_data(data, len);
	//}
	//return result;
//}

//int
//Signature::match_data(const unsigned char * data, uint32_t len)
//{
	//if (!_errh)
        //_errh = ErrorHandler::default_handler();
//
	//if ((_flags & SIGNATURE_TYPE_REGEX ) && _regex.initialized)
	//{
		//int ovector[30];
		//int rc;
		//if ( (rc = pcre_exec(_regex.preg, NULL, (const char * )data, len, 0, 0, ovector, 30 ) ) < 0 )
		//{
			//switch( rc )
			//{
				//case PCRE_ERROR_NOMATCH:
					//break;
				//default:
					//_errh->error( "Matching error: %d", rc );
					//break;
			//}
		//} else 
			//return _action;
	//}		
	//return Signature::SIG_ACTION_ALLOW;
//}

int
Signature::match_data(char* buffer, uint buflen, int offset=0)
{
	short ret = -1;
	if ( !_errh )
        _errh = ErrorHandler::default_handler();

	if ( (_flags & SIGNATURE_TYPE_REGEX ) && _regex.initialized)
	{
		StringAccum sa;
		ret = pcre_exec(_regex.preg, _regex.preg_e, buffer, buflen, offset, _regex.eflags, NULL, 0);
		if ( ret < 0) // we don't care about NOMATCH
		{
			switch(ret) {
				case PCRE_ERROR_NOMATCH:
					break;
				case PCRE_ERROR_NULL:
					sa << "Something was null";
					break;
				case PCRE_ERROR_BADOPTION:
					sa << "A bad option was passed";
					break;
				case PCRE_ERROR_BADMAGIC:
					sa << "Magic number bad (compiled re corrupt?)";
					break;
				case PCRE_ERROR_UNKNOWN_OPCODE:
					sa << "Something kooky in the compiled re";
					break;
				case PCRE_ERROR_NOMEMORY:
					sa << "Ran out of memory";
					break;
				default:
					sa << "Unknown error [" << ret << "]";
					break;
			}
		}
		if ( sa.length() )
			click_chatter("SigMatcher::matches: %s", sa.c_str());
	}
	return ret;
}


/*
 * Signature are in the form of:
 *     allow tcp 192.168.0.1 80 > 192.168.20.10 6238 - pcre:pattern
 *     drop tcp 192.168.0.1 443 < 192.168.20.10 5238 - pcre:pattern
 *     alert tcp 10.0.0.1 8080 <> 10.10.20.10 65512 - pcre:pattern
 */
Signature* Signature::parse_signature(uint32_t signature_id, const String &conf, bool dotted_ip, ErrorHandler *errh)
{	
	String s = cp_uncomment(conf).trim_space();
	uint32_t flags = 0x00;
	
	/* start the parsing */
	
	int l = 0;          /*< start index in the string */
	int r = s.length(); /*< end index in the string */
	int len = 0;        /*< length of the substring, from start to end */
	
	/* action */
	uint8_t action;
	
	r = s.find_left(" ", l);
	len = r - l;
	if (r < 0 || len < 0)
	{
		errh->error("<invalid action>");
		return 0;
	}
	String saction(s.substring(l, len));
	
	if (saction.lower() == "allow")
		action = Signature::SIG_ACTION_ALLOW;
	else if (saction.lower() == "drop")
		action = Signature::SIG_ACTION_DROP;
	else if (saction.lower() == "alert")
		action = Signature::SIG_ACTION_ALERT;
	else
	{
		errh->error("<error: action must be \"allow\", \"drop\" or \"alert\", found \"%s\">", saction.c_str());
		return 0;
	}

	/* protocol */
	uint16_t protocol = 0;
	AppMark mark = AppMarks::empty_mark;
	l = r + 1;
	r = s.find_left(" ", l);
	len = r - l;
	if (r < 0 || len < 0)
	{
		errh->error("<invalid protocol>");
		return 0;
	}
	String proto(s.substring(l, len));
	// TCP/UDP or a defined one
	if (proto == "*")
		flags |= ANY_PROTO;
	else if (proto.lower() == "tcp")
		protocol = IP_PROTO_TCP;
	else if (proto.lower() == "udp")
		protocol = IP_PROTO_UDP;
	else
	{	
		mark = AppMarks::default_instance()->lookup_by_proto_name(proto.lower().c_str());
		if (mark == AppMarks::empty_mark)
		{
			errh->error("<error: protocol must be \"tcp\" or \"udp\" or one defined in the AppMark element, found \"%s\">", proto.c_str());
			return 0;
		}
		
		flags |= APP_MARK_SET;
	}

	/* src */
	l = r + 1;
	r = s.find_left(" ", l);
	len = r - l;
	if (r < 0 || len < 0)
	{
		errh->error("<invalid src IP address>");
		return 0;
	}
	String src(s.substring(l, len));
	if (src == "*")
	{
		flags |= ANY_SRC_IP;
		src = "0.0.0.0";
	}
	
	IPAddress srcAddr(src);
	if (srcAddr.empty() && !(flags & ANY_SRC_IP))
	{
		errh->error("<error: src must be a valid IP address or \"*\": found \"%s\">", src.c_str());
		return 0;
	}

	/* src port */
	unsigned sport = 0;
	
	l = r + 1;
	r = s.find_left(" ", l);
	len = r - l;
	if (r < 0 || len < 0)
	{
		errh->error("<invalid src port>");
		return 0;
	}
	String sp(s.substring(l, len));
	if (sp == "*")
	{
		flags |= ANY_SRC_PORT;
		sp = "0";
	}
	
	if (!cp_unsigned(sp, &sport))
	{
		errh->error("<error: src port must be an integer > 0 or \"*\", found \"%s\"", sp.c_str());
		return 0;
	}
	/*  direction */
	unsigned direction = SRC_TO_DST;
	
	l = r + 1;
	r = s.find_left(" ", l);
	len = r - l;
	if (r < 0 || len < 0)
	{
		errh->error("<invalid direction>");
		return 0;
	}
	String dir(s.substring(l, len));
	if (dir == ">")
		direction = SRC_TO_DST;
	else if (dir == "<")
		direction = DST_TO_SRC;
	else if (dir == "<>")
		flags |= BIDIRECTIONAL;
	else {
		errh->error("<invalid direction: must be \">\", \"<\" or \"<>\", found \"%s\">", dir.c_str());
		return 0;
	}

	/* dst */
	l = r + 1;
	r = s.find_left(" ", l);
	len = r - l;
	if (r < 0 || len < 0)
	{
		errh->error("<invalid dst IP address>");
		return 0;
	}
	String dst(s.substring(l, len));
	if (dst == "*")
	{
		flags |= ANY_DST_IP;
		dst = "0.0.0.0";
	}
	
	IPAddress dstAddr(dst);
	if (dstAddr.empty() && !(flags & ANY_DST_IP))
	{
		errh->error("<error: dst must be a valid IP address or \"*\": found \"%s\">", dst.c_str());
		return 0;
	}

	/* dst port */
	unsigned dport;
	
	l = r + 1;
	r = s.find_left(" ", l);
	len = r - l;
	if (r < 0 || len < 0)
	{
		errh->error("<invalid dst port>");
		return 0;
	}
	String dp(s.substring(l, len));
	if (dp == "*")
	{
		flags |= ANY_DST_PORT;
		dp = "0";
	}
	
	if (!cp_unsigned(dp, &dport))
	{
		errh->error("<error: dst port must be an integer > 0 or \"*\", found \"%s\"", dp.c_str());
		return 0;
	}

	// pattern
	l = r + 1;
	r = s.find_left("- ", l);
	l = r + 2;
	len = s.length() - l;
	if (r < 0 || len < 0)
	{
		errh->error("<error: invalid modifier start>");
		return 0;	
	}
	
	if (!s.substring(l, len).starts_with("pcre:"))
	{
		errh->error("<error: pattern must start with \"pcre:\">");
		return 0;
	}
	else
	{
		
		flags |= SIGNATURE_TYPE_REGEX;
	}	
	
	String pattern(s.substring(l+5, len));

	Signature* sig;
	if (direction == SRC_TO_DST)
	{
		sig = new Signature(signature_id, srcAddr.addr(), dstAddr.addr(), htons(sport), htons(dport), protocol, dotted_ip);
	}
	else
	{
		sig = new Signature(signature_id, dstAddr.addr(), srcAddr.addr(), htons(dport), htons(sport), protocol, dotted_ip);
	}
	
	if (!sig) {
		errh->error("out of memory");
		return 0;
	}

	sig->set_mark(mark);
	sig->set_action(action);
	sig->set_flags(flags);
	sig->set_active(true);
	sig->set_pattern(pattern, errh);
	
	if (!sig->_regex.initialized)
	{
		delete sig;
		return 0;
	}
	
	return sig;
}

CLICK_ENDDECLS

#endif
