#ifndef CLICK_STORE_IP_JHASH_HH
#define CLICK_STORE_IP_JHASH_HH

CLICK_DECLS
#include <click/element.hh>

/*
=c

StoreIPJHash([BIDIR] [, ANNO])

=s classifier
store the Jenkins Hash of an IPv4 packets

=d

Expects IP packets as input. Should be placed downstream of a CheckIPHeader or
equivalent element. Only TCP and UDP packets are suppported.

Store the jenkins hash in the packets as annotation using (or not) a symmetric
alghorithm depending by the BIDIR param (if specified), the DEFAULT is asymmetric,
i.e. each direction of the network, characterized by its 5-tuple, will have a
different hash value. A value equal to zero means asymmetric hashing,
otherwise symmetric hashing is used. 

Thsi element sets the packet's EXTRA_PACKETS_ANNO annotation by default, but the ANNO argument can
specify any 4-byte annotation.

=a CheckIPHeader, StatelessFlowPinner*/

class StoreIPJHash : public Element {

	public:

		StoreIPJHash();
		~StoreIPJHash();

		const char *class_name() const                { return "StoreIPJHash"; }
		const char *port_count() const                { return "1/1"; }
		const char *processing() const                { return AGNOSTIC; }

		int configure(Vector<String> &, ErrorHandler *);

		Packet *simple_action(Packet *);

	private:
		uint16_t _bidir;
		uint8_t  _anno;

};

CLICK_ENDDECLS
#endif

