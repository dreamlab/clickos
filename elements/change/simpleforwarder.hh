#ifndef CLICK_SIMPLEFORWARDER_HH
#define CLICK_SIMPLEFORWARDER_HH

/*

=c
SimpleForwarder()

=s 
simple packet forwarder from its input to its output ports.


=a
SimpleForwarder()

*/


#include <click/element.hh>

CLICK_DECLS

class SimpleForwarder : public Element {

public:
		SimpleForwarder();
		~SimpleForwarder();

		const char *class_name() const { return "SimpleForwarder"; }
		const char *port_count() const { return "1/1"; }
		const char *processing() const { return AGNOSTIC; }
		
		Packet *simple_action(Packet *);
};

CLICK_ENDDECLS
#endif
