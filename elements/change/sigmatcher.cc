/*
 * sigmatcher.{cc,hh} -- signature matcher
 * 
 * Vito Piserchia
 *
 * Copyright (c) 2013-2014 Dreamlab Technologies AG
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution.
 * * Neither the name of Dreamlab Technologies AG nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/string.hh>

#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>

#include <pcre.h>

#include "sigmatcher.hh"

CLICK_DECLS
class ErrorHandler;

SigMatcher::SigMatcher()
	: //_num_signatures(0),
	_buffer_count(10)
	, _byte_count(2048)
	, _next_sig_id(0)
	//, _max_pkt_match(0)
	, _alert_pkts(0)
	, _alert_bytes(0)
	, _drop_pkts(0)
	, _drop_bytes(0)
	, _allow_pkts(0)
	, _allow_bytes(0)
{
}

SigMatcher::~SigMatcher()
{
}

void
SigMatcher::cleanup(CleanupStage)
{
    _signatures.clear();
    //_flows.clear();
}

int
SigMatcher::configure(Vector<String> &conf, ErrorHandler *errh)
{

	if (noutputs() > 2)
	{
		click_chatter("%s{element}: requires up to maximum two output ports", this);
		return -1;
	}
	
	_dotted_ip = true;
	_use_appmarks = true;
	Element* af_element = 0;
	
	if (cp_va_kparse(conf, this, errh,
			"FLOWCACHE", cpkP, cpElement, &af_element,
			"MAX_PKTS", 0, cpUnsigned, &_buffer_count,
			"MAX_BYTES", 0, cpUnsigned, &_byte_count,
			"DOTTEDIP", 0, cpBool, &_dotted_ip,
			"USE_APPMARKS", 0, cpBool, &_use_appmarks,
			cpEnd) < 0)
		return -1;
	
	// flowcache
    if (af_element && !(_flow_cache = (FlowCache*)(af_element->cast("FlowCache"))))
        return errh->error("FLOWCACHE must be a FlowCache element");

    if (!_flow_cache)
        _flow_cache = FlowCache::upstream_instance(this);

    if (!_flow_cache)
        return errh->error("Initialization failure!");
	
	_flow_cache->register_flow_state_holder(this, name());
	
	// flowclassifier
	//_flow_classifier = FlowClassifier::upstream_instance(this);
//
    //if (!_flow_classifier)
        //return errh->error("Initialization failure: "
            //"no FlowClassifier found on the upstream ");
//
	//_flow_classifier->attach_listener(static_cast<FlowClassifier::Listener*>(this));

	return 0;
}

int
SigMatcher::initialize(ErrorHandler *errh)
{
	return 0;
}

void
SigMatcher::write_header(DataExport& exporter) const
{
	if (_use_appmarks) {
		exporter += AppMarks::default_instance()->column("SigID");
	} else {
		DataExport::Column::EnumType et;
		et.push_back("NC+" + String(_buffer_count));
		et.push_back("NC-" + String(_buffer_count));
		
		exporter += column("SigID", et, false);
	}

	exporter += constraint("SigID", DataExport::Constraint::KEY);
}

void
SigMatcher::write_flow_state(DataExport& exporter, const BaseFlowState* fs_) const
{

	const FlowState* fs = static_cast<const FlowState*>(fs_);
	if (_use_appmarks) {
		exporter << *((const AppMark *) fs);
	} else {
		if (*fs == AppMarks::empty_mark)
			exporter << "NC-" + String(_buffer_count);
		else if (*fs == AppMarks::unknown_mark)
			exporter << "NC+" + String(_buffer_count);
		else
			exporter << AppMarks::default_instance()->resolve_proto(*fs);
	}
}

//void
//SigMatcher::flow_classified(const FlowCache::Flow& flow, const AppMark& mark)
//{
//#ifdef SIGMATCHER_DEBUG
	//click_chatter("SigMatcher::flow_classified with mark %s", get_name().c_str()get_name().c_str();
	//click_chatter("flow: %s", FlowCache::Flow::s(flow).c_str());
//#endif
//}


void
SigMatcher::push(int port, Packet *p)
{
	uint8_t action = Signature::SIG_ACTION_ALLOW;
	FlowState* fs = _flow_cache->lookup_state<FlowState>(this, p);
	if (!fs )
	{
		fs = new FlowState();
		_flow_cache->state(this, p) = fs;

		if (!fs)
		{
			click_chatter("out of memory!");
			p->kill();

			return;
		}
	}

	action = fs->handle_packet(p, this);
	output( action == Signature::SIG_ACTION_DROP ? 1 : 0).push(p);
}

void
SigMatcher::update_stats(uint8_t action, uint32_t byte_count)
{
	switch (action)
	{
		case Signature::SIG_ACTION_DROP:
		{
			++_drop_pkts;
			_drop_bytes += byte_count;
			break;
		}
		case Signature::SIG_ACTION_ALERT:
		{
			++_alert_pkts;
			_alert_bytes += byte_count;
			break;
		}
		case Signature::SIG_ACTION_ALLOW:
		{
			++_allow_pkts;
			_allow_bytes += byte_count;
			break;
		}
		default:
			break;
	}
}

/* FlowState */
SigMatcher::FlowState::FlowState()
	: signature(0)
{
	packet_counter[0] = 0;
	packet_counter[1] = 0;
	lengthsofar[0] = 0;
	lengthsofar[1] = 0;
	buffer[0] = 0;
	buffer[1] = 0;
	offset[0] = 0;
	offset[1] = 0;

	set_mark(AppMarks::empty_mark);
	status = SIG_NOT_MATCHED;
	action = Signature::SIG_ACTION_ALLOW;
}

SigMatcher::FlowState::~FlowState()
{
	delete [] buffer[0];
	delete [] buffer[1];
}

uint8_t
SigMatcher::FlowState::handle_packet(Packet* p, SigMatcher* matcher)
{
	uint8_t found = SIG_NOT_MATCHED;

	int thlen;
	const click_ip* iph = p->ip_header();

	if (iph->ip_p == IP_PROTO_TCP)
		thlen = p->tcp_header()->th_off * 4;
	else
		thlen = 8; /* UDP header length */

	int dir = PAINT_ANNO(p);
	int payload = p->length() - (p->ip_header_length() + thlen);
	
	if(*this == AppMarks::empty_mark)
	{
		AppMark m(APPMARK_ANNO(p));
		if (m.get_mark())
		{
			set_mark(m);
		}
	}
	// already matched?
	if (status & SIG_MATCHED)
	{
		if (signature && signature->active())
		{
			action = signature->action();
			signature->update_stats((uint32_t) payload);
			matcher->update_stats(action, (uint32_t) payload);
			return action;
		}
		else
			status &= ~SIG_MATCHED;
	}
	// try to match this packets against the installed signatures
	if (!(status & SIG_MATCHED))
	{
		packet_counter[dir]++;
		if (payload > 0)
		{
			if (buffer[dir] == 0)
				buffer[dir] = new char[matcher->_byte_count + 1];
			
			const char* start = reinterpret_cast<const char*>(p->transport_header() + thlen);
			append_to_buffer(start, payload, matcher->_byte_count, buffer[dir], lengthsofar[dir]);
			
		}

		/* the flow the current packet belongs to */	
		const FlowCache::Flow flow = matcher->_flow_cache->lookup_flow(p);

		// this variables are in network order!
		uint32_t src   = flow.src_ip();
		uint32_t dst   = flow.dst_ip();
		uint16_t sport = flow.src_port();
		uint16_t dport = flow.dst_port();
		uint8_t proto  = flow.ip_proto();
	
		/* 
		 * Loop against all the installed signatures. It will break on the
		 * first signature that match the addresses, the ports and the protocol
		 * of the current packet.
		 */
		for (int i = 0; i < matcher->_signatures.size(); i++)
		{
			Signature* sig = matcher->_signatures[i];
			uint32_t flags = sig->flags();
			
			// check the 5-tuples of the packet against the signature's one
			if ( ((flags & ANY_SRC_IP  || src == sig->src_addr()) &&
				(flags & ANY_SRC_PORT  || sport == sig->src_port()) &&
				(flags & ANY_DST_IP    || dst == sig->dst_addr()) &&
				(flags & ANY_DST_PORT  || dport == sig->dst_port()))
				|| ( flags & BIDIRECTIONAL &&
				(flags & ANY_SRC_IP    || dst == sig->src_addr()) &&
				(flags & ANY_SRC_PORT  || dport == sig->src_port()) &&
				(flags & ANY_DST_IP    || src == sig->dst_addr()) &&
				(flags & ANY_DST_PORT  || sport == sig->dst_port())) )
			{
			
				// then check the protocol or the mark if specified in the signature
				if (flags & ANY_PROTO)
				{
					found |= PROTO_MATCH;
				}
				else if (flags & APP_MARK_SET && *this != AppMarks::empty_mark && *this == sig->mark())
				{
					found |= PROTO_MATCH;
				}
				else if ( (proto == sig->protocol()) )
				{
					found |= PROTO_MATCH;
				}
				if (found & PROTO_MATCH)
				{
					if (payload >  0)
					{
						if (sig->match_data(buffer[dir], lengthsofar[dir], offset[dir]) >= 0)
						{
#ifdef SIGMATCHER_DEBUG
							click_chatter("match");
#endif
							sig->add_flow();
							signature = sig;
							action = sig->action();
							status |= SIG_MATCHED;
							click_chatter("%s [SIGID=%d] [flow=%s]", Signature::action2str(action).c_str(), sig->signature_id(), FlowCache::Flow::s(flow).c_str());
							
							
							matcher->update_stats(action, (uint32_t) payload);
							sig->update_stats((uint32_t) payload);

							break;
						}
						else
						{
							uint32_t len = lengthsofar[dir];
							uint32_t off = offset[dir];
							offset[dir] = len + off; 
						}
					}
					
					found &= ~PROTO_MATCH;
				}

			}
		}
		
		// Are the limits reached?
		if (status == SIG_NOT_MATCHED)
			if (packet_counter[dir] >= matcher->_buffer_count || lengthsofar[dir] == matcher->_byte_count)
			{
				status |= LIMIT_REACH;
#ifdef SIGMATCHER_DEBUG
				click_chatter("limit reach");
#endif
			}
		// remove the data in case of match or limits reached
		if ( status != SIG_NOT_MATCHED )
		{
			delete [] buffer[0];
			delete [] buffer[1];
			
			buffer[0] = 0;
			buffer[1] = 0;
			packet_counter[0] = 0;
			packet_counter[1] = 0;
			offset[0] = 0;
			offset[1] = 0;
			lengthsofar[0] = 0;
			lengthsofar[1] = 0;
			
			// restart
			status &= ~LIMIT_REACH;
			
		}		
	}
	
	return action;
}

void
SigMatcher::FlowState::append_to_buffer(const char* app_data, int appdatalen, uint32_t& max_bytes, char* buffer, uint32_t& lengthsofar)
{
	uint32_t length = 0;
	uint32_t oldlength = lengthsofar;
	uint32_t ln = max_bytes - lengthsofar;

	/*
	 * Strip nulls.  Add it to the end of the current data.
	 */
	uint32_t len = static_cast<uint32_t>(appdatalen);
	
	for (uint32_t i=0; length<ln && i<len; ++i)
	{
		if (app_data[i] != '\0')
		{
			buffer[length + oldlength] = app_data[i];
			++length;
		}
	}

	buffer[length + oldlength] = '\0';
	lengthsofar += length;
}

/*
 * HANDLERS
 */

int
SigMatcher::add_signature(Signature* sig, ErrorHandler *errh)
{
	// search and overwrite any existing signature
	for (int i = 0; i < _signatures.size(); i++)
	{
		if (*_signatures[i] == *sig)
		{
			_signatures[i]->set_action(sig->action());
			_signatures[i]->set_pattern(sig->regex().pattern, errh);
			_signatures[i]->set_active(true);
			if (_signatures[i]->regex().initialized == 0)
				return -1;
			click_chatter("[SigMatcher] signature MOD: %s", _signatures[i]->s().c_str());
			return 0;
		}
	}
	// maybe make a new slot
	sig->set_pattern(sig->regex().pattern, errh);
	if (sig->regex().initialized == 0)
		return -1;
	_signatures.push_back(sig);

	// update the idx 
	++_next_sig_id;
	click_chatter("[SigMatcher] signature ADD: %s", sig->s().c_str());
	return 0;
}

int
SigMatcher::del_signature(uint32_t sig_id, ErrorHandler *errh)
{
	for ( Vector<Signature*>::iterator it = _signatures.begin(); it != _signatures.end(); ++it)
	{
		Signature* sig = (*it);
		if (sig->signature_id() == sig_id)
		{
			click_chatter("[SigMatcher] signature DEL: %s", sig->s().c_str());
			sig->set_active(false);
			// del it from the list of active signatures
			_signatures.erase(it);

			return 0;
		}
	}
    return -ENOENT;
}

String
SigMatcher::read_handler(Element* e, void* thunk)
{
    SigMatcher* sm = static_cast<SigMatcher*>(e);
	
	switch (reinterpret_cast<uintptr_t>(thunk)) {
		case H_LIST:
		{
			StringAccum sa;
			for ( Vector<Signature*>::iterator it = sm->_signatures.begin(); it != sm->_signatures.end(); ++it)
			{
				Signature* sig = (*it);
				if (sig->active())
					sa << sig->s() << "\n";
			}
			return sa.take_string();
		}
		case H_NEXT_ID:
		{
			return String(sm->_next_sig_id);
		}
		case H_NUM:
		{
			return String((uint32_t)sm->_signatures.size());
		}
		case H_STATS:
		{
			StringAccum sa;
			sa << "DROP: [pkts: " << sm->_drop_pkts << ", bytes: " << sm->_drop_bytes << "] ";
			sa << "ALERT: [pkts: " << sm->_alert_pkts << ", bytes: " << sm->_alert_bytes << "] ";
			sa << "ALLOW: [pkts: " << sm->_allow_pkts << ", bytes: " << sm->_allow_bytes  << "]";
			return sa.take_string();
		}
		case H_SIG_STATS:
		{
			String s("");
			for ( Vector<Signature*>::iterator it = sm->_signatures.begin(); it != sm->_signatures.end(); ++it)
			{
				Signature* sig = static_cast<Signature*>(*it);
				if (sig && sig->active())
				{
					s += sig->dump_stats() + "\n";
				}
			}
			return s;
		}
		default:
		{
			return "<error: the handler does not exist>";
		}
	}
}

/* Install the signature specified by the configuration string */
int
SigMatcher::add_signature_handler(const String &conf, Element *e, void *thunk, ErrorHandler *errh)
{
	SigMatcher *m = static_cast<SigMatcher *>(e); 

	// create a unique signature id
	Signature* s = Signature::parse_signature(m->_next_sig_id, conf, m->_dotted_ip, errh);
	if (!s)
		return errh->error("<error: something went wrong during the signature creation>");
	
	int rc = m->add_signature(s, errh);
	if (rc < 0)
		return errh->error("<error: unable to add the signature>");

    return 0;
}

/* Uninstall the signature specified by its signature ID */
int
SigMatcher::del_signature_handler(const String &conf, Element *e, void *, ErrorHandler *errh)
{
    SigMatcher *m = static_cast<SigMatcher *>(e);
    
    String s = cp_uncomment(conf).trim_space();
    uint32_t sig_id;
    
    if (!cp_unsigned(s, &sig_id))
	{
		errh->error("<error: the signature ID must be >= 0, found \"%s\"", s.c_str());
		return -1;
	}
	int rc = m->del_signature(sig_id, errh);
    if (rc < 0 && rc != -ENOENT)
		return errh->error("<error: unable to delete the signature>");
	else if (rc == -ENOENT)
		return errh->error("<warn: signature ID %d does not exist>", sig_id);
	return 0;
}

//XXX: TODO
/* reset the signature specified by its signature ID */
//int
//SigMatcher::reset_signature_handler(const String &conf, Element *e, void *, ErrorHandler *errh)
//{
    //return 0;
//}
//

/* remove all the signatures */
int
SigMatcher::clean_signature_handler(const String &conf, Element *e, void *, ErrorHandler *errh)
{
	SigMatcher *m = static_cast<SigMatcher *>(e);
	for ( Vector<Signature*>::iterator it = m->_signatures.begin(); it != m->_signatures.end(); ++it)
	{
		if (*it)
		{
			Signature* sig = *it;
			click_chatter("[SigMatcher] signature DEL: %s", sig->s().c_str());
			sig->set_active(false);
			// del it from the list of active signatures
			m->_signatures.erase(it);
		}
	}
    return 0;
}

int
SigMatcher::read_signature_stats_handler(int, String& s, Element* e, const Handler* h, ErrorHandler* errh)
{
	SigMatcher *sm = static_cast<SigMatcher *>(e);
	
	int32_t sig_id = -1;
	cp_uncomment(s);
	click_chatter("%s", s.c_str());
	if (s && !IntArg().parse(s, sig_id))
		return errh->error("<error: expected a signature ID found %s", s.c_str());
	
	StringAccum sa;
	for ( Vector<Signature*>::iterator it = sm->_signatures.begin(); it != sm->_signatures.end(); ++it)
	{
		Signature* sig = (*it);
		if (sig && sig->signature_id() == (uint32_t)sig_id)
		{
			s = sig->dump_stats();
			return 0;
		}
	}

	s = "the signature ID does not exist";
	return errh->error("<error: signature ID %u does not exist", sig_id);
}

/* install the handlers */
void
SigMatcher::add_handlers()
{
	add_read_handler("list", read_handler, (void *)H_LIST);
	add_read_handler("next_id", read_handler, (void *) H_NEXT_ID);
	add_read_handler("number", read_handler, (void *) H_NUM);
	add_read_handler("stats", read_handler, (void *) H_STATS);
	add_read_handler("sig_stats",  read_handler, (void *) H_SIG_STATS);

	set_handler("sig_stat",  Handler::OP_READ | Handler::READ_PARAM, read_signature_stats_handler, 0);
	
    add_write_handler("add", add_signature_handler, H_ADD);
	add_write_handler("del", del_signature_handler, H_DEL);
	add_write_handler("clean", clean_signature_handler, H_DEL);
	
	//add_write_handler("signatures_reset", reset_signature_handler, H_DEL);
}


CLICK_ENDDECLS

ELEMENT_REQUIRES(FlowCache L7)
EXPORT_ELEMENT(SigMatcher)
