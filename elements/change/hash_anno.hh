#ifndef HASH_ANNO_HH
#define HASH_ANNO_HH

#include <click/packet_anno.hh>

// bytes 24-27: overwriting EXTRA_PACKETS_ANNO
#define JHASH_PACKETS_ANNO_OFFSET       EXTRA_PACKETS_ANNO_OFFSET 
#define JHASH_PACKETS_ANNO_SIZE         4
#define JHASH_PACKETS_ANNO(p)           ((p)->anno_u32(JHASH_PACKETS_ANNO_OFFSET))
#define SET_JHASH_PACKETS_ANNO(p, v)    ((p)->set_anno_u32(JHASH_PACKETS_ANNO_OFFSET, (v)))

#endif
