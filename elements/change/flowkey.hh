/*
 * flowkey.{hh} -- calculate a Jenkins hash upon the packets passed to it. 
 * Vito Piserchia
 *
 * Copyright (c) 2013-2014 Dreamlab Technologies AG
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, subject to the conditions
 * listed in the Click LICENSE file. These conditions include: you must
 * preserve this copyright notice, and you cannot mention the copyright
 * holders in advertising related to the Software without their permission.
 * The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
 * notice is a summary of the Click LICENSE file; the license in that file is
 * legally binding.
 */

#ifndef _FLOWKEY_HH_
#define _FLOWKEY_HH_

#include <click/packet.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>

#include "hash_jenkins.hh"

/* FlowHashKey
 * 
 * Note: only IPv4 is supported
 */
struct FlowHashKey {
	union {
		struct {
			uint32_t src, dst;
			uint16_t sp, dp;
			uint16_t proto; /**< u16 so proto and bidir add up to u32 */
			uint16_t bidir; /**< u16 so proto and bidir add up to u32 */
		};
		uint32_t u32[4];
	} __attribute__((packed));
	
	FlowHashKey()
		: src()
		, dst()
		, sp()
		, dp()
		, proto()
		, bidir()
	{}

	/* C+11:
	 *
	 * FlowHashKey(const FlowHashKey& rhs) = default;
	 */
	 FlowHashKey(const FlowHashKey& rhs)
		: src(rhs.src)
		, dst(rhs.dst)
		, sp(rhs.sp)
		, dp(rhs.dp)
		, proto(rhs.proto)
		, bidir(rhs.bidir)
	 { }
	/* C+11:
	 *
	 * FlowHashKey& operator=(const FlowHashKey &rhs) = default;
	 */
	 
	FlowHashKey& operator=(const FlowHashKey &rhs)
	{
		src   = rhs.src;
		dst   = rhs.dst;
		sp    = rhs.sp;
		dp    = rhs.dp;
		proto = rhs.proto;
		bidir = rhs.bidir;
		
		return *this;
	}

	bool operator==(const FlowHashKey& rhs) const 
	{
		if (this) {
			if ((src == rhs.src) &&
				(dst == rhs.dst) &&
				(sp == rhs.sp) &&
				(dp == rhs.dp) &&
				(proto == rhs.proto) &&
				(bidir == rhs.bidir) )
			{
					return true;
			}
		}
		return false;
	}

	bool operator!= (const FlowHashKey& rhs) 
	{
		return !(*this == rhs);
	} 

public:
		FlowHashKey(uint32_t _src,
				uint32_t _dst,
				uint16_t _sp,
				uint16_t _dp,
				uint16_t _proto,
				uint16_t _bidir)
		{
			_init(_src, _dst, _sp, _dp, _proto, _bidir);
		};

		FlowHashKey(const Packet& p, uint16_t _bidir)
		{
			uint32_t _src, _dst;
			uint16_t _sp, _dp;
			uint8_t _proto;
			
			assert(p.has_network_header());
			const click_ip *ip_hdr = p.ip_header();
			
			_proto = ip_hdr->ip_p;
			_src   = ip_hdr->ip_src.s_addr;
			_dst   = ip_hdr->ip_dst.s_addr;
			// defaults
			_sp  = 0;
			_dp  = 0;
			
			if ( IP_ISFRAG(ip_hdr) )
				/* nada */;
			else if ( _proto == IP_PROTO_TCP )
			{
				const click_tcp *tcp_hdr = p.tcp_header();

				_sp = tcp_hdr->th_sport;
				_dp = tcp_hdr->th_dport;

			}
			else if (_proto == IP_PROTO_UDP)
			{
				const click_udp *udp_hdr = p.udp_header();

				_sp = udp_hdr->uh_sport;
				_dp = udp_hdr->uh_dport;
			}
			_init(_src, _dst, _sp, _dp, _proto, _bidir);
		}
		inline uint32_t key() { return _key; };

	private:

		uint32_t _key;

		void _init(uint32_t _src,
			uint32_t _dst,
			uint16_t _sp,
			uint16_t _dp,
			uint16_t _proto,
			uint16_t _bidir)
		{
			if (!_bidir || _src > _dst)
			{
				src = _src;
				dst = _dst;
			} else
			{
				src = _dst;
				dst = _src;
			}
			if (!_bidir || _sp > _dp)
			{
				sp = _sp;
				dp = _dp;
			} else
			{
				sp = _dp;
				dp = _sp;
			}
			proto = _proto;
			bidir = _bidir;
			// ok create the key
			_key = BobHash(&(u32), 4, 13);
		}
};

#endif
