/*
 * flowpinner.{cc,hh} -- load balance packet using a Jenkins' hash alghorithm
 * Vito Piserchia
 *
 * Copyright (c) 2013-2014 Dreamlab Technologies AG
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, subject to the conditions
 * listed in the Click LICENSE file. These conditions include: you must
 * preserve this copyright notice, and you cannot mention the copyright
 * holders in advertising related to the Software without their permission.
 * The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
 * notice is a summary of the Click LICENSE file; the license in that file is
 * legally binding.
 */

/* click common header files */
#include <click/config.h>
#include <click/error.hh>
#include <click/args.hh>

/* packet headers */
//#include <clicknet/ether.h>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>

#include <click/straccum.hh>

#include "flowkey.hh"
#include "hash_jenkins.hh"

#include "flowpinner.hh"

CLICK_DECLS

enum { H_COUNT };

FlowPinner::FlowPinner()
  : _queues(1), _next(-1), _fastmod(false)
{ }

FlowPinner::~FlowPinner()
{ }

int
FlowPinner::configure(Vector<String> &conf, ErrorHandler *errh)
{
    return 0;
};

int
FlowPinner::initialize(ErrorHandler *errh)
{
	_queues = noutputs();
	/*
	 * noutputs() is a power of 2?
	 * 
	 * From www.exploringbinary.com/ten-ways-to-check-if-an-integer-is-a-power-of-two-in-c/
	 * 
	 * 10: Complement and Compare
	 */	
	_fastmod = ((_queues != 0) && ((_queues & (~_queues + 1)) == _queues));
	
	return 0;
};

inline int
FlowPinner::_get_queue()
{	
	++_next;
	int queue = _fastmod ? _next & (_queues - 1) : _next % _queues;
	_next = queue; 
	return queue;
};

inline int
FlowPinner::_get_queue(uint32_t key)
{
	return _fastmod ? key & (_queues - 1) : key % _queues;
};

void
FlowPinner::push(int port, Packet *p)
{
	const click_ip *ip_hdr;
	uint32_t key;

	if (!p->has_network_header())
	{
		output(_get_queue()).push(p);
		return;
	}	
	ip_hdr = p->ip_header();

	/* Debug
	IPAddress src_addr(ip_hdr->ip_src.s_addr);
	IPAddress dst_addr(ip_hdr->ip_dst.s_addr);

	click_chatter("got pkt with src %s and dst %s, proto:%d, timestamp:%d",
		src_addr.s().c_str(),
		dst_addr.s().c_str(),
		ip_hdr->ip_p,
		p->timestamp_anno().usec());
	*/ 
	
	/* If not an UDP or TCP packet just use a RR dispatching policy */
	if ( !( ip_hdr->ip_p == IP_PROTO_TCP || ip_hdr->ip_p == IP_PROTO_UDP) )
	{
		output(_get_queue()).push(p);
		return;
	}

	FlowHashKey f = FlowHashKey(*p, 1); // bidirectional key
	key = BobHash(&(f.u32), 4, 13);

	/* Debug
	click_chatter("flow: %u %u %u %u [key=%u] [queue=%u]", f.u32[0], f.u32[1], f.u32[2], f.u32[3], key, _get_queue(key));
	*/

	output(_get_queue(key)).push(p);
}

CLICK_ENDDECLS
EXPORT_ELEMENT(FlowPinner)
