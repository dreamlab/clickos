/*
 * simpleforwarder.{cc,hh} -- simple packet forwarder from its input to its output ports
 * Vito Piserchia
 *
 * Copyright (c) 2013-2014 Dreamlab Technologies AG
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, subject to the conditions
 * listed in the Click LICENSE file. These conditions include: you must
 * preserve this copyright notice, and you cannot mention the copyright
 * holders in advertising related to the Software without their permission.
 * The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
 * notice is a summary of the Click LICENSE file; the license in that file is
 * legally binding.
 */

/* click common header files */
#include <click/config.h>
#include <click/error.hh>
#include <click/args.hh>

#include "simpleforwarder.hh"

CLICK_DECLS

SimpleForwarder::SimpleForwarder()
{ }

SimpleForwarder::~SimpleForwarder()
{ }

Packet *
SimpleForwarder::simple_action(Packet *p_in)
{
	return p_in;
}


CLICK_ENDDECLS
EXPORT_ELEMENT(SimpleForwarder)
