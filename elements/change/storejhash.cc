/*
 * storejhash.{cc,hh} -- element sets packets hash annotation
 * Vito Piserchia
 *
 * Copyright (c) 2013-2014 Dreamlab Technologies AG
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, subject to the conditions
 * listed in the Click LICENSE file. These conditions include: you must
 * preserve this copyright notice, and you cannot mention the copyright
 * holders in advertising related to the Software without their permission.
 * The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
 * notice is a summary of the Click LICENSE file; the license in that file is
 * legally binding.
 */


/* click common header files */
#include <click/config.h>
#include <click/error.hh>
#include <click/args.hh>

/* packet headers */
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>

#include <click/straccum.hh>

#include <click/args.hh>
#include "flowkey.hh"
#include "hash_jenkins.hh"
#include "hash_anno.hh"

#include "storejhash.hh"

CLICK_DECLS

StoreIPJHash::StoreIPJHash()
{ }

StoreIPJHash::~StoreIPJHash()
{ }

int
StoreIPJHash::configure(Vector<String> &conf, ErrorHandler *errh)
{
	// default: asymmetric
	bool bidir = false;
	int anno = JHASH_PACKETS_ANNO_OFFSET;
	if ((Args(conf, this, errh)
            .read_p("BIDIR", bidir)
            .read_p("ANNO", AnnoArg(4), anno).complete() < 0))
	{
		return -1;
	}

	_bidir = bidir ? 1 : 0;
	_anno = anno;
	return 0;
};

Packet*
StoreIPJHash::simple_action(Packet *p)
{
	const click_ip *ip_hdr;
	/* not an IP packet? */
	if (!p->has_network_header())
		return p;
	ip_hdr = p->ip_header();
	/* not an UDP or TCP ? */
	if ( !( ip_hdr->ip_p == IP_PROTO_TCP || ip_hdr->ip_p == IP_PROTO_UDP) )
		return p;

	FlowHashKey f(*p, _bidir);
	p->set_anno_u32(_anno, f.key());

	return p;
};

CLICK_ENDDECLS
EXPORT_ELEMENT(StoreIPJHash)
